#include <iota.cuh>


#include <base_defs.h>


__global__ void gpg::_iota::kernel(int n, int *P)
{
    int gc = threadIdx.x + blockIdx.x * BLOCK_SIZE;
    if (gc < n) {
        P[gc] = gc;
    }
}
