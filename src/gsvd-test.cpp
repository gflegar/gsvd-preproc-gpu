#include <iostream>
#include <limits>
#include <algorithm>
#include <iterator>
#include <stdexcept>
#include <fstream>
#include <cstdlib>
#include <argp.h>
#include <cmath>


#include <gsvd_preproc.h>
#include <timer.h>
#include <test/benchmark_utils.h>


struct Args {
    size_t n;
    size_t m;
    size_t p;
    double tol;
    const char *a_file;
    const char *b_file;
    const char *a_res_file;
    const char *b_res_file;
    const char *u_file;
    const char *v_file;
    const char *q_file;
    Args() : n(0), m(0), p(0), tol(1.0), a_file(NULL), b_file(NULL),
        a_res_file("A.dat"), b_res_file("B.dat"), u_file("U.dat"),
        v_file("V.dat"), q_file("Q.dat") {}
};

const char *argp_program_version = "gsvd-test 0.1";
const char *argp_program_bug_address = "<flegar.goran@gmail.com>";
static char doc[] = "gsvd-test -- a program to test the GSVD preprocessing"
                    " algorithm.";
static char args_doc[] = "AFILE BFILE";
static struct argp_option options[] = {
    {0, 'm', "INTEGER", 0, "The number of rows of matrix A", 0},
    {0, 'n', "INTEGER", 0, "The number of columns of matrices A and B", 0},
    {0, 'p', "INTEGER", 0, "The number of rows of matrix B", 0},
    {0, 'a', "FILENAME", 0, "Output file for updated A", 0},
    {0, 'b', "FILENAME", 0, "Output file for updated B", 0},
    {0, 'u', "FILENAME", 0, "Output file for U", 0},
    {0, 'v', "FILENAME", 0, "Output file for V", 0},
    {0, 'q', "FILENAME", 0, "Output file for Q", 0},
    {0, 't', "REAL", 0, "Tolerance value used to determine rank and 0s", 0},
    {0, 0, 0, 0, 0, 0}
};


static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
    Args* args = static_cast<Args*>(state->input);
    switch(key) {
        case 'm': args->m = atoi(arg); break;
        case 'n': args->n = atoi(arg); break;
        case 'p': args->p = atoi(arg); break;
        case 't': args->tol = atof(arg); break;
        case 'a': args->a_res_file = arg; break;
        case 'b': args->b_res_file = arg; break;
        case 'u': args->u_file = arg; break;
        case 'v': args->v_file = arg; break;
        case 'q': args->q_file = arg; break;
        case ARGP_KEY_ARG:
            if (args->a_file == NULL) {
                args->a_file = arg;
            } else if (args->b_file == NULL) {
                args->b_file = arg;
            } else {
                argp_error(state, "to many arguments");
            }
            break;
        case ARGP_KEY_END:
            if (args->b_file == NULL) {
                argp_error(state, "to few arguments");
            }
            if (args->n <= 0 || args->m <= 0 || args->p <= 0) {
                argp_error(state, "dimensions are not positive");
            }
            break;
        default:
            return ARGP_ERR_UNKNOWN;
    }
    return 0;
}
static struct argp argp = {options, parse_opt, args_doc, doc, 0, 0, 0};


template <typename T>
void read_binary(const char *filename, T *data, size_t size)
{
    std::ifstream fin(filename, std::ios::binary);
    if (!fin) throw std::runtime_error("unable to open file");
    fin.exceptions(std::ios::failbit);
    fin.read(reinterpret_cast<char*>(data), size * sizeof(T));
    fin.close();
}


template <typename T>
void write_binary(const char *filename, const T *data, size_t size)
{
    if (filename == NULL) return;
    std::ofstream fout(filename, std::ios::binary);
    if (!fout) throw std::runtime_error("unable to open file");
    fout.exceptions(std::ios::failbit);
    fout.write(reinterpret_cast<const char*>(data), size * sizeof(T));
    fout.close();
}


int main(int argc, char *argv[])
{
    Args args;
    argp_parse(&argp, argc, argv, 0, 0, &args);

    double *A = new double[args.m * args.n];
    double *B = new double[args.p * args.n];
    double *U = new double[args.m * args.m];
    double *V = new double[args.p * args.p];
    double *Q = new double[args.n * args.n];
    double *RA = new double[args.m * args.n];
    double *RB = new double[args.p * args.n];

    int k, l;

    read_binary(args.a_file, A, args.m * args.n);
    read_binary(args.b_file, B, args.p * args.n);

    std::copy(A, A + args.m*args.n, RA);
    std::copy(B, B + args.p*args.n, RB);

    args.tol *= std::max(gettol(args.m, args.n, A), gettol(args.p, args.n, B));

    std::cout << args.m << ',';
    Timer t1;
    t1.start();
    gsvd_preproc(args.m, args.n, args.p, A, B, &k, &l, U, V, Q, args.tol);
    t1.stop();

    to_upper_triangle(args.m, args.n, k + l, A, args.m);
    to_upper_triangle(args.p, args.n, l, B, args.p);

    std::cout << ',' << pow(args.m, 3) / t1.elapsed() << ','
              << diff(args.m, args.n, U, A, Q, RA) << ','
              << diff(args.p, args.n, V, B, Q, RB);

    write_binary(args.a_res_file, A, args.m * args.n);
    write_binary(args.b_res_file, B, args.p * args.n);
    write_binary(args.u_file, U, args.m * args.m);
    write_binary(args.v_file, V, args.p * args.p);
    write_binary(args.q_file, Q, args.n * args.n);

    delete[] U;
    delete[] V;
    delete[] Q;

    #ifdef FWERR
    std::cout << ','
              << fwerr(args.m, args.p, args.n, k, l, RA, args.m, RB, args.p,
                       A, B, args.tol, args.tol);
    #endif
    std::cout << std::endl;

    delete[] A;
    delete[] B;
    delete[] RA;
    delete[] RB;
    return 0;
}

