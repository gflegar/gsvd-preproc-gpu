#include <gsvd_preproc.h>
#include <timer.h>


#include <iostream>
#include <algorithm>
#include <cmath>


extern "C" void dggsvp_(char *JOBU, char *JOBV, char *JOBQ, int *M, int *P,
    int *N, double *A, int *LDA, double *B, int *LDB, double *TOLA,
    double *TOLB, int *K, int *L, double *U, int *LDU, double *V, int *LDV,
    double *Q, int *LDQ, int *IWORK, double *TAU, double *WORK, int *LWORK,
    int *INFO);


void gsvd_preproc(int m, int n, int p, double *A, double *B, int *k, int *l,
                  double *U, double *V, double *Q, double tol)
{
    char jobu = 'U', jobv = 'V', jobq = 'Q';
    double tola = tol;
    double tolb = tol;
    int info;
    int *iwork = new int[n];
    double *tau = new double[n];

    int lwork = std::max(3*n, std::max(m, p));

    double *work = new double[lwork];

    Timer t;
    t.start();

    dggsvp_(&jobu, &jobv, &jobq, &m, &p, &n, A, &m, B, &p, &tola, &tolb,
             k, l, U, &m, V, &p, Q, &n, iwork, tau, work, &lwork, &info);

    t.stop();
    std::cout << *k  << ',' << *l << ',' << pow(m, 3) / t.elapsed();

    delete[] iwork;
    delete[] work;
    delete[] tau;
}

