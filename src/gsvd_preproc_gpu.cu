#include <gsvd_preproc.h>
#include <base_defs.h>
#include <ggsvp.cuh>
#include <timer.h>
#include <utils.cuh>
#include <dmat.cuh>


#include <cmath>
#include <algorithm>
#include <stdexcept>
#include <iostream>


using namespace gpg;


template <typename T>
void transpose(int n, T *A, int lda)
{
    for (int i = 0; i < n; ++i) {
        for (int j = i + 1; j < n; ++j) {
            std::swap(A[i + j*lda], A[j + i*lda]);
        }
    }
}



void gsvd_preproc(int m, int n, int p, double *A, double *B, int *k, int *l,
                  double *U, double *V, double *Q, double tol)
{
    size_t pa;
    size_t pb;
    size_t pu;
    size_t pv;
    size_t pq;
    size_t pw;
    double *d_A = NULL;
    double *d_B = NULL;
    double *d_U = NULL;
    double *d_V = NULL;
    double *d_Q = NULL;
    double *d_work = NULL;
    int *d_W = NULL;

    const dim3 ws = sizes::ggsvp_w<BLOCK_SIZE>(m, n, p);
    const dim3 iws = sizes::ggsvp_iw<BLOCK_SIZE>(m, n, p);
    const size_t dsize = sizeof(double);

    //cudaDeviceSetLimit(cudaLimitPrintfFifoSize, 1<<25);
    cudaDeviceSetCacheConfig(cudaFuncCachePreferShared);

    cudaAssert(cudaMallocPitch(&d_A, &pa, m * dsize, n));
    cudaAssert(cudaMallocPitch(&d_B, &pb, p * dsize, n));
    cudaAssert(cudaMallocPitch(&d_U, &pu, m * dsize, m));
    cudaAssert(cudaMallocPitch(&d_V, &pv, p * dsize, p));
    cudaAssert(cudaMallocPitch(&d_Q, &pq, n * dsize, n));
    cudaAssert(cudaMallocPitch(&d_work, &pw, ws.x * dsize, ws.y));
    cudaAssert(cudaMalloc(&d_W, iws.x * sizeof(int)));
    cudaAssert(cudaMemcpy2D(d_A, pa, A, m * dsize, m * dsize, n,
                            cudaMemcpyHostToDevice));
    cudaAssert(cudaMemcpy2D(d_B, pb, B, p * dsize, p * dsize, n,
                            cudaMemcpyHostToDevice));

    Timer t;

    t.start();

    ggsvp(DMat<double>(d_A, m, n, pa/dsize),
          DMat<double>(d_B, p, n, pb/dsize),
          DMat<double>(d_U, m, m, pu/dsize),
          DMat<double>(d_V, p, p, pv/dsize),
          DMat<double>(d_Q, n, n, pq/dsize),
          *k, *l, DMat<double>(d_work, ws.x, ws.y, pw/dsize), d_W, tol, tol);

    cudaAssert(cudaDeviceSynchronize());

    t.stop();
    std::cout << *k << ',' << *l << ',' << pow(m, 3) / t.elapsed();

    // std::cout << "Rank: " << rank << std::endl;
    cudaAssert(cudaMemcpy2D(A, m * dsize, d_A, pa, m * dsize, n,
                       cudaMemcpyDeviceToHost));
    cudaAssert(cudaMemcpy2D(B, p * dsize, d_B, pb, p * dsize, n,
                       cudaMemcpyDeviceToHost));
    cudaAssert(cudaMemcpy2D(U, m * dsize, d_U, pu, m * dsize, m,
                       cudaMemcpyDeviceToHost));
    cudaAssert(cudaMemcpy2D(V, p * dsize, d_V, pv, p * dsize, p,
                       cudaMemcpyDeviceToHost));
    cudaAssert(cudaMemcpy2D(Q, n * dsize, d_Q, pq, n * dsize, n,
                       cudaMemcpyDeviceToHost));

    transpose(m, U, m);
    transpose(p, V, p);

    cudaAssert(cudaFree(d_A));
    cudaAssert(cudaFree(d_B));
    cudaAssert(cudaFree(d_U));
    cudaAssert(cudaFree(d_V));
    cudaAssert(cudaFree(d_Q));
    cudaAssert(cudaFree(d_work));
    cudaAssert(cudaFree(d_W));
}

