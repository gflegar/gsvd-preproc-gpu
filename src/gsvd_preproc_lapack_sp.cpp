#include <gsvd_preproc.h>
#include <timer.h>


#include <iostream>
#include <algorithm>
#include <cmath>


extern "C" void sggsvp_(char *JOBU, char *JOBV, char *JOBQ, int *M, int *P,
    int *N, float *A, int *LDA, float *B, int *LDB, float *TOLA,
    float *TOLB, int *K, int *L, float *U, int *LDU, float *V, int *LDV,
    float *Q, int *LDQ, int *IWORK, float *TAU, float *WORK, int *LWORK,
    int *INFO);


void gsvd_preproc_sp(int m, int n, int p, float *A, float *B, int *k, int *l,
                     float *U, float *V, float *Q, float tol)
{
    char jobu = 'U', jobv = 'V', jobq = 'Q';
    float tola = tol;
    float tolb = tol;
    int info;
    int *iwork = new int[n];
    float *tau = new float[n];

    int lwork = std::max(3*n, std::max(m, p));

    float *work = new float[lwork];

    Timer t;
    t.start();

    sggsvp_(&jobu, &jobv, &jobq, &m, &p, &n, A, &m, B, &p, &tola, &tolb,
             k, l, U, &m, V, &p, Q, &n, iwork, tau, work, &lwork, &info);

    t.stop();
    std::cout << *k  << ',' << *l << ',' << pow(m, 3) / t.elapsed();

    delete[] iwork;
    delete[] work;
    delete[] tau;
}

