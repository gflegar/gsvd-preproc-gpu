import matplotlib.pyplot as plt
import scipy as sp


def plotdata(axs, xs, ys, fmts, labels, title, xlabel, ylabel):
    for x, y, fmt, lbl in zip(xs, ys, fmts, labels):
        axs.plot(x, y, fmt, label=lbl)
    axs.set_title(title)
    axs.set_xlabel(xlabel)
    axs.set_ylabel(ylabel)
    axs.legend(loc='best')


def plotspeed(prefix, tests, suffix, ratios):
    plt.style.use('ggplot')
    for t, r in zip(tests, ratios):
        fnames = ['lap', 'mkl', 'gpu']
        labels = ['LAPACK', 'MKL', 'GPU']
        fmts = ['ro-', 'bo-', 'go-']
        data = []
        for fname in fnames:
            fn = prefix + '/' + t + '/' + fname + suffix + '.csv'
            data.append(sp.loadtxt(fn, delimiter=','))
        fig = plt.figure()
        for d, l, f in zip(data, labels, fmts):
            plt.plot(d[:,0], d[:,3]/data[0][:,3], f, label=l, figure=fig)
        plt.title('Speedup (ratio = ' + r + ')')
        plt.xlabel('size ($n$)')
        plt.ylabel('speedup')
        plt.legend(loc='best')
        fig.autofmt_xdate()
        ax = fig.axes[0]
        #ax.set_ylim([0, 2e9])
        fig.savefig('figs/speed_' + t + '.eps')
        plt.close(fig)


def plotrank(prefix, tests, suffix, ratios):
    plt.style.use('ggplot')
    for t, r in zip(tests, ratios):
        fnames = ['lap', 'mkl', 'gpu']
        labels = ['LAPACK ($k+l$)', 'MKL ($k+l$)', 'GPU ($k+l$)']
        labels2 = ['LAPACK ($l$)', 'MKL ($l$)', 'GPU ($l$)']
        fmts = ['ro-', 'bo-', 'go-']
        fmts2 = ['ro--', 'bo--', 'go--']
        data = []
        for fname in fnames:
            fn = prefix + '/' + t + '/' + fname + suffix + '.csv'
            data.append(sp.loadtxt(fn, delimiter=','))
        fig = plt.figure()
        for d, l, f in zip(data, labels, fmts):
            plt.plot(d[:,0], d[:,1] + d[:,2], f, label=l, figure=fig)
        for d, l, f in zip(data, labels2, fmts2):
            plt.plot(d[:,0], d[:,2], f, label=l, figure=fig)
        plt.title('Rank (ratio = ' + r + ')')
        plt.xlabel('size ($n$)')
        plt.ylabel('$rank$')
        plt.legend(loc='best')
        fig.autofmt_xdate()
        ax = fig.axes[0]
        #ax.set_ylim([0, 2e9])
        fig.savefig('figs/rank_' + t + '.eps')
        plt.close(fig)


def plotberr(prefix, tests, suffix, ratios):
    plt.style.use('ggplot')
    for t, r in zip(tests, ratios):
        fnames = ['lap', 'mkl', 'gpu']
        labels = ['LAPACK ($A$)', 'MKL ($A$)', 'GPU ($A$)']
        labels2 = ['LAPACK ($B$)', 'MKL ($B$)', 'GPU ($B$)']
        fmts = ['ro-', 'bo-', 'go-']
        fmts2 = ['ro--', 'bo--', 'go--']
        data = []
        for fname in fnames:
            fn = prefix + '/' + t + '/' + fname + suffix + '.csv'
            data.append(sp.loadtxt(fn, delimiter=','))
        fig = plt.figure()
        for d, l, f in zip(data, labels, fmts):
            plt.plot(d[:,0], -sp.log10(d[:,5]), f, label=l, figure=fig)
        for d, l, f in zip(data, labels2, fmts2):
            plt.plot(d[:,0], -sp.log10(d[:,6]), f, label=l, figure=fig)
        plt.title('Backward error (ratio = ' + r + ')')
        plt.xlabel('size ($n$)')
        plt.ylabel('$digits$')
        plt.legend(loc='best')
        fig.autofmt_xdate()
        ax = fig.axes[0]
        #ax.set_ylim([0, 2e9])
        fig.savefig('figs/berr_' + t + '.eps')
        plt.close(fig)


def plotsvals(prefix, tests, suffix, ratios):
    plt.style.use('ggplot')
    for t, r in zip(tests, ratios):
        fnames = ['gpu']
        labels = ['svals']
        fmts = ['ko-']
        data = []
        for fname in fnames:
            fn = prefix + '/' + t + '/' + fname + suffix + '.csv'
            data.append(sp.loadtxt(fn, delimiter=','))
        fig = plt.figure()
        for d, l, f in zip(data, labels, fmts):
            plt.plot(d[:,0], -sp.log10(d[:,7]), f, label=l, figure=fig)
        plt.title('Singular value ratios (ratio = ' + r + ')')
        plt.xlabel('size ($n$)')
        plt.ylabel('$digits$')
        plt.legend(loc='best')
        fig.autofmt_xdate()
        ax = fig.axes[0]
        #ax.set_ylim([0, 2e9])
        fig.savefig('figs/sval_' + t + '.eps')
        plt.close(fig)

def visualize(rdir, suffix = ''):
    lap = sp.loadtxt(rdir + '/lap' + suffix + '.csv', delimiter=',')
    mkl = sp.loadtxt(rdir + '/mkl' + suffix + '.csv', delimiter=',')
    gpu = sp.loadtxt(rdir + '/gpu' + suffix + '.csv', delimiter=',')

    for arr in [lap, mkl, gpu]:
        arr[:,1] += arr[:,2]
        for i in [5, 6]:
            arr[:,i] = -sp.log10(arr[:,i])

    plt.style.use('ggplot')

    labels = ['LAPACK', 'MKL', 'GPU']
    fmts = ['ro-', 'bo-', 'go-']
    fmts2 = ['ro--', 'bo--', 'go--']
    titles = ['$speed$', '$rank$', '$err(A)$', '$err(B)$']
    ylabels = ['$m^3/time$', '$rank$', '$digits$', '$digits$']
    cols = [3, 1, 5, 6]

    fig = plt.figure()
    for i in range(4):
        axs = plt.subplot(2,2, i+1)
        c = cols[i]
        plotdata(axs, [lap[:,0], mkl[:,0], gpu[:,0]],
                 [lap[:,c], mkl[:,c], gpu[:,c]], fmts, labels, titles[i],
                 '$size(m)$', ylabels[i])

    plt.plot(gpu[:,0], -sp.log10(gpu[:,7]), 'ko-', label='sing')
    axs.legend()

    axs = plt.subplot(2,2, 2)
    c = 2
    i = 1
    plotdata(axs, [lap[:,0], mkl[:,0], gpu[:,0]],
             [lap[:,c], mkl[:,c], gpu[:,c]], fmts2, labels, titles[i],
             '$size(m)$', ylabels[i])

    fig.autofmt_xdate()
    plt.tight_layout()
    plt.show()


def fwerr(rdir, suffix = ''):
    gpu = sp.loadtxt(rdir + '/gpu' + suffix + '.csv', delimiter=',')
    plt.plot(gpu[:,0], -sp.log10(gpu[:,7]), 'bo-')
    plt.title('Forward error')
    plt.xlabel('$size(m)$')
    plt.ylabel('$err$')

