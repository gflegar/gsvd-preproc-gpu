function [] = visualize(dir)
    names = glob([dir '/*.csv']);
    
    data = cell(size(names));
    for i = 1:length(names)
        data{i} = load(names{i});
    end
    showPlots(cellfun(@getFilename, names, 'UniformOutput', false), data);
end

function [name] = getFilename(path)
    [~, name, ~] = fileparts(path);
    name = strsplit(name, '_'){1};
end