function [] = showPlots(names, data)
    close;
    figure;
    subplot(3, 2, 1);
    plotColumn(names, data, 4);
    title('Speed');
    xlabel('matrix size (m)');
    ylabel('m^3 / time');
    
    subplot(3, 2, 3);
    plotColumn(names, data, 2);
    title('Rank of block of A (k)');
    xlabel('matrix size (m)');
    ylabel('rank');
    
    subplot(3, 2, 4);
    plotColumn(names, data, 3);
    title('Rank of B (l)');
    xlabel('matrix size (m)');
    ylabel('rank');
    
    subplot(3, 2, 5);
    plotColumn(names, data, 6);
    title('Relative error in A');
    xlabel('matrix size (m)');
    ylabel('error');
    
    subplot(3, 2, 6);
    plotColumn(names, data, 7);
    title('Relative error in B');
    xlabel('matrix size (m)');
    ylabel('error');
    
end


function [] = plotColumn(names, data, col)
    pdata = cell(1, 2*length(data));
    for i = 1:length(data)
        pdata{2*i-1} = data{i}(:,1);
        pdata(2*i) = data{i}(:,col);
    end
    plot(pdata{:});
    legend(names{:}, 'location', 'northwest');
end