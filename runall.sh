set -x
ID=$1
GPUID=1
shift
for tcase in $@; do
    SPATH=results/single/t$tcase
    DPATH=results/double/t$tcase
    mkdir -p $SPATH
    mkdir -p $DPATH
    #for alg in lap mkl gpu; do
    for alg in gpu; do
        FNAME=$alg$ID.csv
        fermi run ./run.sh $tcase 10 $alg $GPUID | tee $DPATH/$FNAME
        fermi run ./run.sh $tcase 10 ${alg}-sp $GPUID | tee $SPATH/$FNAME
    done
done

