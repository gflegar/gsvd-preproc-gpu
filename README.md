GSVD preprocessing algorithm on GPU
===================================

This repository contains a GPGPU (CUDA) implementation of the preprocessing
algorithm of Page's GSVD algorithm.


