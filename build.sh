DIR=$(dirname "$(readlink -f "$0")")

mkdir -p $DIR/build
cd $DIR/build
cmake .. -DCMAKE_BUILD_TYPE=Release ${@:2} && make -j4 $1

