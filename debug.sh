DIR=$(dirname "$(readlink -f "$0")")

mkdir -p $DIR/debug
cd $DIR/debug
cmake .. -DCMAKE_BUILD_TYPE=Debug ${@:2} && make -j4 $1

