TD=$1
TC=$2
ALG=$3

if [ -z $4 ]; then
    VD=0
else
    VD=$4
fi

CUDA_VISIBLE_DEVICES=$VD\
    ./build/gsvd-test-$ALG $(cat test-data/t$TD/call.txt.$TC) || exit 1
