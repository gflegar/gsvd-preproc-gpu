#include <gtest/gtest.h>
#include <armadillo>
#include <algorithm>
#include <limits>


#include <utils.cuh>
#include <dmat.cuh>
#include <test/utils.cuh>
#include <test/block_view.h>
#include <base_defs.h>
#include <bgrmmr.cuh>


using namespace arma;
using namespace gpg;


namespace {


class BgrmmrTest : public ::testing::Test {
protected:
    BgrmmrTest() { arma_rng::set_seed(161616); }


    template <typename T>
    void runBgrmmr(const Mat<T> &D, const Mat<T> &A, Mat<T> &R, int offset)
    {
        R.zeros(size(A));
        T *d_D, *d_A;
        int ldd, lda;
        createOnDevice(D, d_D, ldd);
        createOnDevice(A, d_A, lda);
        bgrmmr(DMat<T>(d_D, D.n_rows, D.n_cols, ldd),
               DMat<T>(d_A, A.n_rows, A.n_cols, lda),
               offset, 0);
        moveFromDevice(R, d_A, lda);
    }

    template <typename T>
    void checkResults(const Mat<T> &D, const Mat<T> &A, const Mat<T> &R,
                      int offset, T eps = std::numeric_limits<T>::epsilon())
    {
        BlockView<const Mat<T> > Dv(D);
        Dv.rblocks.add(BLOCK_SIZE, 2);
        Dv.cblocks.add(D.n_cols % BLOCK_SIZE)
                  .add(BLOCK_SIZE, D.n_cols/BLOCK_SIZE);
        Mat<T> Dd(D.n_cols, D.n_cols, fill::eye);
        BlockView<Mat<T> > Ddv(Dd);
        Ddv.rblocks.add(Dd.n_rows % BLOCK_SIZE)
                   .add(BLOCK_SIZE, Dd.n_rows/BLOCK_SIZE);
        Ddv.cblocks = Ddv.rblocks;

        for (int i = Dv.n_cols() - 1; i >= 0; i -= 2*offset) {
            int rs = Ddv(i,i).n_rows;
            Ddv(i,i) = Mat<T>(Dv(1,i)).tail_rows(rs);
            if (i - offset >= 0) {
                rs = Ddv(i-offset,i).n_rows;
                Ddv(i,i-offset) = Dv(1,i-offset);
                Ddv(i-offset,i) = Mat<T>(Dv(0,i)).tail_rows(rs);
                Ddv(i-offset,i-offset) = Mat<T>(Dv(0,i-offset)).tail_rows(rs);
            }
        }

        EXPECT_PRED3(matricesNear<T>, A*Dd, R, eps);
    }
};


TEST_F(BgrmmrTest, SingleBlockPairNoOffset)
{
    mat A(2*BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    mat D(2*BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    mat R;
    runBgrmmr(D, A, R, 1);
    checkResults(D, A, R, 1);
}


TEST_F(BgrmmrTest, MultipleBlockPairsNoOffset)
{
    mat A(3*BLOCK_SIZE + 7, 5*BLOCK_SIZE + 3, fill::randn);
    mat D(2*BLOCK_SIZE, 5*BLOCK_SIZE + 3, fill::randn);
    mat R;
    runBgrmmr(D, A, R, 1);
    checkResults(D, A, R, 1);
}


TEST_F(BgrmmrTest, MultipleBlockPairsWithOffset)
{
    mat A(5*BLOCK_SIZE + 7, 5*BLOCK_SIZE + 3, fill::randn);
    mat D(2*BLOCK_SIZE, 5*BLOCK_SIZE + 3, fill::randn);
    mat R;
    runBgrmmr(D, A, R, 2);
    checkResults(D, A, R, 2);
}


TEST_F(BgrmmrTest, SingleBlockPairNoOffset_SP)
{
    fmat A(2*BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    fmat D(2*BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    fmat R;
    runBgrmmr(D, A, R, 1);
    checkResults(D, A, R, 1);
}


TEST_F(BgrmmrTest, MultipleBlockPairsNoOffset_SP)
{
    fmat A(3*BLOCK_SIZE + 7, 5*BLOCK_SIZE + 3, fill::randn);
    fmat D(2*BLOCK_SIZE, 5*BLOCK_SIZE + 3, fill::randn);
    fmat R;
    runBgrmmr(D, A, R, 1);
    checkResults(D, A, R, 1);
}


TEST_F(BgrmmrTest, MultipleBlockPairsWithOffset_SP)
{
    fmat A(5*BLOCK_SIZE + 7, 5*BLOCK_SIZE + 3, fill::randn);
    fmat D(2*BLOCK_SIZE, 5*BLOCK_SIZE + 3, fill::randn);
    fmat R;
    runBgrmmr(D, A, R, 2);
    checkResults(D, A, R, 2);
}


}
