#include <gtest/gtest.h>
#include <armadillo>
#include <algorithm>


#include <utils.cuh>
#include <test/utils.cuh>
#include <dmat.cuh>
#include <base_defs.h>
#include <sbrq.cuh>


using namespace arma;
using namespace gpg;


namespace {


class SbrqTest : public ::testing::Test {
protected:
    SbrqTest() { arma_rng::set_seed(242424); }

    template <typename T>
    void runSbrq(const Mat<T> &A, Mat<T> &Q, Mat<T> &R)
    {
        const dim3 qs = sizes::sbrq_q<BLOCK_SIZE>(A.n_rows, A.n_cols);
        Q = zeros<Mat<T> >(qs.x, qs.y);
        R = zeros<Mat<T> >(size(A));
        T *d_A, *d_Q;
        int lda, ldq;
        createOnDevice(A, d_A, lda);
        createOnDevice(Q, d_Q, ldq);
        sbrq(DMat<T>(d_A, A.n_rows, A.n_cols, lda),
             DMat<T>(d_Q, Q.n_rows, Q.n_cols, ldq),
             0);
        moveFromDevice(R, d_A, lda);
        moveFromDevice(Q, d_Q, ldq);
    }

    template <typename T>
    void checkResults(const Mat<T> &A, const Mat<T> &Q, Mat<T> &R,
                      T eps = std::numeric_limits<T>::epsilon())
    {
        int ofr = A.n_rows - (A.n_rows / BLOCK_SIZE) * BLOCK_SIZE;
        int ofc = A.n_cols - (A.n_cols / BLOCK_SIZE) * BLOCK_SIZE;
        if (ofr == 0) ofr = BLOCK_SIZE;
        if (ofc == 0) ofc = BLOCK_SIZE;
        for (int i = ofr; i < A.n_rows; i += BLOCK_SIZE) {
            for (int j = ofc; j < A.n_cols; j += BLOCK_SIZE) {
                span rows(std::max<int>(i-BLOCK_SIZE, 0), i-1);
                span cols(std::max<int>(j-BLOCK_SIZE, 0), j-1);
                Mat<T> Ap = A(rows, cols);
                Mat<T> Qp = Q(span(i-ofr, i+BLOCK_SIZE-ofr-1), cols);
                if (j == ofc) {
                    Qp = Qp.tail_rows(ofc);
                }
                Mat<T> Rp = R(rows, cols);
                Mat<T> I = eye<Mat<T> >(Qp.n_rows, Qp.n_rows);
                EXPECT_PRED3(matricesNear<T>, I, Qp*Qp.t(), eps)
                    << "Submatrix of Q starting at (" << i << "," << j
                    << ") is not orthogonal";
                EXPECT_PRED3(matricesNear<T>,
                             upperTriangle(Rp, Rp.n_cols - Rp.n_rows - 1),
                             Rp, eps)
                    << "Submatrix of R starting at (" << i << "," << j
                    << ") is not triangular";
                Rp = upperTriangle(Rp, Rp.n_cols - Rp.n_rows - 1);
                Qp = Qp.head_cols(Qp.n_rows);
                EXPECT_PRED3(matricesNear<T>, Ap, Rp*Qp.t(), eps)
                    << "QR != A for submatrices at (" << i << "," << j <<")";
            }
        }
    }
};


TEST_F(SbrqTest, SingleBlock)
{
    mat A = randn<mat>(BLOCK_SIZE, BLOCK_SIZE);
    mat Q, R;
    runSbrq(A, Q, R);
    checkResults(A, Q, R);
}


TEST_F(SbrqTest, MultipleBlocks)
{
    mat A = randn<mat>(3*BLOCK_SIZE + 12, 2*BLOCK_SIZE + 15);
    mat Q, R;
    runSbrq(A, Q, R);
    checkResults(A, Q, R);
}


TEST_F(SbrqTest, Zeros)
{
    mat A = zeros<mat>(2*BLOCK_SIZE + 3, BLOCK_SIZE + 7);
    mat Q, R;
    runSbrq(A, Q, R);
    checkResults(A, Q, R);
}


TEST_F(SbrqTest, SingleBlock_SP)
{
    fmat A = randn<fmat>(BLOCK_SIZE, BLOCK_SIZE);
    fmat Q, R;
    runSbrq(A, Q, R);
    checkResults(A, Q, R);
}


TEST_F(SbrqTest, MultipleBlocks_SP)
{
    fmat A = randn<fmat>(3*BLOCK_SIZE + 12, 2*BLOCK_SIZE + 15);
    fmat Q, R;
    runSbrq(A, Q, R);
    checkResults(A, Q, R);
}


TEST_F(SbrqTest, Zeros_SP)
{
    fmat A = zeros<fmat>(2*BLOCK_SIZE + 3, BLOCK_SIZE + 7);
    fmat Q, R;
    runSbrq(A, Q, R);
    checkResults(A, Q, R);
}


}
