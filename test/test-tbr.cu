#include <gtest/gtest.h>
#include <armadillo>
#include <limits>


#include <base_defs.h>
#include <dmat.cuh>
#include <utils.cuh>
#include <test/utils.cuh>
#include <sbr.cuh>
#include <tbr.cuh>


using namespace gpg;
using namespace arma;


namespace {


class TbrTest : public ::testing::Test {
protected:
    TbrTest() { arma_rng::set_seed(202020); }

    Col<int> count(int n)
    {
        Col<int> c(n);
        for (int i = 0; i < n; ++i) {
            c(i) = i;
        }
        return c;
    }

    template <typename T>
    Mat<T> preprocess(const Mat<T> &A)
    {
        Mat<T> R(size(A));
        Mat<int> P = count(A.n_cols);
        int lda, ldr, ldp;
        int *d_P;
        T *d_A, *d_R;
        createOnDevice(A, d_A, lda);
        createOnDevice(R, d_R, ldr);
        createOnDevice(P, d_P, ldp);
        sbr(DMat<T>(d_A, A.n_rows, A.n_cols, lda),
            DMat<T>(d_R, A.n_rows, A.n_cols, ldr),
            d_P, 0);
        moveFromDevice(R, d_R, ldr);
        return R;
    }

    template <typename T>
    void runTbr(const  Mat<T> &A, Mat<T> &R, int offset)
    {
        R.zeros(size(A));
        int lda;
        T *d_A;
        createOnDevice(A, d_A, lda);
        tbr(DMat<T>(d_A, A.n_rows, A.n_cols, lda), offset, 0);
        moveFromDevice(R, d_A, lda);
    }

    template <typename T>
    void checkResults(const Mat<T> &A, const Mat<T> &R, int offset,
                      T eps = std::numeric_limits<T>::epsilon())
    {
        int ofs = offset * 2*BLOCK_SIZE;
        for (int i = 0; i < A.n_rows; i += 2*ofs) {
            for (int j = 0; j < A.n_cols; j += 2*ofs) {
                int r1 = std::min<int>(2*BLOCK_SIZE, A.n_rows - i);
                int r2 = std::min<int>(2*BLOCK_SIZE, A.n_rows - i - ofs);
                int c = std::min<int>(2*BLOCK_SIZE, A.n_cols - j);
                span rs1(i, i + r1 - 1);
                span rs2(i + ofs, i + ofs + r2 - 1);
                span cs(j, j + c - 1);
                Mat<T> A1 = A(rs1, cs);
                Mat<T> A2(1, c, fill::zeros);
                if (r2 > 0) {
                    A2 = A(rs2, cs);
                }
                Mat<T> Rs = R(rs1, cs);
                EXPECT_PRED3(matricesNear<T>, upperTriangle(Rs), Rs, eps)
                    << "Block starting at (" << i << "," << j
                    << ") is not triangular";
                EXPECT_PRED3(matricesNear<T>, diagvec(A1.t()*A1 + A2.t()*A2),
                             diagvec(Rs.t()*Rs), eps)
                    << "Column norms of blocks starting at (" << i << "," << j
                    << ") do not match";
            }
        }
    }
};


TEST_F(TbrTest, SingleBlock)
{
    mat A = preprocess(mat(2*BLOCK_SIZE, 2*BLOCK_SIZE, fill::randn));
    mat R;
    runTbr(A, R, 1);
    checkResults(A, R, 1);
}


TEST_F(TbrTest, MultipleBlocks)
{
    mat A = preprocess(mat(15*BLOCK_SIZE + 7, 12*BLOCK_SIZE + 9, fill::randn));
    mat R;
    runTbr(A, R, 1);
    checkResults(A, R, 1);
}


TEST_F(TbrTest, MultipleBlocksOffset)
{
    mat A = preprocess(mat(15*BLOCK_SIZE + 7, 12*BLOCK_SIZE + 9, fill::randn));
    mat R;
    runTbr(A, R, 1);
    checkResults(A, R, 1);
    runTbr(A, R, 2);
    checkResults(A, R, 2);
    runTbr(A, R, 4);
    checkResults(A, R, 4);
}


TEST_F(TbrTest, SingleBlock_SP)
{
    fmat A = preprocess(fmat(2*BLOCK_SIZE, 2*BLOCK_SIZE, fill::randn));
    fmat R;
    runTbr(A, R, 1);
    checkResults(A, R, 1);
}


TEST_F(TbrTest, MultipleBlocks_SP)
{
    fmat A = preprocess(fmat(15*BLOCK_SIZE + 7, 12*BLOCK_SIZE + 9, fill::randn));
    fmat R;
    runTbr(A, R, 1);
    checkResults(A, R, 1);
}


TEST_F(TbrTest, MultipleBlocksOffset_SP)
{
    fmat A = preprocess(fmat(15*BLOCK_SIZE + 7, 12*BLOCK_SIZE + 9, fill::randn));
    fmat R;
    runTbr(A, R, 1);
    checkResults(A, R, 1);
    runTbr(A, R, 2);
    checkResults(A, R, 2);
    runTbr(A, R, 4);
    checkResults(A, R, 4);
}

}

