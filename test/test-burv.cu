
#include <gtest/gtest.h>
#include <armadillo>
#include <algorithm>
#include <limits>
#include <cmath>


#include <utils.cuh>
#include <test/utils.cuh>
#include <base_defs.h>
#include <burv.cuh>


using namespace arma;
using namespace gpg;


namespace {


class BurvTest : public ::testing::Test {
protected:
    BurvTest() { arma_rng::set_seed(313131); }

    template <typename T>
    void runBurv(const Mat<T> &A, const Mat<T> &U, const Mat<T> &V, Mat<T> &R,
                 Mat<T> &U2, Mat<T> &V2, int &rank,
                 T eps = std::numeric_limits<T>::epsilon())
    {
        const dim3 ws = sizes::burv_w<BLOCK_SIZE>(A.n_rows, A.n_cols);
        const dim3 iws = sizes::burv_iw<BLOCK_SIZE>(A.n_rows, A.n_cols);
        R.zeros(size(A));
        U2.zeros(size(U));
        V2.zeros(size(V));
        Mat<T> W(ws.x, ws.y, fill::zeros);
        Mat<int> IW(iws.x, 1, fill::zeros);
        T *d_A, *d_U, *d_W, *d_V;
        int *d_IW;
        int lda, ldu, ldw, ldv, ldiw;
        createOnDevice(A, d_A, lda);
        createOnDevice(U, d_U, ldu);
        createOnDevice(W, d_W, ldw);
        createOnDevice(V, d_V, ldv);
        createOnDevice(IW, d_IW, ldiw);

        cudaStream_t s[3];
        for (int i = 0; i < 3; ++i) {
            cudaAssert(cudaStreamCreate(&s[i]));
        }

        burv(DMat<T>(d_A, A.n_rows, A.n_cols, lda), s[0],
             std::vector<DMat<T> >(1, DMat<T>(d_U, U.n_rows, U.n_cols, ldu)),
             std::vector<cudaStream_t>(1, s[1]),
             std::vector<DMat<T> >(1, DMat<T>(d_V, V.n_rows, V.n_cols, ldv)),
             std::vector<cudaStream_t>(1, s[2]),
             DMat<T>(d_W, W.n_rows, W.n_cols, ldw), d_IW, rank,
             eps * norm(A, "fro") * std::max(A.n_rows, A.n_cols));

        cudaAssert(cudaDeviceSynchronize());

        moveFromDevice(R, d_A, lda);
        moveFromDevice(U2, d_U, ldu);
        moveFromDevice(V2, d_V, ldv);

        for (int i = 0; i < 3; ++i) {
            cudaAssert(cudaStreamDestroy(s[i]));
        }
    }

    template <typename T>
    void checkResults(const Mat<T> &A, const Mat<T> &U, const Mat<T> &V,
                      const Mat<T> &R, const Mat<T> &U2, const Mat<T> &V2,
                      int trank, int rank,
                      T eps = std::numeric_limits<T>::epsilon())
    {
        EXPECT_PRED3(matricesNear<T>,
                     upperTriangle(R, R.n_cols - rank - 1),
                     R, eps)
            << "R factor is not triangular";
        EXPECT_PRED3(matricesNear<T>, U.t() * U, U2.t()*U2, eps)
            << "U factor is not orthogonal";
        EXPECT_PRED3(matricesNear<T>, V * V.t(), V2 * V2.t(), eps)
            << "V factor is not orthogonal";
        EXPECT_PRED3(matricesNear<T>, U.t() * A * V.t(), U2.t() * R * V2.t(),
                     eps)
            << "U' * A * V' != U2' * R * V2'";
        std::cout << "Computed rank: " << rank << " True rank: " << trank
                  << std::endl;
    }
};


TEST_F(BurvTest, SingleBlock)
{
    mat A(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    mat Q(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    mat P(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    mat R, Q2, P2;
    int rank;
    runBurv(A, Q, P, R, Q2, P2, rank);
    checkResults(A, Q, P, R, Q2, P2, BLOCK_SIZE, rank);
}


TEST_F(BurvTest, SingleBlockColumn)
{
    mat A = randn<mat>(5*BLOCK_SIZE + 7, 7) *
            randn<mat>(7, BLOCK_SIZE);
    mat Q(5*BLOCK_SIZE + 7, 3*BLOCK_SIZE + 2, fill::randn);
    mat P(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    mat R, Q2, P2;
    int rank;
    runBurv(A, Q, P, R, Q2, P2, rank);
    checkResults(A, Q, P, R, Q2, P2, 7, rank);
}


TEST_F(BurvTest, MultipleBlocksNoQP)
{
    mat A = randn<mat>(5*BLOCK_SIZE + 7, 17) *
            randn<mat>(17, 2*BLOCK_SIZE + 3);
    mat Q(5*BLOCK_SIZE + 7, 5*BLOCK_SIZE + 7, fill::eye);
    mat P(2*BLOCK_SIZE + 3, 2*BLOCK_SIZE + 3, fill::eye);
    mat R, Q2, P2;
    int rank;
    runBurv(A, Q, P, R, Q2, P2, rank);
    checkResults(A, Q, P, R, Q2, P2, 17, rank);
}

TEST_F(BurvTest, MultipleBlocks)
{
    mat A = randn<mat>(5*BLOCK_SIZE + 7, 17) *
            randn<mat>(17, 2*BLOCK_SIZE + 3);
    mat Q(5*BLOCK_SIZE + 7, 3*BLOCK_SIZE + 2, fill::randn);
    mat P(2*BLOCK_SIZE + 3, 2*BLOCK_SIZE + 3, fill::randn);
    mat R, Q2, P2;
    int rank;
    runBurv(A, Q, P, R, Q2, P2, rank);
    checkResults(A, Q, P, R, Q2, P2, 17, rank);
}


TEST_F(BurvTest, SingleBlock_SP)
{
    fmat A(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    fmat Q(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    fmat P(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    fmat R, Q2, P2;
    int rank;
    runBurv(A, Q, P, R, Q2, P2, rank);
    checkResults(A, Q, P, R, Q2, P2, BLOCK_SIZE, rank);
}


TEST_F(BurvTest, SingleBlockColumn_SP)
{
    fmat A = randn<fmat>(5*BLOCK_SIZE + 7, 7) *
             randn<fmat>(7, BLOCK_SIZE);
    fmat Q(5*BLOCK_SIZE + 7, 3*BLOCK_SIZE + 2, fill::randn);
    fmat P(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    fmat R, Q2, P2;
    int rank;
    runBurv(A, Q, P, R, Q2, P2, rank);
    checkResults(A, Q, P, R, Q2, P2, 7, rank);
}


TEST_F(BurvTest, MultipleBlocks_SP)
{
    fmat A = randn<fmat>(5*BLOCK_SIZE + 7, 17) *
             randn<fmat>(17, 2*BLOCK_SIZE + 3);
    fmat Q(5*BLOCK_SIZE + 7, 3*BLOCK_SIZE + 2, fill::randn);
    fmat P(2*BLOCK_SIZE + 3, 2*BLOCK_SIZE + 3, fill::randn);
    fmat R, Q2, P2;
    int rank;
    runBurv(A, Q, P, R, Q2, P2, rank);
    checkResults(A, Q, P, R, Q2, P2, 17, rank);
}


}
