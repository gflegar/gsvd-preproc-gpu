#include <gtest/gtest.h>
#include <armadillo>
#include <algorithm>
#include <limits>


#include <utils.cuh>
#include <dmat.cuh>
#include <test/utils.cuh>
#include <base_defs.h>
#include <bdmmr.cuh>


using namespace arma;
using namespace gpg;


namespace {


class BdmmrTest : public ::testing::Test {
protected:
    BdmmrTest() { arma_rng::set_seed(252525); }

    template <typename T>
    void runBdmmr(const Mat<T> &D, const Mat<T> &A, Mat<T> &R)
    {
        R = zeros<Mat<T> >(size(A));
        T *d_D, *d_A;
        int ldd, lda;
        createOnDevice(D, d_D, ldd);
        createOnDevice(A, d_A, lda);
        bdmmr(DMat<T>(d_D, D.n_rows, D.n_cols, ldd),
              DMat<T>(d_A, A.n_rows, A.n_cols, lda),
              0);
        moveFromDevice(R, d_A, lda);
    }

    template <typename T>
    void checkResults(const Mat<T> &D, const Mat<T> &A, const Mat<T> &R,
                      T eps = std::numeric_limits<T>::epsilon())
    {
        Mat<T> Dd(D.n_cols, D.n_cols, fill::zeros);
        int ofs = (D.n_cols % BLOCK_SIZE);
        if (ofs == 0) ofs = BLOCK_SIZE;
        for (int i = ofs; i <= D.n_cols; i += BLOCK_SIZE) {
            const int sz = (i == ofs) ? ofs : BLOCK_SIZE;
            const span rng(i - sz, i - 1);
            Dd(rng, rng) = D(span(BLOCK_SIZE - sz, BLOCK_SIZE - 1), rng);
        }
        EXPECT_PRED3(matricesNear<T>, R, A * Dd, eps);
    }
};


TEST_F(BdmmrTest, SingleBlock)
{
    mat D(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    mat A(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    mat R;
    runBdmmr(D, A, R);
    checkResults(D, A, R);
}


TEST_F(BdmmrTest, MultipleBlocks)
{
    mat D(BLOCK_SIZE, 4*BLOCK_SIZE + 15, fill::randn);
    mat A(3*BLOCK_SIZE + 5, 4 * BLOCK_SIZE + 15, fill::randn);
    mat R;
    runBdmmr(D, A, R);
    checkResults(D, A, R);
}


TEST_F(BdmmrTest, SingleBlock_SP)
{
    fmat D(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    fmat A(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    fmat R;
    runBdmmr(D, A, R);
    checkResults(D, A, R);
}


TEST_F(BdmmrTest, MultipleBlocks_SP)
{
    fmat D(BLOCK_SIZE, 4*BLOCK_SIZE + 15, fill::randn);
    fmat A(3*BLOCK_SIZE + 5, 4 * BLOCK_SIZE + 15, fill::randn);
    fmat R;
    runBdmmr(D, A, R);
    checkResults(D, A, R);
}


}

