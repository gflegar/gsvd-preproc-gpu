#include <gtest/gtest.h>
#include <armadillo>
#include <algorithm>
#include <limits>


#include <utils.cuh>
#include <test/utils.cuh>
#include <base_defs.h>
#include <dmat.cuh>
#include <bcp.cuh>


using namespace arma;
using namespace gpg;


namespace {


class BcpTest : public ::testing::Test {
protected:
    BcpTest() { arma_rng::set_seed(272727); }

    template <typename T>
    uvec toUvec(Mat<T> M) {
        uvec r(M.n_rows);
        for (int i = 0 ; i< M.n_rows; ++i) {
            r(i) = M(i, 0);
        }
        return r;
    }

    Col<int> count(int n)
    {
        Col<int> c(n);
        for (int i = 0; i < n; ++i) {
            c(i) = i;
        }
        return c;
    }

    template <typename T>
    void runBcp(const Mat<T> &A, const Mat<int> &P, Mat<T> &R)
    {
        R.resize(size(A));
        T *d_A;
        int *d_P;
        int lda, ldp;
        createOnDevice(A, d_A, lda);
        createOnDevice(P, d_P, ldp);
        bcp(DMat<T>(d_A, A.n_rows, A.n_cols, lda), d_P, 0);
        moveFromDevice(R, d_A, lda);
    }

    template <typename T>
    void checkResults(const Mat<T> &A, const Mat<int> &P, const Mat<T> &R,
                      T eps = std::numeric_limits<T>::epsilon())
    {
        Mat<T> C = A.cols(toUvec(P));
        EXPECT_PRED3(matricesNear<T>, C, R.head_cols(P.n_rows), eps);
    }
};


TEST_F(BcpTest, SingleBlock)
{
    mat A(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    Mat<int> P(shuffle(count(A.n_cols)));
    mat R;
    runBcp(A, P, R);
    checkResults(A, P, R);
}


TEST_F(BcpTest, MultipleBlocks)
{
    mat A(4*BLOCK_SIZE + 2, 3*BLOCK_SIZE + 5, fill::randn);
    Mat<int> P(shuffle(count(BLOCK_SIZE)));
    mat R;
    runBcp(A, P, R);
    checkResults(A, P, R);
}


TEST_F(BcpTest, ReducedBlock)
{
    mat A(4*BLOCK_SIZE + 2, BLOCK_SIZE - 2, fill::randn);
    Mat<int> P(shuffle(count(A.n_cols)));
    mat R;
    runBcp(A, P, R);
    checkResults(A, P, R);
}


TEST_F(BcpTest, MultipleBlockOutOfBounds)
{
    mat A(4*BLOCK_SIZE + 2, 3*BLOCK_SIZE + 2, fill::randn);
    Mat<int> P(shuffle(count(A.n_cols)));
    mat R;
    P = P.head_rows(BLOCK_SIZE);
    runBcp(A, P, R);
    checkResults(A, P, R);
}


TEST_F(BcpTest, SingleBlock_SP)
{
    fmat A(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    Mat<int> P(shuffle(count(A.n_cols)));
    fmat R;
    runBcp(A, P, R);
    checkResults(A, P, R);
}


TEST_F(BcpTest, MultipleBlocks_SP)
{
    fmat A(4*BLOCK_SIZE + 2, 3*BLOCK_SIZE + 5, fill::randn);
    Mat<int> P(shuffle(count(BLOCK_SIZE)));
    fmat R;
    runBcp(A, P, R);
    checkResults(A, P, R);
}


TEST_F(BcpTest, ReducedBlock_SP)
{
    fmat A(4*BLOCK_SIZE + 2, BLOCK_SIZE - 2, fill::randn);
    Mat<int> P(shuffle(count(A.n_cols)));
    fmat R;
    runBcp(A, P, R);
    checkResults(A, P, R);
}


TEST_F(BcpTest, MultipleBlockOutOfBounds_SP)
{
    fmat A(4*BLOCK_SIZE + 2, 3*BLOCK_SIZE + 2, fill::randn);
    Mat<int> P(shuffle(count(A.n_cols)));
    fmat R;
    P = P.head_rows(BLOCK_SIZE);
    runBcp(A, P, R);
    checkResults(A, P, R);
}


}
