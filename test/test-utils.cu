#include <gtest/gtest.h>
#include <armadillo>
#include <stdexcept>


#include <utils.cuh>
#include <test/utils.cuh>


using namespace arma;
using namespace gpg;


namespace {


TEST(TestUtils, BlockCalculations)
{
    ASSERT_EQ(4, rud(10, 3));
    dim3 d = ggs(dim3(32,32,32), 43, 41, 40);
    EXPECT_EQ(2, d.x);
    EXPECT_EQ(2, d.y);
    EXPECT_EQ(2, d.z);
}


TEST(TestUtils, CudaErrorDetection)
{
    EXPECT_NO_THROW(cudaAssert(cudaSuccess));
    EXPECT_THROW(cudaAssert(cudaErrorLaunchFailure), std::runtime_error);
}


TEST(TestUtils, DeviceCopy)
{
    arma_rng::set_seed(171717);
    mat A = randn<mat>(3, 5);
    mat B = randn<mat>(3, 5);
    double *d_A;
    int lda;
    createOnDevice(A, d_A, lda);
    moveFromDevice(B, d_A, lda);
    ASSERT_PRED3(matricesNear<double>, A, B, 1e-16);
}


TEST(TestUtils, UpperTriangle)
{
    arma_rng::set_seed(171718);
    mat A = randn<mat>(4, 3);
    mat B = upperTriangle(A);
    mat C = upperTriangle(A, 0);
    A(1, 0) = A(2, 0) = A(3, 0) = 0;
    A(2, 1) = A(3, 1) = 0;
    A(3, 2) = 0;
    EXPECT_PRED3(matricesNear<double>, A, B, 1e-16);
    A(0, 0) = A(1, 1) = A(2, 2) = 0;
    EXPECT_PRED3(matricesNear<double>, A, C, 1e-16);
}


}
