#include <gtest/gtest.h>
#include <armadillo>
#include <algorithm>
#include <limits>


#include <utils.cuh>
#include <dmat.cuh>
#include <test/utils.cuh>
#include <base_defs.h>
#include <bdmm.cuh>


using namespace arma;
using namespace gpg;


namespace {


class BdmmTest : public ::testing::Test {
protected:
    BdmmTest() { arma_rng::set_seed(151515); }

    template <typename T>
    void runBdmm(const Mat<T> &D, const Mat<T> &A, Mat<T> &R)
    {
        R = zeros<Mat<T> >(size(A));
        T *d_D, *d_A;
        int ldd, lda;
        createOnDevice(D, d_D, ldd);
        createOnDevice(A, d_A, lda);
        bdmm(DMat<T>(d_D, D.n_rows, D.n_cols, ldd),
             DMat<T>(d_A, A.n_rows, A.n_cols, lda),
             0);
        moveFromDevice(R, d_A, lda);
    }

    template <typename T>
    void checkResults(const Mat<T> &D, const Mat<T> &A, const Mat<T> &R,
                      T eps = std::numeric_limits<T>::epsilon())
    {
        Mat<T> Dd(D.n_rows, D.n_rows, fill::zeros);
        for (int i = 0; i < D.n_rows; i += BLOCK_SIZE) {
            const int sz = std::min<int>(BLOCK_SIZE, D.n_rows - i);
            const span rng(i, i + sz - 1);
            Dd(rng, rng) = D(rng, span(0, sz - 1));
        }
        EXPECT_PRED3(matricesNear<T>, R, Dd*A, eps);
    }
};


TEST_F(BdmmTest, SingleBlock)
{
    mat D(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    mat A(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    mat R;
    runBdmm(D, A, R);
    checkResults(D, A, R);
}


TEST_F(BdmmTest, MultipleBlocks)
{
    mat D(3*BLOCK_SIZE + 5, BLOCK_SIZE, fill::randn);
    mat A(3*BLOCK_SIZE + 5, 4 * BLOCK_SIZE + 15, fill::randn);
    mat R;
    runBdmm(D, A, R);
    checkResults(D, A, R);
}


TEST_F(BdmmTest, SingleBlock_SP)
{
    fmat D(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    fmat A(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    fmat R;
    runBdmm(D, A, R);
    checkResults(D, A, R);
}


TEST_F(BdmmTest, MultipleBlocks_SP)
{
    fmat D(3*BLOCK_SIZE + 5, BLOCK_SIZE, fill::randn);
    fmat A(3*BLOCK_SIZE + 5, 4 * BLOCK_SIZE + 15, fill::randn);
    fmat R;
    runBdmm(D, A, R);
    checkResults(D, A, R);
}


}

