#include <gtest/gtest.h>
#include <armadillo>
#include <algorithm>
#include <limits>


#include <utils.cuh>
#include <test/utils.cuh>
#include <base_defs.h>
#include <bcfp.cuh>


using namespace arma;
using namespace gpg;


namespace {


class BcfpTest : public ::testing::Test {
protected:
    BcfpTest() { arma_rng::set_seed(232323); }

    template <typename T>
    uvec toUvec(Mat<T> M) {
        uvec r(M.n_rows);
        for (int i = 0 ; i< M.n_rows; ++i) {
            r(i) = M(i, 0);
        }
        return r;
    }

    template <typename T>
    void runBcfp(const Mat<T> &A, Mat<int> &P, int &rank,
                 T eps = std::numeric_limits<T>::epsilon())
    {
        dim3 ws = sizes::bcfp_w<BLOCK_SIZE>(A.n_rows, A.n_cols);
        dim3 iws = sizes::bcfp_iw<BLOCK_SIZE>(A.n_rows, A.n_cols);
        Mat<T> W(ws.x, ws.y, fill::zeros);
        Mat<int> IW(iws.x, 1, fill::zeros);
        T *d_A, *d_W;
        int *d_P, *d_IW;
        int lda, ldw, ldp, ldiw;
        createOnDevice(A, d_A, lda);
        createOnDevice(P, d_P, ldp);
        createOnDevice(W, d_W, ldw);
        createOnDevice(IW, d_IW, ldiw);
        bcfp(DMat<T>(d_A, A.n_rows, A.n_cols, lda),
             DMat<T>(d_W, W.n_rows, W.n_cols, ldw),
             d_P, d_IW, rank, eps * norm(A, "fro"), 0);
        moveFromDevice(P, d_P, ldp);
    }

    template <typename T>
    void checkResults(const Mat<T> &A, const Mat<int> &P, int cr, int tr)
    {
        int cols = std::min<int>(BLOCK_SIZE, A.n_cols);
        tr = std::min<int>(tr, cols);
        Mat<T> Ap = A.cols(toUvec(Mat<int>(P.rows(0, cols-1))));
        EXPECT_EQ(tr, cr) << "True rank and computed rank differ.";
        EXPECT_EQ(tr, rank(Ap))
            << "Rank of permuted matrix doesn't match the true rank.";
    }
};


TEST_F(BcfpTest, SingleBlock)
{
    mat A(2*BLOCK_SIZE, 2*BLOCK_SIZE, fill::randn);
    Mat<int> P(2*BLOCK_SIZE + 1, 1);
    int rank;
    runBcfp(A, P, rank);
    checkResults(A, P, rank, 2*BLOCK_SIZE);
}


TEST_F(BcfpTest, SingleBlockReducedRank)
{
    mat A = randn<mat>(2*BLOCK_SIZE, 7) * randn<mat>(7, 2*BLOCK_SIZE);
    Mat<int> P(2*BLOCK_SIZE + 1, 1);
    int rank;
    runBcfp(A, P, rank);
    checkResults(A, P, rank, 7);
}


TEST_F(BcfpTest, MultibleBlocks)
{
    mat A(5*BLOCK_SIZE + 3, 4*BLOCK_SIZE + 7, fill::randn);
    Mat<int> P(4*BLOCK_SIZE + 7, 1);
    int rank;
    runBcfp(A, P, rank);
    checkResults(A, P, rank, 4*BLOCK_SIZE + 7);
}


TEST_F(BcfpTest, MultibleBlocksReducedRank)
{
    mat A = randn<mat>(5*BLOCK_SIZE + 3, 13) *
            randn<mat>(13, 4*BLOCK_SIZE + 7);
    Mat<int> P(4*BLOCK_SIZE + 7, 1);
    int rank;
    runBcfp(A, P, rank);
    checkResults(A, P, rank, 13);
}


TEST_F(BcfpTest, SingleBlock_SP)
{
    fmat A(2*BLOCK_SIZE, 2*BLOCK_SIZE, fill::randn);
    Mat<int> P(2*BLOCK_SIZE + 1, 1);
    int rank;
    runBcfp(A, P, rank);
    checkResults(A, P, rank, 2*BLOCK_SIZE);
}


TEST_F(BcfpTest, SingleBlockReducedRank_SP)
{
    fmat A = randn<fmat>(2*BLOCK_SIZE, 7) * randn<fmat>(7, 2*BLOCK_SIZE);
    Mat<int> P(2*BLOCK_SIZE + 1, 1);
    int rank;
    runBcfp(A, P, rank);
    checkResults(A, P, rank, 7);
}


TEST_F(BcfpTest, MultibleBlocks_SP)
{
    fmat A(5*BLOCK_SIZE + 3, 4*BLOCK_SIZE + 7, fill::randn);
    Mat<int> P(4*BLOCK_SIZE + 7, 1);
    int rank;
    runBcfp(A, P, rank);
    checkResults(A, P, rank, 4*BLOCK_SIZE + 7);
}


TEST_F(BcfpTest, MultibleBlocksReducedRank_SP)
{
    fmat A = randn<fmat>(5*BLOCK_SIZE + 3, 13) *
             randn<fmat>(13, 4*BLOCK_SIZE + 7);
    Mat<int> P(4*BLOCK_SIZE + 7, 1);
    int rank;
    runBcfp(A, P, rank);
    checkResults(A, P, rank, 13);
}


}
