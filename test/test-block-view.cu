#include <gtest/gtest.h>
#include <armadillo>
#include <algorithm>
#include <limits>


#include <base_defs.h>
#include <test/utils.cuh>
#include <test/block_view.h>


using namespace arma;
using namespace gpg;


namespace {


class BlockViewTest : public ::testing::Test {
protected:
    const double DEPS;
    BlockViewTest() : DEPS(std::numeric_limits<double>::epsilon())
    {
        arma_rng::set_seed(10101);
    }
};


TEST_F(BlockViewTest, SingleBlock)
{
    mat A(10, 10, fill::randn);
    BlockView<mat> V(A);
    V.rblocks.add(10);
    V.cblocks.add(10);
    EXPECT_PRED3(matricesNear<double>, A, V(0, 0), DEPS);
}


TEST_F(BlockViewTest, TwoByTwo)
{
    mat A(10, 10, fill::randn);
    BlockView<mat> V(A);
    V.rblocks.add(5, 2);
    V.cblocks.add(5, 2);

    EXPECT_PRED3(matricesNear<double>, A.submat(0, 0, 4, 4), V(0, 0), DEPS);
    EXPECT_PRED3(matricesNear<double>, A.submat(5, 0, 9, 4), V(1, 0), DEPS);
    EXPECT_PRED3(matricesNear<double>, A.submat(0, 5, 4, 9), V(0, 1), DEPS);
    EXPECT_PRED3(matricesNear<double>, A.submat(5, 5, 9, 9), V(1, 1), DEPS);
}


TEST_F(BlockViewTest, MultipleBlocks)
{
    mat A(5*BLOCK_SIZE + 3, 7*BLOCK_SIZE + 7, fill::randn);
    BlockView<mat> V(A);
    V.rblocks.add(3).add(BLOCK_SIZE, 5);
    V.cblocks.add(7).add(BLOCK_SIZE, 7);

    for (int i = 0; i < V.n_rows(); ++i) {
        for (int j = 0; j < V.n_cols(); ++j) {
            int br = std::max<int>(0, (i-1)*BLOCK_SIZE + 3);
            int bc = std::max<int>(0, (j-1)*BLOCK_SIZE + 7);
            mat Ap = A(span(br, i*BLOCK_SIZE + 2), span(bc, j*BLOCK_SIZE + 6));
            EXPECT_PRED3(matricesNear<double>, Ap, V(i, j), DEPS);
        }
    }
}


TEST_F(BlockViewTest, ViewModificationTest)
{
    mat A(10, 10, fill::zeros);
    BlockView<mat> V(A);
    V.rblocks.add(5, 2);
    V.cblocks.add(7).add(3);

    V(0, 0) = ones<mat>(5, 7);
    V(1, 1) = ones<mat>(5, 3);

    EXPECT_PRED3(matricesNear<double>, A.submat(0, 0, 4, 6),
                 ones<mat>(5, 7), DEPS);
    EXPECT_PRED3(matricesNear<double>, A.submat(5, 0, 9, 6),
                 zeros<mat>(5, 7), DEPS);
    EXPECT_PRED3(matricesNear<double>, A.submat(0, 7, 4, 9),
                 zeros<mat>(5, 3), DEPS);
    EXPECT_PRED3(matricesNear<double>, A.submat(5, 7, 9, 9),
                 ones<mat>(5, 3), DEPS);
}


}

