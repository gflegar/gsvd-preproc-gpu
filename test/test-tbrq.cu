#include <gtest/gtest.h>
#include <armadillo>
#include <algorithm>
#include <limits>


#include <utils.cuh>
#include <dmat.cuh>
#include <test/utils.cuh>
#include <test/block_view.h>
#include <base_defs.h>
#include <sbrq.cuh>
#include <tbrq.cuh>


using namespace arma;
using namespace gpg;


namespace {


class TbrqTest : public ::testing::Test {
protected:
    TbrqTest() { arma_rng::set_seed(141414); }

    template <typename T>
    void prepareMatrix(Mat<T> &A)
    {
        const dim3 qs = sizes::sbrq_q<BLOCK_SIZE>(A.n_rows, A.n_cols);
        Mat<T> Q(qs.x, qs.y, fill::zeros);
        T *d_A, *d_Q;
        int lda, ldq;
        createOnDevice(A, d_A, lda);
        createOnDevice(Q, d_Q, ldq);
        sbrq(DMat<T>(d_A, A.n_rows, A.n_cols, lda),
             DMat<T>(d_Q, Q.n_rows, Q.n_cols, ldq),
             0);
        moveFromDevice(A, d_A, lda);
    }

    template<typename T>
    void runTbrq(const Mat<T> &A, Mat<T> &Q, Mat<T> &R, int offset = 1)
    {
        const dim3 qs = sizes::tbrq_q<BLOCK_SIZE>(A.n_rows, A.n_cols);
        Q = zeros<Mat<T> >(qs.x, qs.y);
        R = zeros<Mat<T> >(size(A));
        T *d_A, *d_Q;
        int lda, ldq;
        createOnDevice(A, d_A, lda);
        createOnDevice(Q, d_Q, ldq);
        tbrq(DMat<T>(d_A, A.n_rows, A.n_cols, lda),
             DMat<T>(d_Q, Q.n_rows, Q.n_cols, ldq),
             offset, 0);
        moveFromDevice(R, d_A, lda);
        moveFromDevice(Q, d_Q, ldq);
    }

    template<typename T>
    void checkResults(const Mat<T> &A, const Mat<T> &Q, const Mat<T> &R,
                      int offset = 1, 
                      T eps = std::numeric_limits<T>::epsilon())
    {
        BlockView<const Mat<T> > Ab(A);
        Ab.rblocks.add(A.n_rows % BLOCK_SIZE)
                  .add(BLOCK_SIZE, A.n_rows / BLOCK_SIZE);
        Ab.cblocks.add(A.n_cols % BLOCK_SIZE)
                  .add(BLOCK_SIZE, A.n_cols / BLOCK_SIZE);
        BlockView<const Mat<T> > Qb(Q);
        Qb.rblocks.add(2*BLOCK_SIZE, Q.n_rows / (2*BLOCK_SIZE));
        Qb.cblocks.add(Q.n_cols % BLOCK_SIZE)
                  .add(BLOCK_SIZE, Q.n_cols / BLOCK_SIZE);
        BlockView<const Mat<T> > Rb(R);
        Rb.rblocks.add(R.n_rows % BLOCK_SIZE)
                  .add(BLOCK_SIZE, R.n_rows / BLOCK_SIZE);
        Rb.cblocks.add(R.n_cols % BLOCK_SIZE)
                  .add(BLOCK_SIZE, R.n_cols / BLOCK_SIZE);

        for (int i = 0; i < Ab.n_rows(); ++i) {
            for (int j = Ab.n_cols() - 1; j >= 0; j -= 2*offset) {
                Mat<T> Rp = Rb(i, j);
                Mat<T> Ap = Ab(i, j);
                Mat<T> Qp = Qb(i, j);
                if (j - offset >= 0) {
                    Rp.insert_cols(0, Rb(i, j-offset));
                    Ap.insert_cols(0, Mat<T>(Ab(i, j-offset)));
                    Qp.insert_cols(0 ,Mat<T>(Qb(i, j-offset)));
                }
                Mat<T> I = eye<Mat<T> >(Qp.n_cols, Qp.n_cols);
                EXPECT_PRED3(matricesNear<T>, I, Qp.t()*Qp, eps)
                    << "(" << i << "," << j
                    << ")-th submatrix of Q is not orthogonal";
                EXPECT_PRED3(matricesNear<T>,
                             upperTriangle(Rp, Rp.n_cols - Rp.n_rows -1),
                             Rp, eps)
                    << "(" << i << "," << j
                    << ")-th submatrix of Q is not triangular";
                Qp = Qp.tail_rows(Qp.n_cols);

                EXPECT_PRED3(matricesNear<T>, Ap, Rp*Qp.t(), eps)
                    << "RQ != A for submatrices at (" << i << "," << j
                    << ") and (" << i << "," << j-offset << ")";
            }
        }
    }
};


TEST_F(TbrqTest, SingleBlockNoOffset)
{
    mat A = randn<mat>(2*BLOCK_SIZE, BLOCK_SIZE);
    mat Q;
    mat R;
    prepareMatrix(A);
    runTbrq(A, Q, R);
    checkResults(A, Q, R);
}


TEST_F(TbrqTest, MultipleBlocksNoOffset)
{
    mat A = randn<mat>(4*BLOCK_SIZE + 13, 2*BLOCK_SIZE + 3);
    mat Q;
    mat R;
    prepareMatrix(A);
    runTbrq(A, Q, R);
    checkResults(A, Q, R);
}


TEST_F(TbrqTest, MultipleBlocksWithOffset)
{
    mat A = randn<mat>(4*BLOCK_SIZE + 13, 2*BLOCK_SIZE + 3);
    mat Q;
    mat R;
    prepareMatrix(A);
    runTbrq(A, Q, R);
    checkResults(A, Q, R);
    A = R;
    Q.zeros();
    R.zeros();
    runTbrq(A, Q, R, 2);
    checkResults(A, Q, R, 2);
}


TEST_F(TbrqTest, SingleBlockNoOffset_SP)
{
    fmat A = randn<fmat>(2*BLOCK_SIZE, BLOCK_SIZE);
    fmat Q;
    fmat R;
    prepareMatrix(A);
    runTbrq(A, Q, R);
    checkResults(A, Q, R);
}


TEST_F(TbrqTest, MultipleBlocksNoOffset_SP)
{
    fmat A = randn<fmat>(4*BLOCK_SIZE + 13, 2*BLOCK_SIZE + 3);
    fmat Q;
    fmat R;
    prepareMatrix(A);
    runTbrq(A, Q, R);
    checkResults(A, Q, R);
}


TEST_F(TbrqTest, MultipleBlocksWithOffset_SP)
{
    fmat A = randn<fmat>(4*BLOCK_SIZE + 13, 2*BLOCK_SIZE + 3);
    fmat Q;
    fmat R;
    prepareMatrix(A);
    runTbrq(A, Q, R);
    checkResults(A, Q, R);
    A = R;
    Q.zeros();
    R.zeros();
    runTbrq(A, Q, R, 2);
    checkResults(A, Q, R, 2);
}


}
