#include <gtest/gtest.h>
#include <armadillo>
#include <algorithm>
#include <limits>


#include <utils.cuh>
#include <test/utils.cuh>
#include <dmat.cuh>
#include <base_defs.h>
#include <sbqr.cuh>
#include <tbqr.cuh>


using namespace arma;
using namespace gpg;
using namespace std;


namespace {


class TbqrTest : public ::testing::Test {
protected:
    TbqrTest() { arma_rng::set_seed(141414); }

    template <typename T>
    void prepareMatrix(Mat<T> &A)
    {
        dim3 qs = sizes::sbqr_q<BLOCK_SIZE>(A.n_rows, A.n_cols);
        Mat<T> Q(qs.x, qs.y, fill::zeros);
        T *d_A, *d_Q;
        int lda, ldq;
        createOnDevice(A, d_A, lda);
        createOnDevice(Q, d_Q, ldq);
        sbqr(DMat<T>(d_A, A.n_rows, A.n_cols, lda),
             DMat<T>(d_Q, Q.n_rows, Q.n_cols, ldq),
             0);
        moveFromDevice(A, d_A, lda);
    }

    template<typename T>
    void runTbqr(const Mat<T> &A, Mat<T> &Q, Mat<T> &R, int offset = 1)
    {
        dim3 qs = sizes::tbqr_q<BLOCK_SIZE>(A.n_rows, A.n_cols);
        Q = zeros<Mat<T> >(qs.x, qs.y);
        R = zeros<Mat<T> >(size(A));
        T *d_A, *d_Q;
        int lda, ldq;
        createOnDevice(A, d_A, lda);
        createOnDevice(Q, d_Q, ldq);
        tbqr(DMat<T>(d_A, A.n_rows, A.n_cols, lda),
             DMat<T>(d_Q, Q.n_rows, Q.n_cols, ldq),
             offset, 0);
        moveFromDevice(R, d_A, lda);
        moveFromDevice(Q, d_Q, ldq);
    }

    template<typename T>
    void checkResults(const Mat<T> &A, const Mat<T> &Q, const Mat<T> &R,
                      int offset = 1, 
                      T eps = std::numeric_limits<T>::epsilon())
    {
        const int ofs  = offset * BLOCK_SIZE;
        for (int i = 0; i < A.n_rows; i += 2*ofs) {
            for (int j = 0; j < A.n_cols; j += BLOCK_SIZE) {
                int r1 = std::min<int>(BLOCK_SIZE, A.n_rows - i);
                int r2 = std::min<int>(BLOCK_SIZE, A.n_rows - i - ofs);
                int c = std::min<int>(BLOCK_SIZE, A.n_cols - j);
                span rows1(i, i + r1 - 1);
                span rows2(i + ofs, i + ofs + r2 - 1);
                span cols(j, j + c - 1);
                span qcols(2*j, 2*j + 2*BLOCK_SIZE - 1);
                Mat<T> Ap(r1 + std::max(0, r2), c);
                Mat<T> Qp(r1 + std::max(0, r2), 2*BLOCK_SIZE);
                Mat<T> Rp(r1 + std::max(0, r2), c);
                Ap(span(0, r1 - 1), span::all) = A(rows1, cols);
                Qp(span(0, r1 - 1), span::all) = Q(rows1, qcols);
                Rp(span(0, r1 - 1), span::all) = R(rows1, cols);
                if (r2 > 0) {
                    Ap(span(r1, r1 + r2 - 1), span::all) = A(rows2, cols);
                    Qp(span(r1, r1 + r2 - 1), span::all) = Q(rows2, qcols);
                    Rp(span(r1, r1 + r2 - 1), span::all) = R(rows2, cols);
                }
                Mat<T> I = eye<Mat<T> >(Qp.n_rows, Qp.n_rows);
                EXPECT_PRED3(matricesNear<T>, I, Qp*Qp.t(), eps)
                    << "Submatrix of Q starting at (" << i << "," << 2*j
                    << ") and (" << i+ofs << "," << 2*j
                    << ") is not orthogonal";
                EXPECT_PRED3(matricesNear<T>, upperTriangle(Rp), Rp, eps) 
                    << "Submatrix of R starting at (" << i << "," << j
                    << ") and (" << i+ofs << "," << j
                    << ") is not otriangular";
                Qp = Qp.head_cols(Qp.n_rows);
                EXPECT_PRED3(matricesNear<T>, Ap, Qp.t()*Rp, eps)
                    << "QR != A for sumatrices at (" << i << "," << j
                    << ") and (" << i+ofs << "," << j << ")";
            }
        }
    }
};


TEST_F(TbqrTest, SingleBlockNoOffset)
{
    mat A = randn<mat>(2*BLOCK_SIZE, BLOCK_SIZE);
    mat Q;
    mat R;
    prepareMatrix(A);
    runTbqr(A, Q, R);
    checkResults(A, Q, R);
}


TEST_F(TbqrTest, MultipleBlocksNoOffset)
{
    mat A = randn<mat>(4*BLOCK_SIZE + 13, 2*BLOCK_SIZE + 3);
    mat Q;
    mat R;
    prepareMatrix(A);
    runTbqr(A, Q, R);
    checkResults(A, Q, R);
}


TEST_F(TbqrTest, MultipleBlocksWithOffset)
{
    mat A = randn<mat>(4*BLOCK_SIZE + 13, 2*BLOCK_SIZE + 3);
    mat Q;
    mat R;
    prepareMatrix(A);
    runTbqr(A, Q, R);
    checkResults(A, Q, R);
    A = R;
    Q.zeros();
    R.zeros();
    runTbqr(A, Q, R, 2);
    checkResults(A, Q, R, 2);
}


TEST_F(TbqrTest, SingleBlockNoOffset_SP)
{
    fmat A = randn<fmat>(2*BLOCK_SIZE, BLOCK_SIZE);
    fmat Q;
    fmat R;
    prepareMatrix(A);
    runTbqr(A, Q, R);
    checkResults(A, Q, R);
}


TEST_F(TbqrTest, MultipleBlocksNoOffset_SP)
{
    fmat A = randn<fmat>(4*BLOCK_SIZE + 13, 2*BLOCK_SIZE + 3);
    fmat Q;
    fmat R;
    prepareMatrix(A);
    runTbqr(A, Q, R);
    checkResults(A, Q, R);
}


TEST_F(TbqrTest, MultipleBlocksWithOffset_SP)
{
    fmat A = randn<fmat>(4*BLOCK_SIZE + 13, 2*BLOCK_SIZE + 3);
    fmat Q;
    fmat R;
    prepareMatrix(A);
    runTbqr(A, Q, R);
    checkResults(A, Q, R);
    A = R;
    Q.zeros();
    R.zeros();
    runTbqr(A, Q, R, 2);
    checkResults(A, Q, R, 2);
}


}
