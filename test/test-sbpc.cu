#include <gtest/gtest.h>
#include <armadillo>
#include <algorithm>
#include <limits>


#include <utils.cuh>
#include <dmat.cuh>
#include <test/utils.cuh>
#include <base_defs.h>
#include <sbpc.cuh>



using namespace arma;
using namespace gpg;


namespace {


class SbpcTest : public ::testing::Test {
protected:
    SbpcTest() { arma_rng::set_seed(222222); }

    Col<int> count(int n)
    {
        Col<int> c(n);
        for (int i = 0; i < n; ++i) {
            c(i) = i;
        }
        return c;
    }

    template <typename T>
    uvec toUvec(Mat<T> M) {
        uvec r(M.n_rows);
        for (int i = 0 ; i< M.n_rows; ++i) {
            r(i) = M(i, 0);
        }
        return r;
    }

    template <typename T>
    void runSbpc(const Mat<T> &A, const Mat<int> &P, Mat<int> &O,
                 Mat<int> &rank, T eps = std::numeric_limits<T>::epsilon())
    {
        Mat<T> R(size(A), fill::zeros);
        rank.zeros(1, 1);
        int blocks = rud(P.n_elem, 2*BLOCK_SIZE);
        int lb = P.n_elem - 2*BLOCK_SIZE*(blocks-1);
        O.resize(BLOCK_SIZE * (blocks - 1) + std::min<int>(BLOCK_SIZE, lb), 1);
        T *d_A;
        int *d_P, *d_O, *d_rank;
        int lda, ldp, ldo, ldr;
        createOnDevice(A, d_A, lda);
        createOnDevice(P, d_P, ldp);
        createOnDevice(O, d_O, ldo);
        createOnDevice(rank, d_rank, ldr);
        sbpc(DMat<T>(d_A, A.n_rows, A.n_cols, lda), d_P, d_O, d_rank,
             eps*norm(A, "fro") / A.n_elem * BLOCK_SIZE * BLOCK_SIZE, 0);
        moveFromDevice(R, d_A, lda);
        moveFromDevice(O, d_O, ldo);
        moveFromDevice(rank, d_rank, ldr);
    }

    template <typename T>
    void checkResults(const Mat<T> &A, const Mat<int> &P, const Mat<int> &O,
                      const Mat<int> &rk,
                      T eps = std::numeric_limits<T>::epsilon())
    {
        Mat<T> Ao = A.cols(toUvec(O));
        Mat<T> Ap = A.cols(toUvec(P));
        for (int i = 0; i < Ao.n_cols; i += BLOCK_SIZE) {
            int s1 = std::min<int>(BLOCK_SIZE, Ao.n_cols - i);
            int s2 = std::min<int>(2*BLOCK_SIZE, Ap.n_cols - 2*i);
            int r1 = rank(Ao.cols(i, i + s1 - 1));
            int r2 = rank(Ap.cols(2*i, 2*i + s2 - 1));
            EXPECT_EQ(std::min<int>(r2, s1), r1)
                << "Ranks starting at position " << i << "don't match.";
            EXPECT_EQ(std::min<int>(rk(i/BLOCK_SIZE, 0), s1), r1)
                << "Ranks starting at position " << i << " don't match the "
                << "actual rank.";
        }
    }
};


TEST_F(SbpcTest, SingleBlockFullRank)
{
    mat A(2*BLOCK_SIZE, 2*BLOCK_SIZE, fill::randn);
    Mat<int> P(count(2*BLOCK_SIZE));
    Mat<int> O;
    Mat<int> rank;
    runSbpc(mat(A.cols(toUvec(P))), P, O, rank);
    rank(0, 0) = 2*BLOCK_SIZE;
    checkResults(A, P, O, rank);
}


TEST_F(SbpcTest, SingleBlockReducedRank)
{
    mat A = randn<mat>(2*BLOCK_SIZE, 7) * randn<mat>(7, 2*BLOCK_SIZE);
    Mat<int> P(count(2*BLOCK_SIZE));
    Mat<int> O;
    Mat<int> rank;
    runSbpc(mat(A.cols(toUvec(P))), P, O, rank);
    rank(0, 0) = 7;
    checkResults(A, P, O, rank);
}


TEST_F(SbpcTest, SingleBlockPermuted)
{
    mat A = randn<mat>(2*BLOCK_SIZE, 7) * randn<mat>(7, 2*BLOCK_SIZE);
    Mat<int> P(shuffle(count(2*BLOCK_SIZE)));
    Mat<int> O;
    Mat<int> rank;
    runSbpc(mat(A.cols(toUvec(P))), P, O, rank);
    rank(0, 0) = 7;
    checkResults(A, P, O, rank);
}


TEST_F(SbpcTest, MultipleBlocks)
{
    mat A(2*BLOCK_SIZE, 7*BLOCK_SIZE + 5);
    Mat<int> rank(4, 1);
    for (int i = 0; i < A.n_cols; i += 2*BLOCK_SIZE) {
        int sz = std::min<int>(2*BLOCK_SIZE, A.n_cols - i);
        int cr = randi<imat>(1, 1, distr_param(1, sz))(0, 0);
        rank(i / (2*BLOCK_SIZE), 0) = cr;
        A(span::all, span(i, i + sz - 1)) =
            randn<mat>(A.n_rows, cr) * randn<mat>(cr, sz);
    }
    Mat<int> P(count(A.n_cols));
    Mat<int> O;
    Mat<int> tmp;
    runSbpc(mat(A.cols(toUvec(P))), P, O, tmp);
    checkResults(A, P, O, rank);
}


TEST_F(SbpcTest, MultipleBlocksPermuted)
{
    mat A(BLOCK_SIZE + 3, 7*BLOCK_SIZE + 5);
    Mat<int> P(shuffle(count(A.n_cols)));
    Mat<int> rank(4, 1);
    for (int i = 0; i < A.n_cols; i += 2*BLOCK_SIZE) {
        int sz = std::min<int>(2*BLOCK_SIZE, A.n_cols - i);
        int cr = randi<imat>(1, 1, distr_param(1, std::min<int>(sz, A.n_rows)))
                 (0, 0);
        rank(i / (2*BLOCK_SIZE), 0) = cr;
        A.cols(toUvec(Mat<int>(P(span(i, i + sz - 1), span::all)))) =
            randn<mat>(A.n_rows, cr) * randn<mat>(cr, sz);
    }
    Mat<int> O;
    Mat<int> tmp;
    runSbpc(mat(A.cols(toUvec(P))), P, O, tmp);
    checkResults(A, P, O, rank);
}


TEST_F(SbpcTest, SingleBlockFullRank_SP)
{
    fmat A(2*BLOCK_SIZE, 2*BLOCK_SIZE, fill::randn);
    Mat<int> P(count(2*BLOCK_SIZE));
    Mat<int> O;
    Mat<int> rank;
    runSbpc(fmat(A.cols(toUvec(P))), P, O, rank);
    rank(0, 0) = 2*BLOCK_SIZE;
    checkResults(A, P, O, rank);
}


TEST_F(SbpcTest, SingleBlockReducedRank_SP)
{
    fmat A = randn<fmat>(2*BLOCK_SIZE, 7) * randn<fmat>(7, 2*BLOCK_SIZE);
    Mat<int> P(count(2*BLOCK_SIZE));
    Mat<int> O;
    Mat<int> rank;
    runSbpc(fmat(A.cols(toUvec(P))), P, O, rank);
    rank(0, 0) = 7;
    checkResults(A, P, O, rank);
}


TEST_F(SbpcTest, SingleBlockPermuted_SP)
{
    fmat A = randn<fmat>(2*BLOCK_SIZE, 7) * randn<fmat>(7, 2*BLOCK_SIZE);
    Mat<int> P(shuffle(count(2*BLOCK_SIZE)));
    Mat<int> O;
    Mat<int> rank;
    runSbpc(fmat(A.cols(toUvec(P))), P, O, rank);
    rank(0, 0) = 7;
    checkResults(A, P, O, rank);
}


TEST_F(SbpcTest, MultipleBlocks_SP)
{
    fmat A(2*BLOCK_SIZE, 7*BLOCK_SIZE + 5);
    Mat<int> rank(4, 1);
    for (int i = 0; i < A.n_cols; i += 2*BLOCK_SIZE) {
        int sz = std::min<int>(2*BLOCK_SIZE, A.n_cols - i);
        int cr = randi<imat>(1, 1, distr_param(1, sz))(0, 0);
        rank(i / (2*BLOCK_SIZE), 0) = cr;
        A(span::all, span(i, i + sz - 1)) =
            randn<fmat>(A.n_rows, cr) * randn<fmat>(cr, sz);
    }
    Mat<int> P(count(A.n_cols));
    Mat<int> O;
    Mat<int> tmp;
    runSbpc(fmat(A.cols(toUvec(P))), P, O, tmp);
    checkResults(A, P, O, rank);
}


TEST_F(SbpcTest, MultipleBlocksPermuted_SP)
{
    fmat A(BLOCK_SIZE + 3, 7*BLOCK_SIZE + 5);
    Mat<int> P(shuffle(count(A.n_cols)));
    Mat<int> rank(4, 1);
    for (int i = 0; i < A.n_cols; i += 2*BLOCK_SIZE) {
        int sz = std::min<int>(2*BLOCK_SIZE, A.n_cols - i);
        int cr = randi<imat>(1, 1, distr_param(1, std::min<int>(sz, A.n_rows)))
                 (0, 0);
        rank(i / (2*BLOCK_SIZE), 0) = cr;
        A.cols(toUvec(Mat<int>(P(span(i, i + sz - 1), span::all)))) =
            randn<fmat>(A.n_rows, cr) * randn<fmat>(cr, sz);
    }
    Mat<int> O;
    Mat<int> tmp;
    runSbpc(fmat(A.cols(toUvec(P))), P, O, tmp);
    checkResults(A, P, O, rank);
}


}
