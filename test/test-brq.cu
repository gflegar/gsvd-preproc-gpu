#include <gtest/gtest.h>
#include <armadillo>
#include <algorithm>
#include <limits>
#include <cmath>


#include <utils.cuh>
#include <test/utils.cuh>
#include <base_defs.h>
#include <brq.cuh>


using namespace arma;
using namespace gpg;


namespace {


class BrqTest : public ::testing::Test {
protected:
    BrqTest() { arma_rng::set_seed(181818); }

    template <typename T>
    void runBrq(const Mat<T> &A, const Mat<T> &Q, Mat<T> &R, Mat<T> &Q2)
    {
        const dim3 ws = sizes::brq_w<BLOCK_SIZE>(A.n_rows, A.n_cols);
        R.zeros(size(A));
        Q2.zeros(size(Q));
        Mat<T> W(ws.x, ws.y, fill::zeros);
        T *d_A, *d_Q, *d_W;
        int lda, ldq, ldw;
        createOnDevice(A, d_A, lda);
        createOnDevice(Q, d_Q, ldq);
        createOnDevice(W, d_W, ldw);
        DMat<T> DA(d_A, A.n_rows, A.n_cols, lda);
        cudaStream_t astream;
        cudaStreamCreate(&astream);
        std::vector<DMat<T> > Qs(1, DMat<T>(d_Q, Q.n_rows, Q.n_cols, ldq));
        std::vector<cudaStream_t> qstreams(1);
        cudaStreamCreate(&qstreams[0]);
        DMat<T> work(d_W, W.n_rows, W.n_cols, ldw);

        brq(DA, astream, Qs, qstreams, work);

        cudaAssert(cudaDeviceSynchronize());

        moveFromDevice(R, d_A, lda);
        moveFromDevice(Q2, d_Q, ldq);

        cudaStreamDestroy(astream);
        cudaStreamDestroy(qstreams[0]);
    }

    template <typename T>
    void checkResults(const Mat<T> &A, const Mat<T> &Q, const Mat<T> &R,
                      const Mat<T> &Q2,
                      T eps = std::numeric_limits<T>::epsilon())
    {
        EXPECT_PRED3(matricesNear<T>,
                     upperTriangle(R, R.n_cols - R.n_rows - 1),
                     R, eps)
            << "R factor is not triangular";
        EXPECT_PRED3(matricesNear<T>, Q*Q.t(),
                     Q2*Q2.t(), eps)
            << "Q2 factor is not orthogonal";
        EXPECT_PRED3(matricesNear<T>, A*Q.t(), R*Q2.t(), eps)
            << "A*Q' != R*Q2'";
    }
};


TEST_F(BrqTest, SingleBlock)
{
    mat A(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    mat Q, R, Q2;
    qr(Q, R, randn<mat>(BLOCK_SIZE, BLOCK_SIZE));
    runBrq(A, Q, R, Q2);
    checkResults(A, Q, R, Q2);
}


TEST_F(BrqTest, SingleBlockRow)
{
    mat A(BLOCK_SIZE, 5*BLOCK_SIZE + 7, fill::randn);
    mat Q, R, Q2;
    qr(Q, R, randn<mat>(5*BLOCK_SIZE + 7, 5*BLOCK_SIZE + 7));
    runBrq(A, Q, R, Q2);
    checkResults(A, Q, R, Q2);
}


TEST_F(BrqTest, MultipleBlocks)
{
    mat A(5*BLOCK_SIZE + 7, 3*BLOCK_SIZE + 4, fill::randn);
    mat Q, R, Q2;
    qr(Q, R, randn<mat>(3*BLOCK_SIZE + 4, 3*BLOCK_SIZE + 4));
    runBrq(A, Q, R, Q2);
    checkResults(A, Q, R, Q2);
}


TEST_F(BrqTest, MultipleBlocksNonSquareQ)
{
    mat A(5*BLOCK_SIZE +7, 3*BLOCK_SIZE + 4, fill::randn);
    mat Q, R, Q2;
    qr_econ(Q, R, randn<mat>(5*BLOCK_SIZE + 7, 3*BLOCK_SIZE + 4));
    runBrq(A, Q, R, Q2);
    checkResults(A, Q, R, Q2);
}


TEST_F(BrqTest, SingleBlock_SP)
{
    fmat A(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    fmat Q, R, Q2;
    qr(Q, R, randn<fmat>(BLOCK_SIZE, BLOCK_SIZE));
    runBrq(A, Q, R, Q2);
    checkResults(A, Q, R, Q2);
}


TEST_F(BrqTest, SingleBlockRow_SP)
{
    fmat A(BLOCK_SIZE, 5*BLOCK_SIZE + 7, fill::randn);
    fmat Q, R, Q2;
    qr(Q, R, randn<fmat>(5*BLOCK_SIZE + 7, 5*BLOCK_SIZE + 7));
    runBrq(A, Q, R, Q2);
    checkResults(A, Q, R, Q2);
}


TEST_F(BrqTest, MultipleBlocks_SP)
{
    fmat A(5*BLOCK_SIZE + 7, 3*BLOCK_SIZE + 4, fill::randn);
    fmat Q, R, Q2;
    qr(Q, R, randn<fmat>(3*BLOCK_SIZE + 4, 3*BLOCK_SIZE + 4));
    runBrq(A, Q, R, Q2);
    checkResults(A, Q, R, Q2);
}


TEST_F(BrqTest, MultipleBlocksNonSquareQ_SP)
{
    fmat A(5*BLOCK_SIZE +7, 3*BLOCK_SIZE + 4, fill::randn);
    fmat Q, R, Q2;
    qr_econ(Q, R, randn<fmat>(7*BLOCK_SIZE + 3, 3*BLOCK_SIZE + 4));
    runBrq(A, Q, R, Q2);
    checkResults(A, Q, R, Q2);
}


}
