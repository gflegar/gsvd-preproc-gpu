#include <gtest/gtest.h>
#include <armadillo>
#include <algorithm>


#include <utils.cuh>
#include <dmat.cuh>
#include <test/utils.cuh>
#include <base_defs.h>
#include <sbqr.cuh>


using namespace arma;
using namespace gpg;


namespace {


class SbqrTest : public ::testing::Test {
protected:
    SbqrTest() { arma_rng::set_seed(131313); }

    template <typename T>
    void runSbqr(const Mat<T> &A, Mat<T> &Q, Mat<T> &R)
    {
        dim3 qs = sizes::sbqr_q<BLOCK_SIZE>(A.n_rows, A.n_cols);
        Q = zeros<Mat<T> >(qs.x, qs.y);
        R = zeros<Mat<T> >(size(A));
        T *d_A, *d_Q;
        int lda, ldq;
        createOnDevice(A, d_A, lda);
        createOnDevice(Q, d_Q, ldq);
        sbqr(DMat<T>(d_A, A.n_rows, A.n_cols, lda),
             DMat<T>(d_Q, Q.n_rows, Q.n_cols, ldq),
             0);
        moveFromDevice(R, d_A, lda);
        moveFromDevice(Q, d_Q, ldq);
    }

    template <typename T>
    void checkResults(const Mat<T> &A, const Mat<T> &Q, const Mat<T> &R,
                      T eps = std::numeric_limits<T>::epsilon())
    {
        for (int i = 0; i < A.n_rows; i += BLOCK_SIZE) {
            for (int j = 0; j < A.n_cols; j += BLOCK_SIZE) {
                span rows(i, std::min<int>(i+BLOCK_SIZE, A.n_rows)-1);
                span cols(j, std::min<int>(j+BLOCK_SIZE, A.n_cols)-1);
                Mat<T> Ap = A(rows, cols);
                Mat<T> Qp = Q(rows, span(j, j+BLOCK_SIZE-1));
                Mat<T> Rp = R(rows, cols);
                Mat<T> I = eye<Mat<T> >(Qp.n_rows, Qp.n_rows);
                EXPECT_PRED3(matricesNear<T>, I, Qp*Qp.t(), eps)
                    << "Submatrix of Q starting at (" << i << "," << j
                    << ") is not orthogonal";
                EXPECT_PRED3(matricesNear<T>, upperTriangle(Rp), Rp, eps)
                    << "Submatrix of R starting at (" << i << "," << j
                    << ") is not triangular";
                Rp = upperTriangle(Rp);
                Qp = Qp.head_cols(Qp.n_rows);
                EXPECT_PRED3(matricesNear<T>, Ap, Qp.t()*Rp, eps)
                    << "QR != A for submatrices at (" << i << "," << j <<")";
            }
        }
    }
};


TEST_F(SbqrTest, SingleBlock)
{
    mat A = randn<mat>(BLOCK_SIZE, BLOCK_SIZE);
    mat Q, R;
    runSbqr(A, Q, R);
    checkResults(A, Q, R);
}


TEST_F(SbqrTest, MultipleBlocks)
{
    mat A = randn<mat>(3*BLOCK_SIZE + 12, 2*BLOCK_SIZE + 15);
    mat Q, R;
    runSbqr(A, Q, R);
    checkResults(A, Q, R);
}


TEST_F(SbqrTest, Zeros)
{
    mat A = zeros<mat>(2*BLOCK_SIZE + 3, BLOCK_SIZE + 7);
    mat Q, R;
    runSbqr(A, Q, R);
    checkResults(A, Q, R);
}


TEST_F(SbqrTest, SingleBlock_SP)
{
    fmat A = randn<fmat>(BLOCK_SIZE, BLOCK_SIZE);
    fmat Q, R;
    runSbqr(A, Q, R);
    checkResults(A, Q, R);
}


TEST_F(SbqrTest, MultipleBlocks_SP)
{
    fmat A = randn<fmat>(3*BLOCK_SIZE + 12, 2*BLOCK_SIZE + 15);
    fmat Q, R;
    runSbqr(A, Q, R);
    checkResults(A, Q, R);
}


TEST_F(SbqrTest, Zeros_SP)
{
    fmat A = zeros<fmat>(2*BLOCK_SIZE + 3, BLOCK_SIZE + 7);
    fmat Q, R;
    runSbqr(A, Q, R);
    checkResults(A, Q, R);
}


}
