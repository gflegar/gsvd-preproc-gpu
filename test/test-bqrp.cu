#include <gtest/gtest.h>
#include <armadillo>
#include <algorithm>
#include <limits>
#include <cmath>


#include <utils.cuh>
#include <test/utils.cuh>
#include <base_defs.h>
#include <bqrp.cuh>


using namespace arma;
using namespace gpg;


namespace {


class BqrpTest : public ::testing::Test {
protected:
    BqrpTest() { arma_rng::set_seed(303030); }

    template <typename T>
    void runBqrp(const Mat<T> &A, const Mat<T> &Q, const Mat<T> &P, Mat<T> &R,
                 Mat<T> &Q2, Mat<T> &P2, int &rank,
                 T eps = std::numeric_limits<T>::epsilon())
    {
        const dim3 ws = sizes::bqrp_w<BLOCK_SIZE>(A.n_rows, A.n_cols);
        const dim3 iws = sizes::bqrp_iw<BLOCK_SIZE>(A.n_rows, A.n_cols);
        R.zeros(size(A));
        Q2.zeros(size(Q));
        P2.zeros(size(P));
        Mat<T> W(ws.x, ws.y, fill::zeros);
        Mat<int> IW(iws.x, 1, fill::zeros);
        T *d_A, *d_Q, *d_W, *d_P;
        int *d_IW;
        int lda, ldq, ldw, ldp, ldiw;
        createOnDevice(A, d_A, lda);
        createOnDevice(Q, d_Q, ldq);
        createOnDevice(W, d_W, ldw);
        createOnDevice(P, d_P, ldp);
        createOnDevice(IW, d_IW, ldiw);

        cudaStream_t s[3];
        for (int i = 0; i < 3; ++i) {
            cudaAssert(cudaStreamCreate(&s[i]));
        }

        std::vector<DMat<T> > Qs(1, DMat<T>(d_Q, Q.n_rows, Q.n_cols, ldq));
        DMat<T> work(d_W, W.n_rows, W.n_cols, ldw);

        bqrp(DMat<T>(d_A, A.n_rows, A.n_cols, lda), s[0],
             std::vector<DMat<T> >(1, DMat<T>(d_Q, Q.n_rows, Q.n_cols, ldq)),
             std::vector<cudaStream_t>(1, s[1]),
             std::vector<DMat<T> >(1, DMat<T>(d_P, P.n_rows, P.n_cols, ldp)),
             std::vector<cudaStream_t>(1, s[2]),
             DMat<T>(d_W, W.n_rows, W.n_cols, ldw), d_IW, rank,
             eps * norm(A, "fro") * std::max(A.n_rows, A.n_cols));

        cudaAssert(cudaDeviceSynchronize());

        moveFromDevice(R, d_A, lda);
        moveFromDevice(Q2, d_Q, ldq);
        moveFromDevice(P2, d_P, ldp);

        for (int i = 0; i < 3; ++i) {
            cudaAssert(cudaStreamDestroy(s[i]));
        }
    }

    template <typename T>
    void checkResults(const Mat<T> &A, const Mat<T> &Q, const Mat<T> &P,
                      const Mat<T> &R, const Mat<T> &Q2, const Mat<T> &P2,
                      int trank, int rank,
                      T eps = std::numeric_limits<T>::epsilon())
    {
        EXPECT_PRED3(matricesNear<T>, upperTriangle(R), R, eps)
            << "R factor is not triangular";
        EXPECT_PRED3(matricesNear<T>, Q.t() * Q, Q2.t()*Q2, eps)
            << "Q factor is not orthogonal";
        EXPECT_PRED3(matricesNear<T>, P * P.t(), P2 * P2.t(), eps)
            << "P factor is not orthogonal";
        EXPECT_PRED3(matricesNear<T>, Q.t() * A * P.t(), Q2.t() * R * P2.t(),
                     eps)
            << "Q' * A * P' != Q2' * R * P2'";
        EXPECT_EQ(trank, rank) << "Ranks don't match "
            << "P:\n" << P << "R:\n" << upperTriangle(R);
        /*
        std::cout << "Computed rank: " << rank << " True rank: " << trank
                  << std::endl;
                  */
    }
};


TEST_F(BqrpTest, SingleBlock)
{
    mat A(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    mat Q(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    mat P(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    mat R, Q2, P2;
    int rank;
    runBqrp(A, Q, P, R, Q2, P2, rank);
    checkResults(A, Q, P, R, Q2, P2, BLOCK_SIZE, rank);
}


TEST_F(BqrpTest, SingleBlockColumn)
{
    mat A = randn<mat>(5*BLOCK_SIZE + 7, 7) *
            randn<mat>(7, BLOCK_SIZE);
    mat Q(5*BLOCK_SIZE + 7, 3*BLOCK_SIZE + 2, fill::randn);
    mat P(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    mat R, Q2, P2;
    int rank;
    runBqrp(A, Q, P, R, Q2, P2, rank);
    checkResults(A, Q, P, R, Q2, P2, 7, rank);
}


TEST_F(BqrpTest, MultipleBlocksNoQP)
{
    mat A = randn<mat>(5*BLOCK_SIZE + 7, 17) *
            randn<mat>(17, 2*BLOCK_SIZE + 3);
    mat Q(5*BLOCK_SIZE + 7, 5*BLOCK_SIZE + 7, fill::eye);
    mat P(2*BLOCK_SIZE + 3, 2*BLOCK_SIZE + 3, fill::eye);
    mat R, Q2, P2;
    int rank;
    runBqrp(A, Q, P, R, Q2, P2, rank);
    checkResults(A, Q, P, R, Q2, P2, 17, rank);
}

TEST_F(BqrpTest, MultipleBlocks)
{
    mat A = randn<mat>(5*BLOCK_SIZE + 7, 17) *
            randn<mat>(17, 2*BLOCK_SIZE + 3);
    mat Q(5*BLOCK_SIZE + 7, 3*BLOCK_SIZE + 2, fill::randn);
    mat P(2*BLOCK_SIZE + 3, 2*BLOCK_SIZE + 3, fill::randn);
    mat R, Q2, P2;
    int rank;
    runBqrp(A, Q, P, R, Q2, P2, rank);
    checkResults(A, Q, P, R, Q2, P2, 17, rank);
}


TEST_F(BqrpTest, SingleBlock_SP)
{
    fmat A(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    fmat Q(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    fmat P(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    fmat R, Q2, P2;
    int rank;
    runBqrp(A, Q, P, R, Q2, P2, rank);
    checkResults(A, Q, P, R, Q2, P2, BLOCK_SIZE, rank);
}


TEST_F(BqrpTest, SingleBlockColumn_SP)
{
    fmat A = randn<fmat>(5*BLOCK_SIZE + 7, 7) *
             randn<fmat>(7, BLOCK_SIZE);
    fmat Q(5*BLOCK_SIZE + 7, 3*BLOCK_SIZE + 2, fill::randn);
    fmat P(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    fmat R, Q2, P2;
    int rank;
    runBqrp(A, Q, P, R, Q2, P2, rank);
    checkResults(A, Q, P, R, Q2, P2, 7, rank);
}


TEST_F(BqrpTest, MultipleBlocks_SP)
{
    fmat A = randn<fmat>(5*BLOCK_SIZE + 7, 17) *
             randn<fmat>(17, 2*BLOCK_SIZE + 3);
    fmat Q(5*BLOCK_SIZE + 7, 3*BLOCK_SIZE + 2, fill::randn);
    fmat P(2*BLOCK_SIZE + 3, 2*BLOCK_SIZE + 3, fill::randn);
    fmat R, Q2, P2;
    int rank;
    runBqrp(A, Q, P, R, Q2, P2, rank);
    checkResults(A, Q, P, R, Q2, P2, 17, rank);
}


}
