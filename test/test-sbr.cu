#include <gtest/gtest.h>
#include <armadillo>
#include <limits>


#include <base_defs.h>
#include <dmat.cuh>
#include <utils.cuh>
#include <test/utils.cuh>
#include <sbr.cuh>


using namespace gpg;
using namespace arma;


namespace {

class SbrTest : public ::testing::Test {
protected:
    SbrTest() { arma_rng::set_seed(191919); }

    Col<int> count(int n)
    {
        Col<int> c(n);
        for (int i = 0; i < n; ++i) {
            c(i) = i;
        }
        return c;
    }

    template <typename T>
    void runSbr(const  Mat<T> &A, const Col<int> &P, Mat<T> &R)
    {
        R.zeros(size(A));
        int lda, ldr, ldp;
        int *d_P;
        T *d_A, *d_R;
        createOnDevice(A, d_A, lda);
        createOnDevice(R, d_R, ldr);
        createOnDevice(Mat<int>(P), d_P, ldp);
        sbr(DMat<T>(d_A, A.n_rows, A.n_cols, lda),
            DMat<T>(d_R, A.n_rows, A.n_cols, ldr),
            d_P, 0);
        moveFromDevice(R, d_R, ldr);
    }

    template <typename T>
    void checkResults(const Mat<T> &A, const Col<int> &P, const Mat<T> &R,
                      T eps = std::numeric_limits<T>::epsilon())
    {
        uvec uP(size(P));
        for (int i = 0; i < uP.n_elem; ++i) {
            uP(i) = P(i);
        }
        Mat<T> Ap = A.cols(uP);
        for (int i = 0; i < A.n_rows; i += 2*BLOCK_SIZE) {
            for (int j = 0; j < A.n_cols; j += 2*BLOCK_SIZE) {
                span rs(i, std::min<int>(i + 2*BLOCK_SIZE, A.n_rows) - 1);
                span cs(j, std::min<int>(j + 2*BLOCK_SIZE, A.n_cols) - 1);
                Mat<T> As = Ap(rs, cs);
                Mat<T> Rs = R(rs, cs);
                EXPECT_PRED3(matricesNear<T>, upperTriangle(Rs), Rs, eps)
                    << "Block starting at (" << i << "," << j
                    << ") is not triangular";
                EXPECT_PRED3(matricesNear<T>, diagmat(As.t()*As),
                             diagmat(Rs.t()*Rs), eps)
                    << "Column norms of blocks starting at (" << i << "," << j
                    << ") do not match";
            }
        }
    }
};


TEST_F(SbrTest, SingleBlock)
{
    mat A(2*BLOCK_SIZE, 2*BLOCK_SIZE, fill::randn);
    Col<int> P = count(2*BLOCK_SIZE);
    mat R;
    runSbr(A, P, R);
    checkResults(A, P, R);
}


TEST_F(SbrTest, SingleBlockPermuted)
{
    mat A(2*BLOCK_SIZE, 2*BLOCK_SIZE, fill::randn);
    Col<int> P = shuffle(count(2*BLOCK_SIZE));
    mat R;
    runSbr(A, P, R);
    checkResults(A, P, R);
}


TEST_F(SbrTest, MultipleBlocks)
{
    mat A(11*BLOCK_SIZE + 7, 13*BLOCK_SIZE + 9, fill::randn);
    Col<int> P = count(13*BLOCK_SIZE + 9);
    mat R;
    runSbr(A, P, R);
    checkResults(A, P, R);
}


TEST_F(SbrTest, MultipleBlocksPermuted)
{
    mat A(11*BLOCK_SIZE + 7, 13*BLOCK_SIZE + 9, fill::randn);
    Col<int> P = shuffle(count(13*BLOCK_SIZE + 9));
    mat R;
    runSbr(A, P, R);
    checkResults(A, P, R);
}


TEST_F(SbrTest, SingleBlock_SP)
{
    fmat A(2*BLOCK_SIZE, 2*BLOCK_SIZE, fill::randn);
    Col<int> P = count(2*BLOCK_SIZE);
    fmat R;
    runSbr(A, P, R);
    checkResults(A, P, R);
}


TEST_F(SbrTest, SingleBlockPermuted_SP)
{
    fmat A(2*BLOCK_SIZE, 2*BLOCK_SIZE, fill::randn);
    Col<int> P = shuffle(count(2*BLOCK_SIZE));
    fmat R;
    runSbr(A, P, R);
    checkResults(A, P, R);
}


TEST_F(SbrTest, MultipleBlocks_SP)
{
    fmat A(11*BLOCK_SIZE + 7, 13*BLOCK_SIZE + 9, fill::randn);
    Col<int> P = count(13*BLOCK_SIZE + 9);
    fmat R;
    runSbr(A, P, R);
    checkResults(A, P, R);
}


TEST_F(SbrTest, MultipleBlocksPermuted_SP)
{
    fmat A(11*BLOCK_SIZE + 7, 13*BLOCK_SIZE + 9, fill::randn);
    Col<int> P = shuffle(count(13*BLOCK_SIZE + 9));
    fmat R;
    runSbr(A, P, R);
    checkResults(A, P, R);
}

}

