#include <gtest/gtest.h>
#include <armadillo>
#include <limits>


#include <base_defs.h>
#include <utils.cuh>
#include <test/utils.cuh>
#include <bcr.cuh>


using namespace gpg;
using namespace arma;


namespace {


class BcrTest : public ::testing::Test {
protected:
    BcrTest() { arma_rng::set_seed(212121); }

    Col<int> count(int n)
    {
        Col<int> c(n);
        for (int i = 0; i < n; ++i) {
            c(i) = i;
        }
        return c;
    }

    template <typename T>
    void runBcr(const Mat<T> &A, const Col<int> &P, Mat<T> &R)
    {
        R.zeros(size(A));
        int lda, ldp, ldr;
        int *d_P;
        T *d_A, *d_R;
        createOnDevice(A, d_A, lda);
        createOnDevice(Mat<int>(P), d_P, ldp);
        createOnDevice(R, d_R, ldr);
        bcr(DMat<T>(d_A, A.n_rows, A.n_cols, lda),
            DMat<T>(d_R, A.n_rows, A.n_cols, ldr), d_P, 0);
        moveFromDevice(R, d_R, ldr);
    }

    template <typename T>
    void checkResults(const Mat<T> &A, const Col<int> &P, const Mat<T> &R,
                      T eps = std::numeric_limits<T>::epsilon())
    {
        uvec uP(P.n_elem);
        for (int i = 0; i < P.n_elem; ++i) {
            uP(i) = P(i);
        }
        Mat<T> Ap = A.cols(uP);
        Mat<T> Rp = R.rows(0, std::min<int>(2*BLOCK_SIZE, R.n_rows)-1);
        for (int i = 0; i < A.n_cols; i += 2*BLOCK_SIZE) {
            Mat<T> R1 = Rp.cols(i, std::min<int>(i+2*BLOCK_SIZE, R.n_cols)-1);
            EXPECT_PRED3(matricesNear<T>, upperTriangle(R1), R1, eps)
                << "Block-column starting at column " << i
                << " is not triangular";
        }
        EXPECT_PRED3(matricesNear<T>, diagvec(Ap.t()*Ap),
                     diagvec(Rp.t()*Rp), eps)
                << "Column norms do not match";
    }
};


TEST_F(BcrTest, SingleBlock)
{
    mat A(2*BLOCK_SIZE, 2*BLOCK_SIZE, fill::randn);
    Mat<int> P = count(2*BLOCK_SIZE);
    mat R;
    runBcr(A, P, R);
    checkResults(A, P, R);
}


TEST_F(BcrTest, SingleBlockPermuted)
{
    mat A(2*BLOCK_SIZE, 2*BLOCK_SIZE, fill::randn);
    Mat<int> P = shuffle(count(2*BLOCK_SIZE));
    mat R;
    runBcr(A, P, R);
    checkResults(A, P, R);
}


TEST_F(BcrTest, MultipleBlocks)
{
    mat A(15*BLOCK_SIZE + 7, 12*BLOCK_SIZE + 9, fill::randn);
    Mat<int> P = count(12*BLOCK_SIZE + 9);
    mat R;
    runBcr(A, P, R);
    checkResults(A, P, R);
}


TEST_F(BcrTest, MultipleBlocksOffset)
{
    mat A(15*BLOCK_SIZE + 7, 12*BLOCK_SIZE + 9, fill::randn);
    Mat<int> P = shuffle(count(12*BLOCK_SIZE + 9));
    mat R;
    runBcr(A, P, R);
    checkResults(A, P, R);
}


TEST_F(BcrTest, SingleBlock_SP)
{
    fmat A(2*BLOCK_SIZE, 2*BLOCK_SIZE, fill::randn);
    Mat<int> P = count(2*BLOCK_SIZE);
    fmat R;
    runBcr(A, P, R);
    checkResults(A, P, R);
}


TEST_F(BcrTest, SingleBlockPermuted_SP)
{
    fmat A(2*BLOCK_SIZE, 2*BLOCK_SIZE, fill::randn);
    Mat<int> P = shuffle(count(2*BLOCK_SIZE));
    fmat R;
    runBcr(A, P, R);
    checkResults(A, P, R);
}


TEST_F(BcrTest, MultipleBlocks_SP)
{
    fmat A(15*BLOCK_SIZE + 7, 12*BLOCK_SIZE + 9, fill::randn);
    Mat<int> P = count(12*BLOCK_SIZE + 9);
    fmat R;
    runBcr(A, P, R);
    checkResults(A, P, R);
}


TEST_F(BcrTest, MultipleBlocksOffset_SP)
{
    fmat A(15*BLOCK_SIZE + 7, 12*BLOCK_SIZE + 9, fill::randn);
    Mat<int> P = shuffle(count(12*BLOCK_SIZE + 9));
    fmat R;
    runBcr(A, P, R);
    checkResults(A, P, R);
}


}

