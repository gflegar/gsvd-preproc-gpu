#include <gtest/gtest.h>
#include <armadillo>
#include <algorithm>
#include <limits>


#include <utils.cuh>
#include <test/utils.cuh>
#include <test/block_view.h>
#include <base_defs.h>
#include <ggsvp.cuh>


using namespace arma;
using namespace gpg;


namespace {


class GgsvpTest : public ::testing::Test {
protected:
    GgsvpTest() { arma_rng::set_seed(303030); }

    template<typename T>
    void runGgsvp(const Mat<T> &A, const Mat<T> &B, Mat<T> &A1, Mat<T> &B1,
                  Mat<T> &U, Mat<T> &V, Mat<T> &Q, int &k, int &l,
                  T eps = std::numeric_limits<T>::epsilon())
    {
        A1.zeros(size(A));
        B1.zeros(size(B));
        U.zeros(A.n_rows, A.n_rows);
        V.zeros(B.n_rows, B.n_rows);
        Q.zeros(A.n_cols, A.n_cols);
        const dim3 ws = sizes::ggsvp_w<BLOCK_SIZE>
            (A.n_rows, A.n_cols, B.n_rows);
        const dim3 iws = sizes::ggsvp_iw<BLOCK_SIZE>
            (A.n_rows, A.n_cols, B.n_rows);

        Mat<T> W(ws.x, ws.y);
        Mat<int> IW(iws.x, 1);

        T *d_A, *d_B, *d_U, *d_V, *d_Q, *d_W;
        int *d_IW;
        int lda, ldb, ldu, ldv, ldq, ldw, ldiw;
        createOnDevice(A, d_A, lda);
        createOnDevice(B, d_B, ldb);
        createOnDevice(U, d_U, ldu);
        createOnDevice(V, d_V, ldv);
        createOnDevice(Q, d_Q, ldq);
        createOnDevice(W, d_W, ldw);
        createOnDevice(IW, d_IW, ldiw);

        ggsvp(DMat<T>(d_A, A.n_rows, A.n_cols, lda),
              DMat<T>(d_B, B.n_rows, B.n_cols, ldb),
              DMat<T>(d_U, U.n_rows, U.n_cols, ldu),
              DMat<T>(d_V, V.n_rows, V.n_cols, ldv),
              DMat<T>(d_Q, Q.n_rows, Q.n_cols, ldq),
              k, l,
              DMat<T>(d_W, W.n_rows, W.n_cols, ldw),
              d_IW,
              eps * norm(A, "fro"), eps * norm(B, "fro"));

        cudaAssert(cudaDeviceSynchronize());

        moveFromDevice(A1, d_A, lda);
        moveFromDevice(B1, d_B, ldb);
        moveFromDevice(U, d_U, ldu);
        moveFromDevice(V, d_V, ldv);
        moveFromDevice(Q, d_Q, ldq);
    }

    template<typename T>
    void checkResults(const Mat<T> &A, const Mat<T> &B, const Mat<T> &A1,
                      const Mat<T> &B1, const Mat<T> &U, const Mat<T> &V,
                      const Mat<T> &Q, int tl, int k, int l,
                      T eps = std::numeric_limits<T>::epsilon())
    {
        /*std::cout << "A:\n" << A
                  << "A1:\n" << A1
                  << "U:\n" << U
                  << "B:\n" << B
                  << "B1:\n" << B1
                  << "V:\n" << V
                  << "Q:\n" << Q << std::endl;*/
        std::cout << "Real    : k: N/A\tl: " << l << std::endl;
        std::cout << "Computed: k: " << k << "\tl: " << l << std::endl;
        int n = A.n_cols;
        EXPECT_PRED3(matricesNear<T>, upperTriangle(A1, n-k-l-1), A1, eps);
        EXPECT_PRED3(matricesNear<T>, upperTriangle(B1, n-l-1), B1, eps);
        EXPECT_PRED3(matricesNear<T>, eye<Mat<T> >(size(U)), U.t()*U, eps);
        EXPECT_PRED3(matricesNear<T>, eye<Mat<T> >(size(V)), V.t()*V, eps);
        EXPECT_PRED3(matricesNear<T>, eye<Mat<T> >(size(Q)), Q.t()*Q, eps);
        Mat<T> A2 = upperTriangle(A1, n-k-l-1);
        Mat<T> B2 = upperTriangle(B1, n-l-1);
        EXPECT_PRED3(matricesNear<T>, A, U.t() * A2 * Q.t(), eps);
        EXPECT_PRED3(matricesNear<T>, B, V.t() * B2 * Q.t(), eps);
    }
};


TEST_F(GgsvpTest, SingleBlock)
{
    mat A(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    mat B(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    mat U, V, Q, A1, B1;
    int k, l;
    runGgsvp(A, B, A1, B1, U, V, Q, k, l);
    checkResults(A, B, A1, B1, U, V, Q, BLOCK_SIZE, k, l);
}


TEST_F(GgsvpTest, SingleBlockReducedRank)
{
    mat A = randn<mat>(BLOCK_SIZE, 5) * randn<mat>(5, BLOCK_SIZE);
    mat B = randn<mat>(BLOCK_SIZE, 13) * randn<mat>(13, BLOCK_SIZE);
    mat U, V, Q, A1, B1;
    int k, l;
    runGgsvp(A, B, A1, B1, U, V, Q, k, l);
    checkResults(A, B, A1, B1, U, V, Q, 13, k, l);
}


TEST_F(GgsvpTest, MultipleBlocks)
{
    mat A(7 * BLOCK_SIZE + 3, 4 * BLOCK_SIZE + 7, fill::randn);
    mat B(5 * BLOCK_SIZE + 2, 4 * BLOCK_SIZE + 7, fill::randn);
    mat U, V, Q, A1, B1;
    int k, l;
    runGgsvp(A, B, A1, B1, U, V, Q, k, l);
    checkResults(A, B, A1, B1, U, V, Q, A.n_cols, k, l);
}



TEST_F(GgsvpTest, MultipleBlocksReducedRank)
{
    mat A = randn<mat>(7*BLOCK_SIZE+3, 23) * randn<mat>(23, 4*BLOCK_SIZE+7);
    mat B = randn<mat>(6*BLOCK_SIZE+6, 13) * randn<mat>(13, 4*BLOCK_SIZE+7);
    mat U, V, Q, A1, B1;
    int k, l;
    runGgsvp(A, B, A1, B1, U, V, Q, k, l);
    checkResults(A, B, A1, B1, U, V, Q, 13, k, l);
}


TEST_F(GgsvpTest, SingleBlockReducedRank_SP)
{
    fmat A = randn<fmat>(BLOCK_SIZE, 5) * randn<fmat>(5, BLOCK_SIZE);
    fmat B = randn<fmat>(BLOCK_SIZE, 13) * randn<fmat>(13, BLOCK_SIZE);
    fmat U, V, Q, A1, B1;
    int k, l;
    runGgsvp(A, B, A1, B1, U, V, Q, k, l);
    checkResults(A, B, A1, B1, U, V, Q, 13, k, l);
}


TEST_F(GgsvpTest, MultipleBlocks_SP)
{
    fmat A(7 * BLOCK_SIZE + 3, 4 * BLOCK_SIZE + 7, fill::randn);
    fmat B(5 * BLOCK_SIZE + 2, 4 * BLOCK_SIZE + 7, fill::randn);
    fmat U, V, Q, A1, B1;
    int k, l;
    runGgsvp(A, B, A1, B1, U, V, Q, k, l);
    checkResults(A, B, A1, B1, U, V, Q, A.n_cols, k, l);
}



TEST_F(GgsvpTest, MultipleBlocksReducedRank_SP)
{
    fmat A = randn<fmat>(7*BLOCK_SIZE+3, 23) * randn<fmat>(23, 4*BLOCK_SIZE+7);
    fmat B = randn<fmat>(6*BLOCK_SIZE+6, 13) * randn<fmat>(13, 4*BLOCK_SIZE+7);
    fmat U, V, Q, A1, B1;
    int k, l;
    runGgsvp(A, B, A1, B1, U, V, Q, k, l);
    checkResults(A, B, A1, B1, U, V, Q, 13, k, l);
}

}
