#include <gtest/gtest.h>
#include <armadillo>
#include <algorithm>
#include <vector>
#include <limits>
#include <cmath>


#include <utils.cuh>
#include <dmat.cuh>
#include <test/utils.cuh>
#include <base_defs.h>
#include <brrq.cuh>


using namespace arma;
using namespace gpg;


namespace {


class BrrqTest : public ::testing::Test {
protected:
    BrrqTest() { arma_rng::set_seed(171717); }

    template <typename T>
    void runBrrq(const Mat<T> &A, const Mat<T> &Q, Mat<T> &R, Mat<T> &Q2)
    {
        R.zeros(size(A));
        Q2.zeros(size(Q));

        dim3 ws = sizes::brrq_w<BLOCK_SIZE>(A.n_rows, A.n_cols);
        Mat<T> W(ws.x, ws.y, fill::zeros);
        T *d_A, *d_Q, *d_W;
        int lda, ldq, ldw;
        std::vector<cudaStream_t> s(2);
        for (int i = 0; i < 2; ++i) {
            cudaAssert(cudaStreamCreate(&s[i]));
        }
        createOnDevice(A, d_A, lda);
        createOnDevice(Q, d_Q, ldq);
        createOnDevice(W, d_W, ldw);

        DMat<T> DA(d_A, A.n_rows, A.n_cols, lda);
        DMat<T> work(d_W, W.n_rows, W.n_cols, ldw);
        std::vector<DMat<T> > Qs;
        Qs.push_back(DMat<T>(d_Q, Q.n_rows, Q.n_cols, ldq));
        Qs.push_back(DA(BLOCK_SIZE, -1, 0, -1));
        if (Qs[1].n_rows <= 0) {
            Qs.pop_back();
        }
        
        /*T *Qs[] = {d_A + BLOCK_SIZE*lda, d_Q};
        int ns[] = {A.n_cols - BLOCK_SIZE, Q.n_cols};
        int ldqs[] = {lda, ldq};
        int nqs = 1 + (ns[1] > 0); */
        brrq(DA, s[1], Qs, s, work);
        cudaAssert(cudaDeviceSynchronize());


        moveFromDevice(R, d_A, lda);
        moveFromDevice(Q2, d_Q, ldq);

        for (int i = 0; i < 2; ++i) {
            cudaAssert(cudaStreamDestroy(s[i]));
        }
    }

    template <typename T>
    void checkResults(const Mat<T> &A, const Mat<T> &Q, const Mat<T> &R,
                      const Mat<T> &Q2,
                      T eps = std::numeric_limits<T>::epsilon())
    {
        span sp(0, std::min<int>(BLOCK_SIZE, R.n_rows)-1);
        Mat<T> Rb = R(sp, span::all);
        EXPECT_PRED3(matricesNear<T>,
                     upperTriangle(Rb, Rb.n_cols - BLOCK_SIZE - 1),
                     Rb, eps)
            << "The first block of R is not upper triangular";
        EXPECT_PRED3(matricesNear<T>, Q*Q.t(), Q2*Q2.t(), eps)
            << "Q is not orthogonal";
        EXPECT_PRED3(matricesNear<T>, A*Q.t(), R*Q2.t(), eps)
            << "A*Q' != R*Q2'";
    }
};


TEST_F(BrrqTest, SingleBlock)
{
    mat A(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    mat Q, Q2, R;
    qr(Q, R, randn<mat>(BLOCK_SIZE, BLOCK_SIZE));
    runBrrq(A, Q, R, Q2);
    checkResults(A, Q, R, Q2);
}


TEST_F(BrrqTest, MultipleBlocks)
{
    mat A(6*BLOCK_SIZE + 5, 4*BLOCK_SIZE, fill::randn);
    mat Q, Q2, R;
    qr(Q, R, randn<mat>(4*BLOCK_SIZE, 4*BLOCK_SIZE));
    runBrrq(A, Q, R, Q2);
    checkResults(A, Q, R, Q2);
}


TEST_F(BrrqTest, MultipleBlocksNonSquareQ)
{
    mat A(7*BLOCK_SIZE + 4, 5*BLOCK_SIZE + 3, fill::randn);
    mat Q, Q2, R;
    qr_econ(Q, R, randn<mat>(5*BLOCK_SIZE + 3, 7*BLOCK_SIZE + 4));
    Q = Q.t();
    runBrrq(A, Q, R, Q2);
    checkResults(A, Q, R, Q2);
}


TEST_F(BrrqTest, SingleBlock_SP)
{
    fmat A(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    fmat Q, Q2, R;
    qr(Q, R, randn<fmat>(BLOCK_SIZE, BLOCK_SIZE));
    runBrrq(A, Q, R, Q2);
    checkResults(A, Q, R, Q2);
}


TEST_F(BrrqTest, MultipleBlocks_SP)
{
    fmat A(6*BLOCK_SIZE + 5, 4*BLOCK_SIZE, fill::randn);
    fmat Q, Q2, R;
    qr(Q, R, randn<fmat>(4*BLOCK_SIZE, 4*BLOCK_SIZE));
    runBrrq(A, Q, R, Q2);
    checkResults(A, Q, R, Q2);
}


TEST_F(BrrqTest, MultipleBlocksNonSquareQ_SP)
{
    fmat A(7*BLOCK_SIZE + 4, 5*BLOCK_SIZE + 3, fill::randn);
    fmat Q, Q2, R;
    qr_econ(Q, R, randn<fmat>(5*BLOCK_SIZE + 3, 7*BLOCK_SIZE + 4));
    Q = Q.t();
    runBrrq(A, Q, R, Q2);
    checkResults(A, Q, R, Q2);
}


}

