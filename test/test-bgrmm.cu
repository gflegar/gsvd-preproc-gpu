#include <gtest/gtest.h>
#include <armadillo>
#include <algorithm>
#include <limits>


#include <utils.cuh>
#include <dmat.cuh>
#include <test/utils.cuh>
#include <base_defs.h>
#include <bgrmm.cuh>


using namespace arma;
using namespace gpg;


namespace {


class BgrmmTest : public ::testing::Test {
protected:
    BgrmmTest() { arma_rng::set_seed(161616); }


    template <typename T>
    void runBgrmm(const Mat<T> &D, const Mat<T> &A, Mat<T> &R, int offset)
    {
        R = zeros<Mat<T> >(A.n_rows, A.n_cols);
        T *d_D, *d_A;
        int ldd, lda;
        createOnDevice(D, d_D, ldd);
        createOnDevice(A, d_A, lda);
        bgrmm(DMat<T>(d_D, D.n_rows, D.n_cols, ldd),
              DMat<T>(d_A, A.n_rows, A.n_cols, lda),
              offset, 0);
        moveFromDevice(R, d_A, lda);
    }

    template <typename T>
    void checkResults(const Mat<T> &D, const Mat<T> &A, const Mat<T> &R,
                      int offset, T eps = std::numeric_limits<T>::epsilon())
    {
        const int ofs = offset * BLOCK_SIZE;
        Mat<T> Dd(D.n_rows, D.n_rows, fill::eye);
        for (int i = 0; i < D.n_rows; i += 2*ofs) {
            int usz = std::min<int>(BLOCK_SIZE, D.n_rows - i);
            int dsz = std::min<int>(BLOCK_SIZE, D.n_rows - i - ofs);
            span up(i, i + usz - 1);
            span dn(i + ofs, i + ofs + dsz - 1);
            Dd(up, up) = D(up, span(0, usz - 1));
            if (dsz > 0) {
                Dd(up, dn) = D(up, span(BLOCK_SIZE, BLOCK_SIZE + dsz - 1));
                Dd(dn, up) = D(dn, span(0, usz - 1));
                Dd(dn, dn) = D(dn, span(BLOCK_SIZE, BLOCK_SIZE + dsz - 1));
            }
        }

        EXPECT_PRED3(matricesNear<T>, R, Dd*A, eps);
    }
};


TEST_F(BgrmmTest, SingleBlockPairNoOffset)
{
    mat A(2*BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    mat D(2*BLOCK_SIZE, 2*BLOCK_SIZE, fill::randn);
    mat R;
    runBgrmm(D, A, R, 1);
    checkResults(D, A, R, 1);
}


TEST_F(BgrmmTest, MultipleBlockPairsNoOffset)
{
    mat A(3*BLOCK_SIZE + 7, 5*BLOCK_SIZE + 3, fill::randn);
    mat D(3*BLOCK_SIZE + 7, 2*BLOCK_SIZE, fill::randn);
    mat R;
    runBgrmm(D, A, R, 1);
    checkResults(D, A, R, 1);
}


TEST_F(BgrmmTest, MultipleBlockPairsWithOffset)
{
    fmat A(5*BLOCK_SIZE + 7, 5*BLOCK_SIZE + 3, fill::randn);
    fmat D(5*BLOCK_SIZE + 7, 2*BLOCK_SIZE, fill::randn);
    fmat R;
    runBgrmm(D, A, R, 2);
    checkResults(D, A, R, 2);
}


TEST_F(BgrmmTest, SingleBlockPairNoOffset_SP)
{
    fmat A(2*BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    fmat D(2*BLOCK_SIZE, 2*BLOCK_SIZE, fill::randn);
    fmat R;
    runBgrmm(D, A, R, 1);
    checkResults(D, A, R, 1);
}


TEST_F(BgrmmTest, MultipleBlockPairsNoOffset_SP)
{
    fmat A(3*BLOCK_SIZE + 7, 5*BLOCK_SIZE + 3, fill::randn);
    fmat D(3*BLOCK_SIZE + 7, 2*BLOCK_SIZE, fill::randn);
    fmat R;
    runBgrmm(D, A, R, 1);
    checkResults(D, A, R, 1);
}


TEST_F(BgrmmTest, MultipleBlockPairsWithOffset_SP)
{
    fmat A(5*BLOCK_SIZE + 7, 5*BLOCK_SIZE + 3, fill::randn);
    fmat D(5*BLOCK_SIZE + 7, 2*BLOCK_SIZE, fill::randn);
    fmat R;
    runBgrmm(D, A, R, 2);
    checkResults(D, A, R, 2);
}


}
