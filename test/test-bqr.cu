#include <gtest/gtest.h>
#include <armadillo>
#include <algorithm>
#include <limits>
#include <cmath>


#include <utils.cuh>
#include <test/utils.cuh>
#include <base_defs.h>
#include <bqr.cuh>


using namespace arma;
using namespace gpg;


namespace {


class BqrTest : public ::testing::Test {
protected:
    BqrTest() { arma_rng::set_seed(181818); }

    template <typename T>
    void runBqr(const Mat<T> &A, const Mat<T> &Q, Mat<T> &R, Mat<T> &Q2)
    {
        const dim3 ws = sizes::bqr_w<BLOCK_SIZE>(A.n_rows, A.n_cols);
        R.zeros(size(A));
        Q2.zeros(size(Q));
        Mat<T> W(ws.x, ws.y, fill::zeros);
        T *d_A, *d_Q, *d_W;
        int lda, ldq, ldw;
        createOnDevice(A, d_A, lda);
        createOnDevice(Q, d_Q, ldq);
        createOnDevice(W, d_W, ldw);
        DMat<T> DA(d_A, A.n_rows, A.n_cols, lda);
        cudaStream_t astream;
        cudaStreamCreate(&astream);
        std::vector<DMat<T> > Qs(1, DMat<T>(d_Q, Q.n_rows, Q.n_cols, ldq));
        std::vector<cudaStream_t> qstreams(1);
        cudaStreamCreate(&qstreams[0]);
        DMat<T> work(d_W, W.n_rows, W.n_cols, ldw);

        bqr(DA, astream, Qs, qstreams, work);

        cudaAssert(cudaDeviceSynchronize());

        moveFromDevice(R, d_A, lda);
        moveFromDevice(Q2, d_Q, ldq);

        cudaStreamDestroy(astream);
        cudaStreamDestroy(qstreams[0]);
    }

    template <typename T>
    void checkResults(const Mat<T> &A, const Mat<T> &Q, const Mat<T> &R,
                      const Mat<T> &Q2,
                      T eps = std::numeric_limits<T>::epsilon())
    {
        EXPECT_PRED3(matricesNear<T>, upperTriangle(R), R, eps)
            << "R factor is not triangular";
        EXPECT_PRED3(matricesNear<T>, eye<Mat<T> >(Q2.n_rows, Q2.n_rows),
                     Q2*Q2.t(), eps)
            << "Q2 factor is not orthogonal";
        EXPECT_PRED3(matricesNear<T>, R, Q2*Q.t()*A, eps)
            << "Q2*Q'*A != R";
    }
};


TEST_F(BqrTest, SingleBlock)
{
    mat A(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    mat Q, R, Q2;
    qr(Q, R, randn<mat>(BLOCK_SIZE, BLOCK_SIZE));
    runBqr(A, Q, R, Q2);
    checkResults(A, Q, R, Q2);
}


TEST_F(BqrTest, SingleBlockColumn)
{
    mat A(5*BLOCK_SIZE + 7, BLOCK_SIZE, fill::randn);
    mat Q, R, Q2;
    qr(Q, R, randn<mat>(5*BLOCK_SIZE + 7, 5*BLOCK_SIZE + 7));
    runBqr(A, Q, R, Q2);
    checkResults(A, Q, R, Q2);
}


TEST_F(BqrTest, MultipleBlocks)
{
    mat A(5*BLOCK_SIZE + 7, 3*BLOCK_SIZE + 4, fill::randn);
    mat Q, R, Q2;
    qr(Q, R, randn<mat>(5*BLOCK_SIZE + 7, 5*BLOCK_SIZE + 7));
    runBqr(A, Q, R, Q2);
    checkResults(A, Q, R, Q2);
}


TEST_F(BqrTest, MultipleBlocksNonSquareQ)
{
    mat A(5*BLOCK_SIZE +7, 3*BLOCK_SIZE + 4, fill::randn);
    mat Q, R, Q2;
    qr_econ(Q, R, randn<mat>(7*BLOCK_SIZE + 3, 5*BLOCK_SIZE + 7));
    Q = Q.t();
    runBqr(A, Q, R, Q2);
    checkResults(A, Q, R, Q2);
}


TEST_F(BqrTest, SingleBlock_SP)
{
    fmat A(BLOCK_SIZE, BLOCK_SIZE, fill::randn);
    fmat Q, R, Q2;
    qr(Q, R, randn<fmat>(BLOCK_SIZE, BLOCK_SIZE));
    runBqr(A, Q, R, Q2);
    checkResults(A, Q, R, Q2);
}


TEST_F(BqrTest, SingleBlockColumn_SP)
{
    fmat A(5*BLOCK_SIZE + 7, BLOCK_SIZE, fill::randn);
    fmat Q, R, Q2;
    qr(Q, R, randn<fmat>(5*BLOCK_SIZE + 7, 5*BLOCK_SIZE + 7));
    runBqr(A, Q, R, Q2);
    checkResults(A, Q, R, Q2);
}


TEST_F(BqrTest, MultipleBlocks_SP)
{
    fmat A(5*BLOCK_SIZE + 7, 3*BLOCK_SIZE + 4, fill::randn);
    fmat Q, R, Q2;
    qr(Q, R, randn<fmat>(5*BLOCK_SIZE + 7, 5*BLOCK_SIZE + 7));
    runBqr(A, Q, R, Q2);
    checkResults(A, Q, R, Q2);
}


TEST_F(BqrTest, MultipleBlocksNonSquareQ_SP)
{
    fmat A(5*BLOCK_SIZE +7, 3*BLOCK_SIZE + 4, fill::randn);
    fmat Q, R, Q2;
    qr_econ(Q, R, randn<fmat>(7*BLOCK_SIZE + 3, 5*BLOCK_SIZE + 7));
    Q = Q.t();
    runBqr(A, Q, R, Q2);
    checkResults(A, Q, R, Q2);
}


}
