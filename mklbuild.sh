set -x
icpc src/gsvd-test.cpp src/gsvd_preproc_lapack.cpp -Iinclude\
    -o build/gsvd-test-mkl -O3 -mkl -lm -lrt
icpc src/gsvd-test-sp.cpp src/gsvd_preproc_lapack_sp.cpp -Iinclude\
    -o build/gsvd-test-mkl-sp -O3 -mkl -lm -lrt

