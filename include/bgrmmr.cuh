#ifndef BGRMMR_CUH_
#define BGRMMR_CUH_


#include <base_defs.h>
#include <dmat.cuh>
#include <utils.cuh>


namespace gpg {


namespace _bgrmmr {


template <typename T>
__global__ void kernel(int m, int n, T * __restrict__ Q, int ldq,
                       T * __restrict__ S, int lds, int offset)
{
    const int lr = threadIdx.x;
    const int lc = threadIdx.y;

    const int gr = lr + blockIdx.x * BLOCK_SIZE;
    const int gc = lc%BLOCK_SIZE + n - BLOCK_SIZE
        - (2*blockIdx.y + 1-lc/BLOCK_SIZE) * offset * BLOCK_SIZE;

    __shared__ T LQ[2*BLOCK_SIZE][2*BLOCK_SIZE];
    __shared__ T LS[2*BLOCK_SIZE][BLOCK_SIZE];

    T r = 0.0;

    if (gc >= 0) {
        LQ[lc][lr] = Q[lr + gc*ldq];
        LQ[lc][lr+BLOCK_SIZE] = Q[lr+BLOCK_SIZE + gc*ldq];
    } else {
        LQ[lc][lr] = 0.0;
        LQ[lc][lr+BLOCK_SIZE] = 0.0;
    }

    __syncthreads();

    if (gr < m && gc >= 0) {
        LS[lc][lr] = S[gr + gc*lds];
    } else {
        LS[lc][lr] = 0.0;
    }

    __syncthreads();

    for (int k = 0; k < 2*BLOCK_SIZE; ++k) {
        r += LS[k][lr] * LQ[lc][k];
    }

    if (gr < m && gc >= 0) {
        S[gr + gc*lds] = r;
    }
}


}


/**
 * Multiplies the matrix `S` from the right with the `Q` factor obtained from
 * tbrq().
 *
 * @tparam T  a numeric type
 *
 * @param m  the number of rows of `S`
 * @param n  the number of columns of `S`
 * @param Q  the `2*BLOCK_SIZE`-by-`n` block-row with compressed `Q` factor
 *           obtained from tbrq()
 * @param ldq  the leading dimenion of `Q`
 * @param S  the memory location of `m`-by-`n` matrix `S`
 * @param lds  the leading dimension of `S`
 * @param offset  offset used in the call to tbrq()
 *
 * @see tbrq()
 */
template <typename T>
inline void bgrmmr(const DMat<T> &Q, const DMat<T> &S, int offset,
                   cudaStream_t s)
{
    dim3 b(BLOCK_SIZE, 2*BLOCK_SIZE);
    dim3 g = gpg::ggs(b, S.n_rows, S.n_cols);
    g.y = gpg::rud(g.y, offset);
    _bgrmmr::kernel<<<g, b, 0, s>>>
        (S.n_rows, S.n_cols, Q.data, Q.ld, S.data, S.ld, offset);
}


}


#endif
