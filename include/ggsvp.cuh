#ifndef GGSVP_CUH_
#define GGSVP_CUH_


#include <vector>


#include <base_defs.h>
#include <utils.cuh>
#include <dmat.cuh>
#include <burv.cuh>
#include <bqr.cuh>


namespace gpg {


namespace {


template <int bs, typename T>
__global__ void identity_k(int m, int n, T *P, int ldp)
{
    int idx = threadIdx.x + blockIdx.x * bs;
    int idy = threadIdx.y + blockIdx.y * bs;
    if (idx < m && idy < n) {
        P[idx + idy*ldp] = (idx == idy) * 1.0;
    }
}


template <typename T>
void identity(const DMat<T> I, cudaStream_t s)
{
    const dim3 bs(BLOCK_SIZE, BLOCK_SIZE);
    identity_k<BLOCK_SIZE><<<ggs(bs, I.n_rows, I.n_cols), bs, 0, s>>>
        (I.n_rows, I.n_cols, I.data, I.ld);
}

/*
template <typename T>
void lbqrp(int m, int n, int p, int t, T *A, int lda, T *Q, int ldq, T *P,
           int ldp, T *B, int ldt, T *work, int *iwork, int *rank, T tol)
{

    std::vector<cudaStream_t> s(2);
    for (int i = 0; i < 2; ++i) cudaAssert(cudaStreamCreate(&s[i]));
    *rank = 0;
    int crank;
    for (int o = 0; o < min(m, n); o += BLOCK_SIZE) {
        //bcfp(m-o, n-o, A + o + o*lda, lda, work, lda, iwork, iwork+n, &crank,
        //     tol);
        //bcp(m, n-o, A + o*lda, lda, iwork);
        //bcp(p, n-o, P + o*ldp, ldp, iwork);
        if (B != NULL) {
            //bcp(t, n-o, B + o*ldt, ldt, iwork);
        }

        //int ns[] = {p - o - BLOCK_SIZE, m};
        //T *Qs[] = {A + o + (o+BLOCK_SIZE)*lda, Q + o};
        //int ldqs[] = {lda, ldq};
        cudaAssert(cudaDeviceSynchronize());
        //bcqr(m-o, p-o, A + o + o*lda, lda, s[0], ns, Qs, ldqs, s,
        //     1 + (ns[1]>0), work, lda);
        cudaAssert(cudaDeviceSynchronize());
        *rank += crank;
        if (crank < BLOCK_SIZE) break;
    }

    for (int i = 0; i < 2; ++i) cudaAssert(cudaStreamDestroy(s[i]));
}


template <typename T>
void lurv(int m, int n, int p, int t, T *R, int ldr, T *V, int ldv, T *B,
          int ldt, T *work, int ldw)
{
    for (int b = 0; m - b > 0; b += BLOCK_SIZE) {
        const int r = max(0, m - b - BLOCK_SIZE);
        //sbrq(m - b - r, n - b, R + r, ldr, work, ldw);
        //bdmmr(p, n - b, work, ldw, V, ldv);
        if (B != NULL) {
            //bdmmr(t, n - b, work, ldw, B, ldt);
        }
        if (r > 0) {
            //bdmmr(r, n - b, work, ldw, R, ldr);
        }
        for (int o = 1; o * BLOCK_SIZE < n - b; o <<= 1) {
            //tbrq(m - b - r, n - b, R  + r, ldr, work, ldw, o);
            //bgrmmr(p, n - b, work, ldw, V, ldv, o);
            if (B != NULL) {
                //bgrmmr(t, n - b, work, ldw, B, ldt, o);
            }
            if (r > 0) {
                //bgrmmr(r, n - b, work, ldw, R, ldr, o);
            }
        }
    }
}*/


}


namespace sizes {

template <int bs>
inline dim3 ggsvp_w(int m, int n, int p)
{
    dim3 wa = burv_w<bs>(m, n);
    dim3 wq = bqr_w<bs>(m, n);
    dim3 wb = burv_w<bs>(p, n);
    return dim3(max(max(wa.x, wb.x), wq.x), max(max(wa.y, wb.y), wq.y));
}


template <int bs>
inline dim3 ggsvp_iw(int m, int n, int p)
{
    dim3 iwa = burv_iw<bs>(m, n);
    dim3 iwb = burv_iw<bs>(p, n);
    return dim3(max(iwa.x, iwb.x), max(iwa.y, iwb.y));
}


}

/**
 * Calculates the preprocessing step decomposition for the generalized SVD
 * of a matrix pair.
 *
 * For the given `m`-by-`n` and `p`-by-`n` matrices `A` and `B` calculates the
 * decomposition:
 *
 *     U * A * Q = A1
 *     V * B * Q = B1
 *                n-k-l k   l
 *     A1 = k    [0     A12 A13]
 *          m-k  [0     0   A23]
 *
 *                n-k-l k   l
 *     B1 = l    [0     0   B13]
 *          p-l  [0     0   0  ]
 *
 * where:
 *
 * - `U` is `m`-by-`m` orthonormal
 * - `V` is `p`-by-`p` orthonormal
 * - `Q` is `n`-by-`n` orthonormal
 * - `A12` is upper-triangular and non-singular
 * - `A23` is upper-triangular
 * - `B13` is upper-triangular and non-singular
 *
 * Overwrites matrices `A` and `B` with `A1` and `B1` respectively.
 *
 * @tparam T  a numeric type
 * 
 * @param[in] m  the number of rows of matrix `A`
 * @param[in] n  the number of columns of matrices `A` and `B`
 * @param[in] p  the number of rows of matrix `B`
 * @param[in,out] A  the memory location of the `m`-by-`n` matrix `A`
 * @param[in] lda  the leading dimension of `A`
 * @param[in,out] B  the memory location of the `p`-by-`n` matrix `B`
 * @param[in] ldb  the leading dimension of `B`
 * @param[out] k  dimension of block described above
 * @param[out] l  dimension of block described above
 * @param[out] U  the memory location for the `m`-by-`m` matrix `U`
 * @param[in] ldu  the leading dimension of `U`
 * @param[out] V  the memory location for the `p`-by-`p` matrix `V`
 * @param[in] ldv  the leading dimension of `V`
 * @param[out] Q  the memory location for the `n`-by-`n` matrix `Q`
 * @param[in] ldq  the leading dimension of `Q`
 * @param[out] work  the memory location of an `??`-by-`??` matrix used to
 *                   hold intermediate results
 * @param[in] ldw  the leading dimension of `work`
 * @param[out] iwork  the memory location of an array of size `??` used to hold
 *                    intermediate results
 * @param[in] tol  the tolerance value used for the computation of ranks
 */
template <typename T>
void ggsvp(DMat<T> A, DMat<T> B, DMat<T> U, DMat<T> V, DMat<T> Q,
           int &k, int &l, DMat<T> work, int *iwork, T atol, T btol)
{
    int lp, hp;
    cudaAssert(cudaDeviceGetStreamPriorityRange(&lp, &hp));
    k = l = 0;
    cudaStream_t s[5];
    for (int i = 0; i < 5; ++i) {
        cudaAssert(cudaStreamCreate(&s[i]));
        cudaAssert(cudaStreamCreateWithPriority(
                   &s[i], cudaStreamDefault, hp + i));
    }
    cudaStream_t bs = s[0], as = s[1], us = s[2], vs = s[3], qs = s[4];

    identity(U, us);
    identity(V, vs);
    identity(Q, qs);

    std::vector<DMat<T> > vm(2);
    std::vector<cudaStream_t> vst(2);
    vm[0] = Q; vst[0] = qs;
    vm[1] = A; vst[1] = as;
    burv(B, bs,
         std::vector<DMat<T> >(1, V), std::vector<cudaStream_t>(1, vs),
         vm, vst, work, iwork, l, btol);

    cudaAssert(cudaDeviceSynchronize());

    if (l < A.n_cols) {
        std::vector<DMat<T> > um(2);
        std::vector<cudaStream_t> ust(2);
        um[0] = U; ust[0] = us;
        um[1] = A(0, -1, A.n_cols - l, -1); ust[1] = vs;

        burv(A(0, -1, 0, A.n_cols - l), as,
             um, ust,
             std::vector<DMat<T> >(1, Q(0, -1, 0, Q.n_cols - l)),
             std::vector<cudaStream_t>(1, qs),
             work, iwork, k, atol);

        cudaAssert(cudaDeviceSynchronize());
    }


    bqr(A(k, -1, A.n_cols - l, -1), as,
        std::vector<DMat<T> >(1, U(k, -1, 0, -1)),
        std::vector<cudaStream_t>(1, us),
        work);

    for (int i = 0; i < 5; ++i) {
        cudaStreamDestroy(s[i]);
    }
    //lbqrp(p, n, n, m, B, ldb, V, ldv, Q, ldq, A, lda, work, iwork, l, tol);
    //lurv(*l, n, n, m, B, ldb, Q, ldq, A, lda, work, ldw);
    //lbqrp(m, n-*l, n, 0, A, lda, U, ldu, Q, ldq, (T*)NULL, 0, work, iwork, k,
    //      tol);
    //lurv(*k, n-*l, n, 0, A, lda, Q, ldq, (T*)NULL, 0, work, ldw);
    //bqr(m-*k, *l, m, A+*k+(n-*l)*lda, lda, U+*k, ldu, work, ldw);
}


}


#endif
