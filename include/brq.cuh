#ifndef BRQ_CUH_
#define BRQ_CUH_


#include <vector>
#include <algorithm>


#include <base_defs.h>
#include <dmat.cuh>
#include <brrq.cuh>


namespace gpg {


namespace sizes {


template<int bs>
dim3 brq_w(int m, int n)
{
    dim3 fs = brrq_w<bs>(m, n);
    dim3 ss = brrq_w<bs>(m-bs, n-bs);
    return dim3(fs.x + ss.x, std::max(fs.y, ss.y));
}


}


/**
 * Computes the RQ decomposition of `A`.
 *
 * `A` is replaced with the `R` factor and each matrix in `Qs` is multiplied
 * by `Q**T`.
 *
 * @tparam T  a numeric type
 *
 * @param[in,out] A  described above
 * @param[in] astream  the CUDA stream used for the computation of the QR
 *                     decomposition
 * @param[in,out] Qs  described above
 * @param[in] qstreams  the CUDA streams used for the  multiplication of `Qs`
 *                      by `Q**T`
 * @param[out] work  a matrix with dimension given by
 *                   `sizes::bqr_w(A.n_rows. A.n_cols)` used to store
 *                   intermediate data
 */
template <typename T>
void brq(DMat<T> A, cudaStream_t astream, std::vector<DMat<T> > Qs,
         std::vector<cudaStream_t> qstreams, const DMat<T> &work)
{
    const int nqs = Qs.size();
    const int nb = std::min(A.n_rows, A.n_cols);
    Qs.resize(nqs + 1);
    qstreams.push_back(astream);

    const int split = sizes::brrq_w<BLOCK_SIZE>(A.n_rows, A.n_cols).x;
    DMat<T> ws[] = {work(0, split, 0, -1), work(split, -1, 0, -1)};
    int cw = 0;

    std::vector<cudaEvent_t> multCompleted[2];
    multCompleted[0].resize(nqs);
    multCompleted[1].resize(nqs);
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < nqs; ++j) {
            cudaAssert(cudaEventCreateWithFlags(
                    &multCompleted[i][j], cudaEventDisableTiming));
        }
    }

    for (int b = 0; b < nb; b += BLOCK_SIZE) {
        Qs[nqs] = A(0, std::max(0, A.n_rows - BLOCK_SIZE), 0, -1);
        if (Qs[nqs].n_rows <= 0) {
            Qs.pop_back();
        }
        for (int i = 0; i < nqs; ++i) {
            cudaAssert(cudaStreamWaitEvent(astream, multCompleted[cw][i], 0));
        }
        const int as = std::max(0, A.n_rows - BLOCK_SIZE);
        brrq(A(as, -1, 0, -1), astream, Qs, qstreams, ws[cw]);
        for (int i = 0; i < nqs; ++i) {
            cudaAssert(cudaEventRecord(multCompleted[cw][i], qstreams[i]));
        }
        A = A(0, A.n_rows - BLOCK_SIZE, 0, A.n_cols - BLOCK_SIZE);
        for (int i = 0; i < nqs; ++i) {
            Qs[i] = Qs[i](0, -1, 0, Qs[i].n_cols - BLOCK_SIZE);
        }
        cw = 1 - cw;
    }

    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < nqs; ++j) {
            cudaAssert(cudaEventDestroy(multCompleted[i][j]));
        }
    }
}


}

#endif

