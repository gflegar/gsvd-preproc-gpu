#ifndef TBR_CUH
#define TBR_CUH


#include <base_defs.h>
#include <dmat.cuh>
#include <coppy_block.cuh>
#include <givens_rot.cuh>
#include <utils.cuh>


namespace gpg {


namespace _tbr {


template <int bs, typename T>
__global__ void
__launch_bounds__(512, 3)
kernel(int m, int n, T *R, int ldr, int ofs)
{
    const int ldl = 2*bs + 1;
    const int bx = blockIdx.x * 2*ofs * 2*bs;
    const int by = blockIdx.y * 2*bs;

    m -= bx;
    n -= by;
    R += bx + by*ldr;

    __shared__ T L[2*bs * ldl];

    copyBlockUT<1, 0, 2*bs, bs, 1, 2>(m, n, R, ldr, L, ldl);
    copyBlockUTLT<1, 2*bs, bs, 1, 2>(
            m - ofs*2*bs, n, R + ofs*2*bs, ldr, L + 1, ldl);

    for (int col = 0; col < 2*bs; ++col) {
        __syncthreads();
        for (int j = 0; j < 2; ++j) {
            const int row = threadIdx.y + j*bs;
            if (row >= col) {
                T c, s;
                genRot(L[row*(ldl+1)], L[2*bs-row + col*(ldl+1)], c, s);
                applyRotB<2*bs, 1, ldl>(
                        2*bs-row, c, s,
                        L + row*(ldl+1),
                        L + 2*bs-row + col*(ldl+1));
            }
        }
    }

    __syncthreads();
    copyBlockUT<0, 1, 2*bs, bs, 1, 2>(m, n, L, ldl, R, ldr);
}


}


/**
 * Computes the `R` factor in the QR factorization of triangular block pairs.
 *
 * This function expects that each of the matrices `R{i,j} = 
 * R(i*offset*2*BLOCK_SIZE:(i*offset+1)*2*BLOCK_SIZE,
 * j*2*BLOCK_SIZE:(j+1)*2*BLOCK_SIZE)` is upper triangular.
 *
 * For each pair of matrices `R{2*i,j}` and `R{2*i+1,j}` computes the QR
 * decomposition
 *
 *     [Q{2*i  ,j}] * [R{2*i  ,j}] = [S{2*i  ,j}]
 *     [Q{2*i+1,j}]   [R{2*i+1,j}]   [S{2*i+1,j}]
 *
 * and stores `S{2*i,j}` to `R(i*offset*2*BLOCK_SIZE:(i*offset+1)*2*BLOCK_SIZE,
 * j*2*BLOCK_SIZE:(j+1)*2*BLOCK_SIZE)`.
 *
 * @tparam T  a numeric type
 *
 * @param[in] m  the number of rows of `A`
 * @param[in] n  the number of columns of `A`
 * @param[in,out] R  the memory location of the `m`-by-`n` matrix `R`
 * @param[in] ldr  the leading dimension of `R`
 * @param[in] offset  the vertical offset of block pairs
 */
template <typename T>
inline void tbr(const DMat<T> &R, int offset, cudaStream_t s)
{
    const dim3 b(2*BLOCK_SIZE, BLOCK_SIZE);
    const dim3 g(gpg::rud(gpg::rud(R.n_rows, 2*BLOCK_SIZE), 2*offset),
                 gpg::rud(R.n_cols, 2*BLOCK_SIZE));
    _tbr::kernel<BLOCK_SIZE><<<g, b, 0, s>>>
        (R.n_rows, R.n_cols, R.data, R.ld, offset);
}


}

#endif

