#ifndef TBQR_CUH_
#define TBQR_CUH_


#include <base_defs.h>
#include <dmat.cuh>
#include <coppy_block.cuh>
#include <givens_rot.cuh>
#include <utils.cuh>


namespace gpg {


namespace _tbqr {


template <int bs, typename T>
__global__ void kernel(int m, int n, T * __restrict__ R, int ldr,
                       T * __restrict__ Q, int ldq, int ofs)
{
    const int ldl = 2*bs + 1;
    const int bx = (2*blockIdx.x + threadIdx.x/bs) * ofs * bs -
                   threadIdx.x/bs*bs;
    const int by = blockIdx.y * bs;

    __shared__ T L[3*bs * ldl];

    m -= bx;
    n -= by;
    R += bx + by*ldr;
    Q += bx + by*2*ldq;

    copyBlock<1, 2*bs, bs, 1, 1>(m, n, R, ldr, L, ldl);
    L[threadIdx.x + (threadIdx.y+bs)*ldl] = T(threadIdx.x == threadIdx.y);
    L[threadIdx.x + (threadIdx.y+2*bs)*ldl] = T(threadIdx.x == threadIdx.y+bs);

    for (int col = 0; col < bs; ++col) {
        const int row = threadIdx.y;
        __syncthreads();
        if (row >= col) {
            T c, s;
            genRot(L[row*(ldl+1)], L[row+bs-col + row*ldl], c, s);
            applyRotB<2*bs, 2, ldl>(3*bs, c, s, L + row, L + row+bs-col);
        }
    }

    __syncthreads();

    copyBlock<0, 2*bs, bs, 1, 1>(m, n, L, ldl, R, ldr);
    copyBlock<0, 2*bs, bs, 1, 2>(m, 2*bs, L + bs*ldl, ldl, Q, ldq);
}


}


namespace sizes {


template <int bs>
inline dim3 tbqr_q(int m, int n)
{
    return dim3(m, rud(n, bs) * 2*bs);
}


}


/**
 * Computes the QR factorizations of triangular block pairs.
 *
 * This function expects that each of the matrices `R{i,j} = 
 * R(i*offset*BLOCK_SIZE:(i*offset+1)*BLOCK_SIZE,
 * j*BLOCK_SIZE:(j+1)*BLOCK_SIZE)` is upper triangular.
 *
 * For each pair of matrices `R{2*i,j}` and `R{2*i+1,j}` computes the QR
 * decomposition:
 *
 *     [Q{2*i  ,j}] * [R{2*i  ,j}] = [S{2*i  ,j}]
 *     [Q{2*i+1,j}]   [R{2*i+1,j}]   [S{2*i+1,j}]
 *
 * Stores `S{i,j}` to `R(i*offset*BLOCK_SIZE:(i*offset+1)*BLOCK_SIZE,
 * j*BLOCK_SIZE:(j+1)*BLOCK_SIZE)` and `Q{i,j}` to
 * `Q(i*offset*BLOCK_SIZE:(i*offset+1)*BLOCK_SIZE,
 * 2*j*BLOCK_SIZE:(2*j+2)*BLOCK_SIZE)`.
 *
 * @tparam T  a numeric type
 *
 * @param[in,out] R  the `m`-by-`n` matrix `R`
 * @param[out] Q  the matrix `Q` with dimensions given by `sizes::tbqr_q(m,n)`
 * @param[in] ofs  the vertical offset of block pairs
 * @param[in] s  the stream on which the kernel should be run
 */
template<typename T>
inline void tbqr(const DMat<T> &R, const DMat<T> &Q, int ofs, cudaStream_t s)
{
    const dim3 b(2*BLOCK_SIZE, BLOCK_SIZE);
    dim3 g = gpg::ggs(b, R.n_rows, R.n_cols);
    g.x = gpg::rud(g.x, ofs);
    _tbqr::kernel<BLOCK_SIZE><<<g, b, 0, s>>>
        (R.n_rows, R.n_cols, R.data, R.ld, Q.data, Q.ld, ofs);
}


}

#endif

