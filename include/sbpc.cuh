#ifndef SBPC_CUH_
#define SBPC_CUH_


#include <base_defs.h>
#include <dmat.cuh>
#include <coppy_block.cuh>
#include <givens_rot.cuh>
#include <utils.cuh>

#include <cstdio>

namespace gpg {


namespace _sbpc {


template <int bs, typename T>
__device__ void colNorms(int n, const T * __restrict__ A, int lda,
                         volatile T * __restrict__ work, int ldw)
{
    const int col = threadIdx.x/(bs/2) + 2*threadIdx.y;
    const int row = threadIdx.x % (bs/2);
    A += col*lda;
    work += col*ldw;

    if (row < n) {
        work[row] = A[row] * A[row];
    } else {
        work[row] = T(0);
    }
    if (row + bs/2 < n) {
        work[row] += A[row + bs/2] * A[row + bs/2];
    }
    for (int i = bs/4; i > 0; i >>= 1) {
        if (row < i) {
            work[row] += work[row + i];
        }
    }
}


template<int bs, typename T>
__device__ int maxElem(const T * __restrict__ A, int lda,
                       volatile int * __restrict__ work)
{
    work[threadIdx.x] = threadIdx.x;
    for (int i = bs >> 1; i > 0; i >>= 1) {
        if (threadIdx.x < i) {
            const int t = work[threadIdx.x];
            const int o = work[threadIdx.x+i];
            if (A[o*lda] > A[t*lda]) {
                work[threadIdx.x] = o;
            }
        }
    }
    return work[0];
}


template <typename T>
__device__ void swap(T &a, T &b)
{
    T c = a;
    a = b;
    b = c;
}


template <int bs, typename T>
__global__ void 
__launch_bounds__(512, 3)
kernel(int m, int n, T * __restrict__ A, int lda, int * __restrict__ P,
       int * __restrict__ O, int * __restrict__ rank, T rtol)
{
    const int ldl = 3*bs + 1;
    const int by = blockIdx.y * 2*bs;

    __shared__ T L[2*bs * ldl];
    __shared__ int LP[2*bs];
    __shared__ int MN[2*bs];
    __shared__ bool rdone;
    __shared__ int lr;

    n -= by;
    A += by*lda;
    P += by;
    O += blockIdx.y * bs;

    if (threadIdx.x == 0 && threadIdx.y == 0) {
        rdone = (rank == NULL);
    }

    copyBlock<1, 2*bs, bs, 1, 2>(m, n, A, lda, L, ldl);
    copyBlock<0, 2*bs, bs, 1, 1>(n, 1, P, 1, LP, 1);

    /*
    __syncthreads();
    if (blockIdx.x + blockIdx.y + threadIdx.x + threadIdx.y == 0) {
        for (int i = 0; i < 2*bs; ++i) {
            printf("%d ", P[i]);
        }
        printf("\n L: ");
        for (int i = 0; i < 2*bs; ++i) {
            printf("%d ", LP[i]);
        }
        printf("\n");
    }*/

    for (int col = 0; col < 2*bs - 1; ++col) {
        __syncthreads();
        colNorms<2*bs>(2*bs-col, L + col, ldl, L + 2*bs, ldl);
        __syncthreads();

        if (threadIdx.y == 0) {
            int mc = maxElem<2*bs>(L + 2*bs, ldl, MN);
            /*
            if (blockIdx.x + blockIdx.y + threadIdx.x == 0) {
                printf("%d ", mc);
            }*/
            if (mc >= col && mc < n) {
                swap(L[threadIdx.x + mc*ldl], L[threadIdx.x + col*ldl]);
                if (threadIdx.x == 0) {
                    swap(LP[mc], LP[col]);
                }
            }
        }

        for (int ofs = 1; col + ofs < 2*bs; ofs <<= 1) {
            const int row = col + (2*threadIdx.y + 1) * ofs;
            __syncthreads();
            if (row < 2*bs) {
                T c, s;
                genRot(L[row-ofs + col*ldl], L[row + col*ldl], c, s);
                applyRot<2*bs, 1, ldl>(c, s, L + row-ofs, L + row);
            }
        }

        //__syncthreads();

        if (threadIdx.y == 0 && threadIdx.x == col && !rdone &&
            abs(L[col*(ldl+1)]) < rtol) {
            lr = col;
            rdone = true;
        }
    }

    /*
    if (blockIdx.x + blockIdx.y + threadIdx.x + threadIdx.y == 0) {
        printf("\n");
        for (int i = 0; i < 2*bs; ++i) {
            printf("%d ", LP[i]);
        }
        printf("\n");
    }*/
    __syncthreads();

    if (threadIdx.x == 0 && threadIdx.y == 0) {
        if (!rdone) {
            if (abs(L[(2*bs-1)*(ldl+1)]) < rtol) {
                lr = 2*bs - 1;
            } else {
                lr = 2*bs;
            }
        }
        if (rank != NULL) {
            rank[0] = lr;
        }
    }


    copyBlock<0, 2*bs, bs, 1, 1>(min(bs, n), 1, LP, 1, O, 1);
}


}


/**
 * From each block of 2*BLOCK_SIZE columns of matrix `R * P**T` selects
 * BLOCK_SIZE columns such that the resulting block column has the greatest
 * possible rank.
 *
 * @tparam T  a numeric type
 *
 * @param[in] m  the number of rows of matrix `R`
 * @param[in] n  the number of columns of matrix `R`
 * @param[in,out] R  the memory location of an `m`-by-`n` matrix `R`
 * @param[in] ldr  the leading dimension of `R`
 * @param[in] P  an array containing the row indeces of ones in the
 *               permutation matrix `P`
 * @param[out] O  an array containing the row indeces of ones in the
 *                (incomplete) permutation matrix `O` such that each
 *                BLOCK_SIZE indeces represent a single selected block-column
 *                of maximal rank of matrix `R` (not `R * P**T`!)
 * @param[out] rank  the computed rank of the matrix **deprecated**
 * @param[in] tol  tolerance used for rank computation **deprecated**
 */
template <typename T>
inline void sbpc(const DMat<T> &R, int *P, int *O, int *rank, T tol,
                 cudaStream_t s)
{
    const dim3 b(2*BLOCK_SIZE, BLOCK_SIZE);
    const dim3 g(1, gpg::rud(R.n_cols, 2*BLOCK_SIZE));
    _sbpc::kernel<BLOCK_SIZE><<<g, b, 0, s>>>
        (R.n_rows, R.n_cols, R.data, R.ld, P, O, rank, tol);
}


}


#endif

