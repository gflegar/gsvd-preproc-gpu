#ifndef BCR_CUH_
#define BCR_CUH_


#include <base_defs.h>
#include <dmat.cuh>
#include <sbr.cuh>
#include <tbr.cuh>


namespace gpg {


/**
 * Computes the `R` factors in the QR decomposition of all block-columns of
 * `A * P**T`.
 *
 * For each block-column `B = (A*P**T)(:, i*2*BLOCK_SIZE:(i+1)*2*BLOCK_SIZE)`
 * computes a decomposition `Q*B = Rp` where `Q` is orthonormal and `R` is
 * upper triangular and saves `Rp` to
 * `R(:, i*2*BLOCK_SIZE:(i+1)*2*BLOCK_SIZE)`.
 *
 * @tparam T  a numeric type
 * 
 * @param[in] m  the number of rows of `A`
 * @param[in] n  the number of columns of `A`
 * @param[in] A  the memory location of `m`-by-`n` matrix `A`
 * @param[in] lda  the leading dimension of `A`
 * @param[out] R  the memory location of `m`-by-`n` matrix `R`
 * @param[in] ldr  the leading dimension of `R`
 * @param[in] P  an array containing the position of 1 in each row of `P`
 */
template <typename T>
void bcr(const DMat<T> &A, const DMat<T> &R, int *P, cudaStream_t s)
{
    sbr(A, R, P, s);
    for (int o = 1; o * 2*BLOCK_SIZE < A.n_rows; o <<= 1) {
        tbr(R, o, s);
    }
}


}

#endif

