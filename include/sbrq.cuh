#ifndef SBRQ_CUH_
#define SBRQ_CUH_


#include <base_defs.h>
#include <dmat.cuh>
#include <coppy_block.cuh>
#include <givens_rot.cuh>
#include <utils.cuh>


namespace gpg {


namespace _sbrq {


template <int bs, typename T>
__global__ void kernel(int ox, int oy, T * __restrict__ A, int lda,
                       T * __restrict__ Q, int ldq)
{
    const int ldl = 2*bs + 1;
    const int bx = blockIdx.x * bs;
    const int by = blockIdx.y * 2*bs;

    __shared__ T L[2*bs * ldl];

    ox -= bx;
    oy -= by;
    A += bx + by*lda;
    Q += bx + by*ldq;

    copyBlockFC<1, bs, 2*bs, 1, 1>(ox, oy, A, lda, L, ldl);
    L[threadIdx.x + (threadIdx.y + bs) * ldl] = 
        T((threadIdx.x - threadIdx.y) % bs == 0);

    for (int col = bs-1; col > 0; --col) {
        for (int ofs = 1; col - ofs >= 0; ofs <<= 1) {
            const int pt = threadIdx.y / (bs/2) * (bs/2);
            const int row = col - (2*(threadIdx.y-pt) + 1) * ofs + 2*pt;
            __syncthreads();
            if (row >= bs || (threadIdx.y <= bs/2) && row >= 0) {
                T c, s;
                genRot(L[row+ofs + col*ldl], L[row + col*ldl], c, s);
                applyRot<2*bs, 1, ldl>(c, s, L + row+ofs, L + row);
            }
        }
    }

    __syncthreads();

    copyBlockFCR<0, bs, 2*bs, 1, 1>(ox, oy, L, ldl, A, lda);
    copyBlockFCR<0, bs, 2*bs, 1, 1>(0, oy, L + bs*ldl, ldl, Q, ldq);
}


}


namespace sizes {


template <int bs>
inline dim3 sbrq_q(int m, int n)
{
    return dim3(rud(m, bs) * bs, n);
}


}

/**
 * Computes the RQ factorization of individual matrix blocks.
 *
 * For each `BLOCK_SIZE`-by-`BLOCK_SIZE` submatrix `B` of `A` computes a
 * factorization `B * Qp = Rp` where `Qp` is orthonormal and `Rp` is
 * upper triangular. If rows (columns) of `A` are not divisible by
 * `BLOCK_SIZE`, blocks in the first block-row (block-column) are shortened
 * to make the remaining number of rows (columns) divisible. If `Rp` is not
 * square it is "triangular" in the sense that the lower end of the triangle is
 * in the lower right corner of `Rp`.
 *
 * `B` is overwritten with `Rp` and `Qp` of the block `(i,j)` is stored in the
 * bottom of the `j`-th `BLOCK_SIZE` block of the `i`-th block colum. Each
 * block-column of `Q` is the same size as the coresponding block-column of
 * `A`.
 *
 * @tparam T  a numeric type
 *
 * @param[in] m  the number of rows of `A`
 * @param[in] n  the number of columns of `A`
 * @param[in,out] A  the memory location of the `m`-by-`n` matrix `A`
 * @param[in] lda  the leading dimension of `A`
 * @param[out] Q  the memory location of the
 *                `(floor(m/BLOCK_SIZE)*BLOCK_SIZE)`-by-`n` matrix `Q`
 * @param[in] ldq  the leading dimension of `Q`
 */
template <typename T>
inline void sbrq(const DMat<T> &A, const DMat<T> &Q, cudaStream_t s)
{
    const dim3 b(2*BLOCK_SIZE, BLOCK_SIZE);
    const dim3 g = gpg::ggs(dim3(BLOCK_SIZE, 2*BLOCK_SIZE),
                            A.n_rows, A.n_cols);
    const int ox = g.x*BLOCK_SIZE - A.n_rows;
    const int oy = g.y*2*BLOCK_SIZE - A.n_cols;
    _sbrq::kernel<BLOCK_SIZE><<<g, b, 0, s>>>
        (ox, oy, A.data - ox - oy*A.ld, A.ld, Q.data - oy*Q.ld, Q.ld);
}


}


#endif
