#ifndef DMAT_CUH_
#define DMAT_CUH_


#include <cstdio>


namespace gpg {


/**
 * This class represents a matrix allocated on the device.
 */
template <typename T>
class DMat {
public:
    /**
     * The pointer to device memory containing the matrix.
     */
    T *data;
    /**
     * The number of rows of the matrix.
     */
    int n_rows;
    /**
     * The number of columns of the matrix.
     */
    int n_cols;
    /**
     * The leading dimension of the matrix.
     */
    int ld;


    /**
     * Create an empty matrix.
     */
    DMat() : data(NULL), n_rows(0), n_cols(0), ld(0) {}


    /**
     * Creates a new matrix with from already allocated memory with `ld` equal
     * to the number of rows.
     *
     * @param data  the pointer to device memory
     * @param r  the number of rows of the matrix
     * @param c  the number of columns of the matrix
     */
    DMat(T *data, int r, int c) : data(data), n_rows(r), n_cols(c), ld(r) {}


    /**
     * Creates a new matrix with from already allocated memory with `ld` equal
     * to the number of rows.
     *
     * @param data  the pointer to device memory
     * @param dim  the dimensions of the matrix
     */
    DMat(T *data, dim3 dim) :
        data(data), n_rows(dim.x), n_cols(dim.y), ld(dim.x) {}


    /**
     * Creates a new matrix from already alocated memory.
     *
     * @param data  the pointe to device memory
     * @param r  the number of rows of the matrix
     * @param c  the number of columns of the matrix
     * @param ld  the leading dimension of the matrix
     */
    DMat(T *data, int r, int c, int ld) :
        data(data), n_rows(r), n_cols(c), ld(ld) {}


    /**
     * Creates a new matrix from already alocated memory.
     *
     * @param data  the pointe to device memory
     * @param dim  the dimensions of the matrix
     * @param ld  the leading dimension of the matrix
     */
    DMat(T *data, dim3 dim, int ld) :
        data(data), n_rows(dim.x), n_cols(dim.y), ld(ld) {}


    /**
     * Creates a submatrix view of this matrix.
     *
     * Row and column indeces can be less than zero. In that case they
     * designate rows/columns from the end. For example `A(0,-1,0,-1)` creates
     * a view of the whole matrix.
     *
     * @param[in] sr  the starting row of the submatrix
     * @param[in] er  the row after the last row of the submatrix
     * @param[in] sc  the starting column of the submatrix
     * @param[in] ec  the column after the last column of the submatrix
     *
     * @return A view of the matrix with elements (sr:er, sc:ec).
     */
    DMat operator()(int sr, int er, int sc, int ec) const
    {
        decode_row(sr);
        decode_row(er);
        decode_col(sc);
        decode_col(ec);
        return DMat(data + sr + sc*ld, er - sr, ec - sc, ld);
    }

private:
    void decode_row(int &r) const
    {
        if (r < 0) {
            r += n_rows + 1;
        }
    }


    void decode_col(int &c) const
    {
        if (c < 0) {
            c += n_cols + 1;
        }
    }
};


}


#endif

