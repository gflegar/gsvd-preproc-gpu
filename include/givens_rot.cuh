#ifndef GIVENS_ROT_CUH_
#define GIVENS_ROT_CUH_


namespace gpg {


/**
 * Generate Given's rotation that will cancel-out `y` when applied to `(x, y)`.
 *
 * @tparam T  a numeric type
 *
 * @param[in] x  explained above
 * @param[in] y  explained above
 * @param[out] c  cosine part of the rotation
 * @param[out] s  sine part of the rotation
 */
template <typename T>
__device__ void genRot(T x, T y, T &c, T &s)
{
    c = T(1);
    s = T(0);
    const T m = abs(x) + abs(y);
    if (m != T(0)) {
#ifdef ACCURATE_ROT
        x /= m;
        y /= m;
#endif
        const T d = rsqrt(x*x + y*y);
        c = x * d;
        s = -y * d;
    }
}

/**
 * Generate Given's rotation that will cancel-out `y` when applied to `(x, y)`.
 *
 * This version uses shared memory to decrease the number of required intrinsic
 * function calculations when multiple threads in a warp compute the same
 * rotation.
 *
 * @tparam bs  size of the block that computes the same rotation (< warpSize)
 * @tparam T  a numeric type
 *
 * @param[in] x  explained above
 * @param[in] y  explained above
 * @param[out] c  the cosine part of the rotation
 * @param[out] s  the sine part of the rotation
 * @param[out] shr  a shared memory location used to exchange information
 *                  between threads
 */
template <int bs, typename T>
__device__ void genRotS(const T x, const T y, T &c, T &s, volatile T &shr)
{
    c = T(1);
    s = T(0);
    if (abs(x) + abs(y) != T(0)) {
        if (threadIdx.x % bs == 0) {
            shr = rsqrt(x*x + y*y);
        }
        c = x * shr;
        s = -y * shr;
    }
}


/**
 * Apply a givens rotation to multiple blocks of two vectors.
 *
 * @tparam bs  threadBlock x dimension
 * @tparam nb  the number of block to calculate the rotation on
 * @tparam ofs  offset between two consecutive elements of vectors
 * @tparam T  a numeric type
 *
 * @param[in] c  the cosine part of givens rotation
 * @param[in] s  the sine part of givens rotation
 * @param[in,out] fr  first vector
 * @param[in,out] sr  second vector
 */
template <int bs, int nb, int ofs, typename T>
__device__ void applyRot(const T c, const T s, T * __restrict__ fr,
                         T * __restrict__ sr)
{
    for (int i = 0; i < nb; ++i) {
        const int ind = (threadIdx.x + bs*i)*ofs;
        const T x = fr[ind];
        const T y = sr[ind];
        fr[ind] = c*x - s*y;
        sr[ind] = s*x + c*y;
    }
}


/**
 * Apply a givens rotation to multiple blocks of two vectors.
 *
 * Allows specifying the length of the vectors.
 *
 * @tparam bs  threadBlock x dimension
 * @tparam nb  the number of block to calculate the rotation on
 * @tparam ofs  offset between two consecutive elements of vectors
 * @tparam T  a numeric type
 *
 * @param[in] n  the length of vectors `fr` and `sr`
 * @param[in] c  the cosine part of givens rotation
 * @param[in] s  the sine part of givens rotation
 * @param[in,out] fr  first vector
 * @param[in,out] sr  second vector
 */
template <int bs, int nb, int ofs, typename T>
__device__ void applyRotB(int n, const T c, const T s, T * __restrict__ fr,
                          T * __restrict__ sr)
{
    n -= threadIdx.x;
    for (int i = 0; i < nb; ++i) {
        if (bs*i < n) {
            const int ind = (threadIdx.x + bs*i)*ofs;
            const T x = fr[ind];
            const T y = sr[ind];
            fr[ind] = c*x - s*y;
            sr[ind] = s*x + c*y;
        }
    }
}


}

#endif

