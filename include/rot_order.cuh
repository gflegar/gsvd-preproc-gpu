#ifndef ROT_ORDER_CUH_
#define ROT_ORDER_CUH_


#include <queue>

#include <utils.cuh>


namespace gpg {


/**
 * Generates the rotation sequence for QR decomposition of single matrix block.
 *
 * @tparam bs  the size of the matrix block
 * @tparam ss  the number of steps to perform
 *
 * @param c_to
 * @param c_from
 * @param c_dp
 */
template <int bs, int ss>
void rot_order(int *to, int *from, int *dp)
{
    //int mat[bs][bs];

    std::priority_queue<int> todo[bs];

    for (int i = 0; i < bs; ++i) {
        todo[0].push(i);
    }

    for (int step = 0; step < ss; ++step) {
        int cf = 0;
        int ct = 0;
        for (int d = bs - 1; d >= 0; --d) {
            int take = todo[d].size() / 2;
            for (int i = 0; i < take; ++i) {
                int c = todo[d].top();
                todo[d].pop();
                //mat[c][d] = step;
                from[cf*ss + step] = c;
                dp[(cf++)*ss + step] = d;
                if (c > d) {
                    todo[d+1].push(c);
                }
            }
            for (int i = 0; i < take; ++i) {
                int c = todo[d].top();
                todo[d].pop();
                to[(ct++)*ss + step] = c;
            }
            for (int i = 0; i < take; ++i) {
                todo[d].push(to[(ct-i-1)*ss + step]);
            }
        }
        for (; cf < bs/2; ++cf) {
            from[cf*ss + step] = -1;
            to[cf*ss + step] = -1;
            dp[cf*ss + step] = -1;
        }
    }
    /*
    for (int i = 0; i < bs; ++i) {
        for (int j = 0; j < bs; ++j) {
            if (i > j) {
                printf("%2d ", mat[i][j]);
            } else {
                printf("-- ");
            }
        }
        printf("\n");
    }

    for (int t = 0; t < bs/2; ++t) {
        for (int i = 0; i < ss; ++i) {
            printf("%2d -> %2d (%2d);\t", from[t*ss + i], to[t*ss + i],
                   dp[t*ss + i]);
        }
        printf("\n");
    }
    */

}


}


#endif

