#ifndef SBQR_CUH_
#define SBQR_CUH_


#include <base_defs.h>
#include <dmat.cuh>
#include <coppy_block.cuh>
#include <givens_rot.cuh>
#include <rot_order.cuh>
#include <utils.cuh>


#ifndef NOCONSTANT
__constant__ int C_POOL[3 * (BLOCK_SIZE/2*SB_STEPS)];
#endif

namespace gpg {


namespace _sbqr {


template<int bs, typename T>
__global__ void
__launch_bounds__(512, 3)
#ifdef NOCONSTANT
kernel(int m, int n, T * __restrict__ A, int lda, T * __restrict__ Q, int ldq,
       int * __restrict__ C_POOL)
#else
kernel(int m, int n, T * __restrict__ A, int lda, T * __restrict__ Q, int ldq)
#endif
{
    int *CS = C_POOL;
    int *SS = C_POOL + bs/2 * SB_STEPS;
    int *PE = C_POOL + bs * SB_STEPS;
    const int ldl = 2*bs + 1;
    const int bx = blockIdx.x * 2*bs;
    const int by = blockIdx.y * bs;

    __shared__ T L[2*bs*ldl];

    m -= bx;
    n -= by;
    A += bx + by*lda;
    Q += bx + by*ldq;

    copyBlock<1, 2*bs, bs, 1, 1>(m, n, A, lda, L, ldl);
    L[threadIdx.x + (threadIdx.y + bs)*ldl] =
        ((threadIdx.x - threadIdx.y) % bs == 0) * T(1);

    for (int step = 0; step < SB_STEPS; ++step) {
        const int ind = threadIdx.y % (bs/2);
        const int cosr = CS[step + ind * SB_STEPS] + 2*(threadIdx.y - ind);
        const int sinr = SS[step + ind * SB_STEPS] + 2*(threadIdx.y - ind);
        const int col = PE[step + ind * SB_STEPS];
        __syncthreads();
        if (col != -1) {
            T c, s;
            genRot(L[cosr + col*ldl], L[sinr + col*ldl], c, s);
            applyRot<2*bs, 1, ldl>(c, s, L + cosr, L + sinr);
        }
    }

    __syncthreads();

    copyBlock<0, 2*bs, bs, 1, 1>(m, n, L, ldl, A, lda);
    copyBlock<0, 2*bs, bs, 1, 1>(m, bs, L + bs*ldl, ldl, Q, ldq);
}


}


namespace sizes {


template<int bs>
inline dim3 sbqr_q(int m, int n)
{
    return dim3(m, rud(n, bs) * bs);
}


}


/**
 * Computes the QR factorizations of individual matrix blocks.
 *
 * For each BLOCK_SIZE-by-BLOCK_SIZE submatrix
 * `B = A(i*BLOCK_SIZE:(i+1)*BLOCK_SIZE, j*BLOCK_SIZE:(j+1)*BLOCK_SIZE)`
 * computes a factorization `Qp * B = Rp` where `Qp` is orthonormal and `Rp`
 * upper triangular.
 *
 * Stores `Rp` to
 * `A(i+BLOCK_SIZE:(i+1)*BLOCK_SIZE, j*BLOCK_SIZE:(j+1)*BLOCK_SIZE)` and
 * `Qp` to `Q(i*BLOCK_SIZE:(i+1)*BLOCK_SIZE, j*BLOCK_SIZE, (j+1)*BLOCK_SIZE)`.
 *
 * @tparam T  a numeric type
 *
 * @param[in,out] A  the `m`-by-`n` matrix `A`
 * @param[out] Q  the matrix `Q` with dimensions given by
 *                `sizes::sbqr_q<BLOCK_SIZE>(m, n)`
 * @param[in] s  the stream on which the kernel should be run
 */
template<typename T>
inline void sbqr(const DMat<T>& A, const DMat<T>& Q, cudaStream_t s)
{
    static bool cset = false;
    const dim3 b(2*BLOCK_SIZE, BLOCK_SIZE);
    const dim3 g = ggs(b, A.n_rows, A.n_cols);
    const int cmsize = BLOCK_SIZE/2 * SB_STEPS;
#ifdef NOCONSTANT
    static int *C_POOL;
#endif
    if (!cset) {
        int H_POOL[3*cmsize];
        gpg::rot_order<BLOCK_SIZE, SB_STEPS>(H_POOL, H_POOL+cmsize,
                                             H_POOL+2*cmsize);
#ifdef NOCONSTANT
        cudaAssert(cudaMalloc(&C_POOL, 3*cmsize*sizeof(int)));
        cudaAssert(cudaMemcpy(C_POOL, H_POOL, 3*cmsize*sizeof(int),
                              cudaMemcpyHostToDevice));
#else
        cudaAssert(cudaMemcpyToSymbol(C_POOL, H_POOL, 3*cmsize * sizeof(int)));
#endif
        cset = true;
    }
#ifdef NOCONSTANT
    _sbqr::kernel<BLOCK_SIZE><<<g, b, 0, s>>>
        (A.n_rows, A.n_cols, A.data, A.ld, Q.data, Q.ld, C_POOL);
#else
    _sbqr::kernel<BLOCK_SIZE><<<g, b, 0, s>>>
        (A.n_rows, A.n_cols, A.data, A.ld, Q.data, Q.ld);
#endif
}


}


#endif

