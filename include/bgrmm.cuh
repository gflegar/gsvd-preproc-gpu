#ifndef BGRMM_CUH_
#define BGRMM_CUH_

#include <base_defs.h>
#include <coppy_block.cuh>
#include <utils.cuh>


namespace gpg {


namespace _bgrmm {


template <int bs, typename T>
__global__ void kernel(int m, int n, T * __restrict__ G, int ldg,
                       T * __restrict__ A, int lda, int ofs)
{
    const int ldl = 2*bs;
    const int bx = (2*blockIdx.x + threadIdx.x/bs) * ofs * bs -
                   (threadIdx.x/bs)*bs;
    const int by = blockIdx.y * bs;

    m -= bx;
    n -= by;
    G += bx;
    A += bx + by*lda;

    __shared__ T LG[2*bs*ldl];
    __shared__ T LA[bs*ldl];

    T r = 0.0;

    copyBlock<1, 2*bs, bs, 1, 2>(m, 2*bs, G, ldg, LG, ldl);
    copyBlock<1, 2*bs, bs, 1, 1>(m, n, A, lda, LA, ldl);

    __syncthreads();

    for (int k = 0; k < 2*bs; ++k) {
        r += LG[threadIdx.x + k*ldl] * LA[k + threadIdx.y*ldl];
    }

    if ((int)threadIdx.x < m && (int)threadIdx.y < n) {
        A[threadIdx.x + threadIdx.y*lda] = r;
    }
}


}


/**
 * Multiplies the matrix `A` with the `Q` factor obtained from the call to
 * tbqr().
 *
 * @tparam T  a numeric type
 *
 * @param[in] Q  the `Q` factor from tbqr()
 * @param[in,out] A  the `m`-by-`n` matrix `A`
 * @param[in] offset  offset used in the call to gpg::tbqr
 * @param[in] s  the CUDA stream used for computation
 *
 * @see gpg::tbqr
 */
template <typename T>
inline void bgrmm(const DMat<T> &Q, const DMat<T> &A, int ofs, cudaStream_t s)
{
    dim3 b(2*BLOCK_SIZE, BLOCK_SIZE);
    dim3 g = ggs(b, A.n_rows, A.n_cols);
    g.x = rud(g.x, ofs);
    _bgrmm::kernel<BLOCK_SIZE><<<g, b, 0, s>>>
        (A.n_rows, A.n_cols, Q.data, Q.ld, A.data, A.ld, ofs);
}


}


#endif

