#ifndef BLOCK_VIEW_H_
#define BLOCK_VIEW_H_


#include <vector>
#include <stdexcept>
#include <armadillo>


namespace gpg {


/**
 * This class defines a list of block boundaries of a BlockView.
 */
class BlockConfig {
    template <typename T>
    friend class BlockView;

private:
    /**
     * A list of block boundaries.
     */
    std::vector<int> splits;

    /**
     * Returns the span of the `i`-th block.
     *
     * @param i  block-index
     */
    arma::span span(int i) const
    {
        if (i+1 >= splits.size()) {
            throw std::runtime_error("index out of range");
        }
        return arma::span(splits[i], splits[i+1] - 1);
    }

public:
    /**
     * Creates an empty BlockConfig.
     */
    BlockConfig() : splits(1, 0) {}

    /**
     * Adds `count` blocks of size `size` to the end of the list.
     *
     * @param size  size of each block
     * @param count  number of blocks
     *
     * @return this
     */
    BlockConfig& add(int size, int count = 1)
    {
        if (size <= 0 || count <= 0) {
            return *this;
        }
        for (int i = 0; i < count; ++i) {
            splits.push_back(splits.back() + size);
        }
        return *this;
    }
};

/**
 * This class creates a block-matrix view of the underlaying matrix.
 *
 * @tparam T  the underlaying matrix type
 */
template <typename T>
class BlockView {
private:
    /**
     * The underlaying matrix.
     */
    T &M;

public:
    /**
     * BlockConfig describing the row boundaries of the matrix.
     */
    BlockConfig rblocks;

    /**
     * BlockConfig describing the column boundaries of the matrix.
     */
    BlockConfig cblocks;

    /**
     * Creates a BlockView of matrix `M` with zero block-rows and
     * block-columns.
     */
    BlockView(T &M) : M(M) {}

    /**
     * Returns the `(i,j)`-th block as defined by rblocks and cblocks.
     *
     * @param i  block-row index
     * @param j  block-column index
     *
     * @return the `(i,j)`-th block
     */
    arma::subview<typename T::elem_type> operator ()(int i, int j)
    {
        return M(rblocks.span(i), cblocks.span(j));
    }

    /**
     * Returns the constant `(i,j)`-th block as defined by rblocks and cblocks.
     *
     * @param i  block-row index
     * @param j  block-column index
     *
     * @return the `(i,j)`-th block
     */
    const arma::subview<typename T::elem_type> operator ()(int i, int j) const
    {
        return M(rblocks.span(i), cblocks.span(j));
    }


    /**
     * Returns the number of block-rows.
     */
    int n_rows() const { return rblocks.splits.size() - 1; }

    /**
     * Return the number of block-columns.
     */
    int n_cols() const { return cblocks.splits.size() - 1; }
};


}


#endif

