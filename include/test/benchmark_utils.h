#ifndef BENCHMARK_UTILS_H_
#define BENCHMARK_UTILS_H_

#include <stdexcept> #include <algorithm>
#include <cstdio>
#include <sstream>
#include <cmath>
#include <iostream>


extern "C" double dnrm2_(int *n, double *x, int *incx);
extern "C" float snrm2_(int *n, float *x, int *incx);
extern "C" void dgemm_(char *transA, char *transB, int *m, int *n, int *k,
                       double *alpha, double *A, int *lda, double *B, int *ldb,
                       double *beta, double *C, int *ldc);
extern "C" void sgemm_(char *transA, char *transB, int *m, int *n, int *k,
                       float *alpha, float *A, int *lda, float *B, int *ldb,
                       float *beta, float *C, int *ldc);
extern "C" void dtgsja_(char *jobu, char *jobv, char *jobq, int *m, int *p,
                        int *n, int *k, int *l, double *A, int *lda, double *B,
                        int *ldb, double *tola, double *tolb, double *alpha,
                        double *beta, double *U, int *ldu, double *V, int *ldv,
                        double *Q, int *ldq, double *work, int *ncycle,
                        int *info);
extern "C" void stgsja_(char *jobu, char *jobv, char *jobq, int *m, int *p,
                        int *n, int *k, int *l, float *A, int *lda, float *B,
                        int *ldb, float *tola, float *tolb, float *alpha,
                        float *beta, float *U, int *ldu, float *V, int *ldv,
                        float *Q, int *ldq, float *work, int *ncycle,
                        int *info);
extern "C" void dggsvd_(char *jobu, char *jobv, char *jobq, int *m, int *p,
                        int *n, int *k, int *l, double *A, int *lda, double *B,
                        int *ldb, double *alpha, double *beta, double *U,
                        int *ldu, double *V, int *ldv, double *Q, int *ldq,
                        double *work, int *iwork, int *info);
extern "C" void sggsvd_(char *jobu, char *jobv, char *jobq, int *m, int *p,
                        int *n, int *k, int *l, float *A, int *lda, float *B,
                        int *ldb, float *alpha, float *beta, float *U,
                        int *ldu, float *V, int *ldv, float *Q, int *ldq,
                        float *work, int *iwork, int *info);


template <typename T>
T nrm(int sz, T *A) { throw std::logic_error("Unsupported type!"); }
template <>
double nrm<double>(int sz, double *A) {int o = 1; return dnrm2_(&sz, A, &o);}
template <>
float nrm<float>(int sz, float *A) {int o = 1; return snrm2_(&sz, A, &o);}


template <typename T>
T gettol(int m, int n, T *A)
{
    const T eps = std::numeric_limits<T>::epsilon();
    return nrm(m*n, A) * std::max(m, n) * eps;
}


template <typename T>
void gemm(char transA, char transB, int m, int n, int k, T alpha, T *A,
          int lda, T *B, int ldb, T beta, T *C, int ldc)
{
    throw std::logic_error("Unsupported type!");
}


template <>
void gemm<double>(char transA, char transB, int m, int n, int k, double alpha,
                  double *A, int lda, double *B, int ldb, double beta,
                  double *C, int ldc)
{
    dgemm_(&transA, &transB, &m, &n, &k, &alpha, A, &lda, B, &ldb, &beta, C,
           &ldc);
}
template <>
void gemm<float>(char transA, char transB, int m, int n, int k, float alpha,
                 float *A, int lda, float *B, int ldb, float beta, float *C,
                 int ldc)
{
    sgemm_(&transA, &transB, &m, &n, &k, &alpha, A, &lda, B, &ldb, &beta, C,
           &ldc);
}


template <typename T>
void tgsja(int m, int p, int n, int k, int l, T *A, int lda, T *B, int ldb,
           T tola, T tolb, T *alpha, T *beta)
{
    throw std::logic_error("Unsupported type!");
}


template <>
void tgsja<double>(int m, int p, int n, int k, int l, double *A, int lda,
                   double *B, int ldb, double tola, double tolb, double *alpha,
                   double *beta)
{
    char nc = 'N';
    int ld = 1;
    int ncycle, info;
    double *work = new double[2*n];
    dtgsja_(&nc, &nc, &nc, &m, &p, &n, &k, &l, A, &lda, B, &ldb, &tola, &tolb,
            alpha, beta, NULL, &ld, NULL, &ld, NULL, &ld, work, &ncycle,
            &info);
    if (info != 0) {
        throw std::runtime_error("Error calling dtgsja_");
    }
    delete[] work;
}


template <>
void tgsja<float>(int m, int p, int n, int k, int l, float *A, int lda,
                  float *B, int ldb, float tola, float tolb, float *alpha,
                  float *beta)
{
    char nc = 'N';
    int ld = 1;
    int ncycle, info;
    float *work = new float[2*n];
    stgsja_(&nc, &nc, &nc, &m, &p, &n, &k, &l, A, &lda, B, &ldb, &tola, &tolb,
            alpha, beta, NULL, &ld, NULL, &ld, NULL, &ld, work, &ncycle,
            &info);
    if (info != 0) {
        throw std::runtime_error("Error calling stgsja_");
    }
    delete[] work;
}


template <typename T>
void ggsvd(int m, int p, int n, int *k, int *l, T *A, int lda, T *B, int ldb,
           T *alpha, T *beta)
{
    throw std::logic_error("Unsupported type!");
}


template <>
void ggsvd<double>(int m, int p, int n, int *k, int *l, double *A, int lda,
                   double *B, int ldb, double *alpha, double *beta)
{
    char nc = 'N';
    int ld = 1;
    int info;
    int ws = std::max(std::max(3*n, m), p) + n;
    //std::cerr << "WS: " << ws << std::endl;
    double *work = new double[ws];
    //std::cerr << "OK" << std::endl;
    int *iwork = new int[n];
    //std::cerr << "OK" << std::endl;
    //std::cerr << "M: " << m << " P: " << p << " N: " << n << std::endl;
    //std::cerr << "lda: " << lda << " ldb: " << ldb << std::endl;
    dggsvd_(&nc, &nc, &nc, &m, &n, &p, k, l, A, &lda, B, &ldb, alpha, beta,
            NULL, &ld, NULL, &ld, NULL, &ld, work, iwork, &info);

    //std::cerr << "OK" << std::endl;
    if (info != 0) {
        throw std::runtime_error("Error calling dggsvd_");
    }
    delete[] work;
    delete[] iwork;
}


template <>
void ggsvd<float>(int m, int p, int n, int *k, int *l, float *A, int lda,
                  float *B, int ldb, float *alpha, float *beta)
{
    char nc = 'N';
    int ld = 1;
    int info;
    float *work = new float[std::max(std::max(3*n, m), p) + n];
    int *iwork = new int[n];

    sggsvd_(&nc, &nc, &nc, &m, &n, &p, k, l, A, &lda, B, &ldb, alpha, beta,
            NULL, &ld, NULL, &ld, NULL, &ld, work, iwork, &info);

    if (info != 0) {
        throw std::runtime_error("Error calling sggsvd_");
    }
    delete[] work;
    delete[] iwork;
}


template <typename T>
T fwerr(int m, int p, int n, int k, int l, T *A, int lda, T *B, int ldb,
        T *RA, T * RB, T tola, T tolb) {
    // first, compute the generalized singular values of upper triangular pair
    // (RA, RB)
    T *calpha = new T[n];
    T *cbeta = new T[n];

    tgsja(m, p, n, k, l, RA, lda, RB, ldb, tola, tolb, calpha, cbeta);

    for (int i = 0; i < l; ++i) {
        calpha[i+k] /= cbeta[i+k];
    }

    std::sort(calpha + k, calpha + k + l);

    // now, compute the "real" generalized singular values of (A,B)

    T *alpha = new T[n];
    T *beta = new T[n];
    int rk, rl;

    ggsvd(m, p, n, &rk, &rl, A, lda, B, ldb, alpha, beta);


    //std::cerr << "OK" << std::endl;

    for (int i = 0; i < rl; ++i) {
        alpha[i+rk] /= beta[i+rk];
    }

    std::sort(alpha + rk, alpha + rl + rk);

    //std::cerr << "OK" << std::endl;

    // last, compute the errors:
    T res = T(0);
    int svs = std::min(l, rl);
    for (int i = 0; i < svs; ++i) {
        //std::cerr << calpha[i+k] << '\t' << alpha[i+rk] << std::endl;
        res += std::abs(calpha[i+k] - alpha[i+rk]) /
               std::max(std::abs(calpha[i+k]), std::abs(alpha[i+rk]));
    }

    //std::cerr << "OK" << std::endl;

    res /= svs;

    delete[] calpha;
    delete[] cbeta;
    delete[] alpha;
    delete[] beta;

    return res;
}


template <typename T>
T diff(int m, int n, T *U, T *A, T *V, T* R)
{
    T *tmp = new T[m*n];
    T *tmp2 = new T[m*n];

    std::copy(R, R + m*n, tmp2);
    T dn = nrm(m*n, R);
    gemm('N', 'N', m, n, m, T(1), U, m, A, m, T(0), tmp, m);
    gemm('N', 'T', m, n, n, T(-1), tmp, m, V, n, T(1), tmp2, m);
    T nm = nrm(m*n, tmp2);
    delete[] tmp;
    delete[] tmp2;
    if (dn == 0.0) {
        dn = 1.0;
    }
    return nm / dn;
}


template <typename T>
void to_upper_triangle(int m, int n, int r, T *A, int lda)
{
    for (int j = 0; j < n; ++j) {
        for (int i = std::max(j + r - n + 1, 0); i < m; ++i) {
            A[i + j*lda] = T(0);
        }
    }
}


#endif


