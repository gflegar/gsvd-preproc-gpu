#ifndef GSVD_TEST_PREDICATES_H_
#define GSVD_TEST_PREDICATES_H_


#include <limits>
#include <armadillo>
#include <algorithm>


namespace gpg {

/**
 * Check if relative distance between two matrices is less than \p `eps *
 * size(A)`.
 *
 * @tparam T  matrix element type
 *
 * @param A  first matrix
 * @param B  second matrix
 * @param eps  tolerance
 *
 * @return `True` if `||A - B|| / || A || < eps * size(A)`.
 */
template<typename T>
bool matricesNear(const arma::Mat<T> &A, const arma::Mat<T> &B, T eps)
{
    T nrm = arma::norm(A, "fro");
    if (nrm == T(0)) nrm = T(1);
    return arma::norm(A - B, "fro") < eps * nrm * A.n_elem;
}


/**
 * Extract upper triangle of matrix.
 *
 * @tparam T  matrix element type
 *
 * @param A  a matrix
 * @param offset  the number of diagonals above the main diagonal to clear
 *                (ex. a value of 0 clears the main diagonal, value of 1 clears
 *                one diagonal above the main one, and negative values leave
 *                diagonals bellow the main one)
 * @return Upper triangle of \p A.
 */
template<typename T>
arma::Mat<T> upperTriangle(const arma::Mat<T> &A, int offset = -1)
{
    arma::Mat<T> R(A);
    for (int i = 0; i < R.n_cols; ++i) {
        for (int j = std::max(0, i - offset); j < R.n_rows; ++j) {
            R(j, i) = T(0);
        }
    }
    return R;
}


/**
 * Copies a matrix to the allocated memory on the device.
 *
 * @tparam T  the matrix element type
 *
 * @param[in] A  a matrix allocated on the host
 * @param[out] d_A  a device memory location
 * @param[in] lda  the leading dimension of `d_A`
 */
template <typename T>
void copyToDevice(const arma::Mat<T> &A, T* d_A, int lda)
{
    const size_t tsize = sizeof(T);
    cudaAssert(cudaMemcpy2D(d_A, lda*tsize, A.memptr(), A.n_rows*tsize,
            A.n_rows*tsize, A.n_cols, cudaMemcpyHostToDevice));
}


/**
 * Copies a matrix to the device (and allocates memory for it).
 *
 * @tparam T  the matrix element type
 *
 * @param[in] A  a matrix allocated on the host
 * @param[out] d_A  the newly allocated device memory
 * @param[out] lda  the leading dimension of `d_A`
 */
template <typename T>
void createOnDevice(const arma::Mat<T> &A, T* &d_A, int &lda)
{
    const size_t tsize = sizeof(T);
    size_t pitch;
    cudaAssert(cudaMallocPitch(&d_A, &pitch, A.n_rows*tsize, A.n_cols));
    lda = pitch / tsize;
    copyToDevice(A, d_A, lda);
}


/**
 * Copies a matrix from the device.
 *
 * @tparam T  the matrix element type
 *
 * @param[out] A  a matrix allocated on the host
 * @param[in] d_A  the device memory location containing the matrix
 * @param[in] lda  the leading dimension of `d_A`
 */
template <typename T>
void copyFromDevice(arma::Mat<T> &A, const T* d_A, int lda)
{
    const size_t tsize = sizeof(T);
    cudaAssert(cudaMemcpy2D(A.memptr(), A.n_rows*tsize, d_A, lda*tsize,
             A.n_rows*tsize, A.n_cols, cudaMemcpyDeviceToHost));
}


/**
 * Moves a matrix from the device, freeing the allocated memory afterwards.
 *
 * @tparam T  the matrix element type
 *
 * @param[out] A  a matrix allocated on the host
 * @param[in] d_A  the device memory containing the matrix
 * @param[in] lda  the leading dimension of \p d_A
 */
template <typename T>
void moveFromDevice(arma::Mat<T> &A, T* d_A, int lda)
{
    copyFromDevice(A, d_A, lda);
    cudaAssert(cudaFree(d_A));
}


}

#endif


