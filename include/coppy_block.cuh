#ifndef COPPY_BLOCK_CUH_
#define COPPY_BLOCK_CUH_


namespace gpg {


/**
 * Copy multiple blocks of memory.
 *
 * @tparam init  determines whether to initialize leftovers of last blocks
 *               with zeros (1 - yes, 0 - no)
 * @tparam bsX  threadBlock x dimension
 * @tparam bsY  threadBlock y dimension
 * @tparam nX  the number of blocks to copy in x dimension
 * @tparam nY  the nuber of blocks to copy in y dimension
 * @tparam T  element type
 *
 * @param[in] sX  the number of rows (x-dimension) of `A`
 * @param[in] sY  the number of columns (y-dimension) of `A`
 * @param[in] A  the memory location to copy from
 * @param[in] lda  the leading dimension of `A`
 * @param[out] B  the memory location to copy to
 * @param[in] ldb  the leading dimension of `B`
 */
template <int init, int bsX, int bsY, int nX, int nY, typename T>
__device__ void copyBlock(int sX, int sY, const T * A, int lda,
                          T * B, int ldb)
{
    for (int j = 0; j < nY; ++j) {
        for (int i = 0; i < nX; ++i) {
            const int cx = threadIdx.x + i*bsX;
            const int cy = threadIdx.y + j*bsY;
            const int bi = cx + cy*ldb;
            if (cx < sX && cy < sY) {
                const int ai = cx + cy*lda;
                B[bi] = A[ai];
            } else if (init) {
                B[bi] = T(0);
            }
        }
    }
}



/**
 * Copy multiple blocks of memory and transpose them.
 *
 * Bad access patterns happen on the blocks of `B`.
 *
 * @tparam init  determines whether to initialize leftovers of first blocks
 *               with zeros (1 - yes, 0 - no)
 * @tparam bsX  threadBlock x dimension
 * @tparam bsY  threadBlock y dimension
 * @tparam nX  the number of blocks to copy in x dimension
 * @tparam nY  the nuber of blocks to copy in y dimension
 * @tparam T  element type
 *
 * @param[in] oX  the number of first rows (x-dimension) of `A` to ignore
 * @param[in] oY  the number of first columns (y-dimension) of `A` to ignore
 * @param[in] A  the memory location to copy from
 * @param[in] lda  the leading dimension of `A`
 * @param[out] B  the memory location to copy to
 * @param[in] ldb  the leading dimension of `B`
 */
template <int init, int bsX, int bsY, int nX, int nY, typename T>
__device__ void copyBlockFC(int oX, int oY, const T *A, int lda, T *B, int ldb)
{
    const int tx = threadIdx.x % bsX;
    const int ty = threadIdx.x / bsX + 2 * threadIdx.y;
    for (int j = 0; j < nY; ++j) {
        for (int i = 0; i < nX; ++i) {
            const int cx = tx + i*bsX;
            const int cy = ty + j*bsY;
            const int bi = cy + cx*ldb;
            if (cx >= oX && cy >= oY) {
                const int ai = cx + cy*lda;
                B[bi] = A[ai];
            } else if (init) {
                B[bi] = T(0);
            }
        }
    }
}


/**
 * Copy multiple blocks of memory and transpose them.
 *
 * Bad access patterns happen on the blocks of `A`.
 *
 * @tparam init  determines whether to initialize leftovers of first blocks
 *               with zeros (1 - yes, 0 - no)
 * @tparam bsX  threadBlock x dimension
 * @tparam bsY  threadBlock y dimension
 * @tparam nX  the number of blocks to copy in x dimension
 * @tparam nY  the nuber of blocks to copy in y dimension
 * @tparam T  element type
 *
 * @param[in] oX  the number of first rows (x-dimension) of `B` to ignore
 * @param[in] oY  the number of first columns (y-dimension) of `B` to ignore
 * @param[in] A  the memory location to copy from
 * @param[in] lda  the leading dimension of `A`
 * @param[out] B  the memory location to copy to
 * @param[in] ldb  the leading dimension of `B`
 */
template <int init, int bsX, int bsY, int nX, int nY, typename T>
__device__ void copyBlockFCR(int oX, int oY, const T *A, int lda, T *B,
                             int ldb)
{
    const int tx = threadIdx.x % bsX;
    const int ty = threadIdx.x / bsX + 2*threadIdx.y;
    for (int j = 0; j < nY; ++j) {
        for (int i = 0; i < nX; ++i) {
            const int cx = tx + i*bsX;
            const int cy = ty + j*bsY;
            const int bi = cx + cy*ldb;
            if (cx >= oX && cy >= oY) {
                const int ai = cy + cx*lda;
                B[bi] = A[ai];
            } else if (init) {
                B[bi] = T(0);
            }
        }
    }
}


/**
 * Copy multiple blocks of memory.
 *
 * Columns of matrix `A` are permuted by transposition matrix `P` before the
 * copy.
 *
 * @tparam init  determines whether to initialize leftovers of last blocks
 *               with zeros (1 - yes, 0 - no)
 * @tparam bsX  threadBlock x dimension
 * @tparam bsY  threadBlock y dimension
 * @tparam nX  the number of blocks to copy in x dimension
 * @tparam nY  the nuber of blocks to copy in y dimension
 * @tparam T  element type
 *
 * @param[in] sX  the number of rows (x-dimension) of `A`
 * @param[in] sY  the number of columns (y-dimension) of `A`
 * @param[in] A  the memory location to copy from
 * @param[in] lda  the leading dimension of `A`
 * @param[out] B  the memory location to copy to
 * @param[in] ldb  the leading dimension of `B`
 * @param[in] P  permutation used on matrix `A` before the copy
 */
template <int init, int bsX, int bsY, int nX, int nY, typename T>
__device__ void copyBlockP(int sX, int sY, const T *A, int lda, T *B, int ldb,
                           const int *P)
{
    for (int j = 0; j < nY; ++j) {
        for (int i = 0; i < nX; ++i) {
            const int cx = threadIdx.x + i*bsX;
            const int cy = threadIdx.y + j*bsY;
            const int bi = cx + cy*ldb;
            if (cx < sX && cy < sY) {
                const int ai = cx + P[cy]*lda;
                B[bi] = A[ai];
            } else if (init) {
                B[bi] = T(0);
            }
        }
    }
}


/**
 * Copies upper triangular part of a memory consisting of multiple blocks.
 *
 * @tparam init  determines whether to initialize leftovers of last blocks
 *               with zeros (1 - yes, 0 - no)
 * @tparam zf  determines whether to initialize the lower triangle with zeros
 * @tparam bsX  threadBlock x dimension
 * @tparam bsY  threadBlock y dimension
 * @tparam nX  the number of blocks to copy in x dimension
 * @tparam nY  the nuber of blocks to copy in y dimension
 * @tparam T  element type
 *
 * @param[in] sX  the number of rows (x-dimension) of `A`
 * @param[in] sY  the number of columns (y-dimension) of `A`
 * @param[in] A  the memory location to copy from
 * @param[in] lda  the leading dimension of `A`
 * @param[out] B  the memory location to copy to
 * @param[in] ldb  the leading dimension of `B`
 */
template <int init, int zf, int bsX, int bsY, int nX, int nY, typename T>
__device__ void copyBlockUT(int sX, int sY, const T *A, int lda, T *B, int ldb)
{
    for (int j = 0; j < nY; ++j) {
        for (int i = 0; i < nX; ++i) {
            const int cx = threadIdx.x + i*bsX;
            const int cy = threadIdx.y + j*bsY;
            const int bi = cx + cy*ldb;
            if (cx < sX && cy < sY) {
                if (cx <= cy) {
                    const int ai = cx + cy*lda;
                    B[bi] = A[ai];
                } else if (zf) {
                    B[bi] = T(0);
                }
            } else if (init) {
                B[bi] = T(0);
            }
        }
    }
}


/**
 * Copies upper triangular part of memory consisting of multiple blocks to
 * the lower triangle of destination.
 *
 * Conversion from upper-triangular to lower-triangular block is done by
 * fliping the block horizontaly and shifting the rows to the left.
 *
 * @tparam init  determines whether to initialize leftovers of last blocks
 *               with zeros (1 - yes, 0 - no)
 * @tparam bsX  threadBlock x dimension
 * @tparam bsY  threadBlock y dimension
 * @tparam nX  the number of blocks to copy in x dimension
 * @tparam nY  the nuber of blocks to copy in y dimension
 * @tparam T  element type
 *
 * @param[in] sX  the number of rows (x-dimension) of `A`
 * @param[in] sY  the number of columns (y-dimension) of `A`
 * @param[in] A  the memory location to copy from
 * @param[in] lda  the leading dimension of `A`
 * @param[out] B  the memory location to copy to
 * @param[in] ldb  the leading dimension of `B`
 */
template <int init, int bsX, int bsY, int nX, int nY, typename T>
__device__ void copyBlockUTLT(int sX, int sY, const T *A, int lda, T *B,
                              int ldb)
{
    for (int j = 0; j < nY; ++j) {
        for (int i = 0; i < nX; ++i) {
            const int cx = threadIdx.x + i*bsX;
            const int cy = threadIdx.y + j*bsY;
            if (cx <= cy) {
                const int bi = bsX*nX-1 - cx + (cy-cx)*ldb;
                if (cx < sX && cy < sY) {
                    const int ai = cx + cy*lda;
                    B[bi] = A[ai];
                } else if (init) {
                    B[bi] = T(0);
                }
            }
        }
    }
}


/**
 * Copies lower triangular part of memory obtained by a call to copyBlockUTLT()
 * back to upper triangular block of memory.
 *
 * @tparam init  determines whether to initialize leftovers of last blocks
 *               with zeros (1 - yes, 0 - no)
 * @tparam zf  determines whether to initialize the lower triangle with zeros
 * @tparam bsX  threadBlock x dimension
 * @tparam bsY  threadBlock y dimension
 * @tparam nX  the number of blocks to copy in x dimension
 * @tparam nY  the nuber of blocks to copy in y dimension
 * @tparam T  element type
 *
 * @param[in] sX  the number of rows (x-dimension) of `A`
 * @param[in] sY  the number of columns (y-dimension) of `A`
 * @param[in] A  the memory location to copy from
 * @param[in] lda  the leading dimension of `A`
 * @param[out] B  the memory location to copy to
 * @param[in] ldb  the leading dimension of `B`
 */
template <int init, int zf, int bsX, int bsY, int nX, int nY, typename T>
__device__ void copyBlockLTUT(int sX, int sY, const T *A, int lda, T *B,
                              int ldb)
{
    for (int j = 0; j < nY; ++j) {
        for (int i = 0; i < nX; ++i) {
            const int cx = threadIdx.x + i*bsX;
            const int cy = threadIdx.y + j*bsY;
            const int bi = cx + cy*ldb;
            if (cx < sX && cy < sY) {
                if (cx <= cy) {
                    const int ai = bsX*nX-1 - cx + (cy-cx)*lda;
                    B[bi] = A[ai];
                } else if (zf) {
                    B[bi] = T(0);
                }
            } else if (init) {
                B[bi] = T(0);
            }
        }
    }
}


}

#endif

