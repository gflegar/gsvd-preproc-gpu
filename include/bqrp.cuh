#ifndef BQRP_CUH_
#define BQRP_CUH_


#include <algorithm>
#include <vector>
#include <iostream>


#include <base_defs.h>
#include <dmat.cuh>
#include <bcfp.cuh>
#include <bcp.cuh>
#include <bcqr.cuh>


namespace gpg {


namespace sizes {


template <int bs>
inline dim3 bqrp_w(int m, int n)
{
    dim3 wp = bcfp_w<bs>(m, n);
    dim3 wqr = bcqr_w<bs>(m, n);
    return dim3(max(wp.x, wqr.x), wp.y + wqr.y);
}


template <int bs>
inline dim3 bqrp_iw(int m, int n)
{
    dim3 wp = bcfp_p<bs>(m, n);
    dim3 wip = bcfp_iw<bs>(m, n);
    return dim3(wp.x + wip.x);
}


}


template <typename T>
void bqrp(DMat<T> A, cudaStream_t astream, std::vector<DMat<T> > Qs,
          std::vector<cudaStream_t> qstreams, std::vector<DMat<T> > Ps,
          std::vector<cudaStream_t> pstreams, DMat<T> work, int *iwork,
          int &rank, T tol)
{
    const int nqs = Qs.size();
    const int nb = std::min(A.n_rows, A.n_cols);
    Qs.resize(nqs + 1);
    qstreams.push_back(astream);
    Ps.push_back(A);
    pstreams.push_back(astream);

    const int split = sizes::bcfp_w<BLOCK_SIZE>(A.n_rows, A.n_cols).y;
    const int isplit = sizes::bcfp_p<BLOCK_SIZE>(A.n_rows, A.n_cols).y;

    DMat<T> wp = work(0,-1, 0, split);
    DMat<T> wqr = work(0, -1, split, -1);
    int crank = 0;
    rank = 0;

    cudaEvent_t pivotFound;
    cudaAssert(cudaEventCreateWithFlags(&pivotFound, cudaEventDisableTiming));

    std::vector<cudaEvent_t> pivotCompleted(Ps.size());
    for (int i = 0; i < Ps.size(); ++i) {
        cudaAssert(cudaEventCreateWithFlags(
                &pivotCompleted[i], cudaEventDisableTiming));
    }
    std::vector<cudaEvent_t> multCompleted(nqs);
    for (int i = 0; i < nqs; ++i) {
        cudaAssert(cudaEventCreateWithFlags(
                &multCompleted[i], cudaEventDisableTiming));
    }

    for (int b = 0; b < nb; b += BLOCK_SIZE) {
        for (int i = 0; i < Ps.size(); ++i) {
            cudaAssert(cudaStreamWaitEvent(astream, pivotCompleted[i], 0));
        }
        bcfp(A, wp, iwork, iwork+isplit, crank, tol, astream);
        cudaAssert(cudaEventRecord(pivotFound, astream));

        //std::cout << "crank: " << crank << "\n";

        //cudaAssert(cudaDeviceSynchronize());
        for (int i = Ps.size()-1; i >= 0; --i) {
            cudaAssert(cudaStreamWaitEvent(pstreams[i], pivotFound, 0));
            bcp(Ps[i], iwork, pstreams[i]);
            cudaAssert(cudaEventRecord(pivotCompleted[i], pstreams[i]));
        }

        for (int i = 0; i < Ps.size(); ++i) {
            Ps[i] = Ps[i](0, -1, BLOCK_SIZE, -1);
        }

        //cudaAssert(cudaDeviceSynchronize());
        /*int *bla = new int[BLOCK_SIZE];
        cudaMemcpy(bla, iwork, sizeof(int)*BLOCK_SIZE,
                   cudaMemcpyDeviceToHost);
        std::cout << "DEBUG (A is " << A.n_rows << " x " << A.n_cols << "):";
        for (int i = 0; i < BLOCK_SIZE; ++i) {
            std::cout << bla[i] << ' ';
        }
        std::cout << "; crank = " << crank << std::endl;
        delete[] bla;*/
        //cudaAssert(cudaDeviceSynchronize());
        Qs[nqs] = A(0, -1, BLOCK_SIZE, -1);
        if (Qs[nqs].n_cols <= 0) {
            Qs.pop_back();
        }
        for (int i = 0; i < nqs; ++i) {
            cudaAssert(cudaStreamWaitEvent(astream, multCompleted[i], 0));
        }
        bcqr(A, astream, Qs, qstreams, wqr);
        for (int i = 0; i < nqs; ++i) {
            cudaAssert(cudaEventRecord(multCompleted[i], qstreams[i]));
        }
        A = A(BLOCK_SIZE, -1, BLOCK_SIZE, -1);
        for (int i = 0; i < nqs; ++i) {
            Qs[i] = Qs[i](BLOCK_SIZE, -1, 0, -1);
        }

        //cudaAssert(cudaDeviceSynchronize());
        rank += crank;
        if (crank < BLOCK_SIZE) break;
    }

    cudaAssert(cudaEventDestroy(pivotFound));
    for (int i = 0; i < Ps.size(); ++i) {
        cudaAssert(cudaEventDestroy(pivotCompleted[i]));
    }
    for (int i = 0; i < nqs; ++i) {
        cudaAssert(cudaEventDestroy(multCompleted[i]));
    }
}



}


#endif

