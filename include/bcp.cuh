#ifndef BCP_CUH_
#define BCP_CUH_


#include <base_defs.h>
#include <utils.cuh>
#include <dmat.cuh>


namespace gpg {


namespace _bcp {


template<int bs, typename T>
__global__ void kernel(int m, int n, T* __restrict__ A, int lda,
                       int* __restrict__ P)
{
    const int r = threadIdx.x + blockIdx.x * bs;
    const int c = threadIdx.y + blockIdx.y * bs;
    volatile __shared__ int sP[bs];
    volatile __shared__ int sI[bs];
    T a, b;
    if (threadIdx.y == 0 && threadIdx.x < n) {
        sP[threadIdx.x] = P[threadIdx.x];
        if (sP[threadIdx.x] < n) {
            sI[threadIdx.x] = -1;
        } else {
            sI[threadIdx.x] = sP[threadIdx.x];
        }
        for (int i = 0; i < n; ++i) {
            int tmp = sP[threadIdx.x];
            if (tmp < n && sI[tmp] != -1) {
                sI[threadIdx.x] = sI[tmp];
                sI[tmp] = -1;
            }
        }
    }
    __syncthreads();
    if (r < m && c < n) {
        a = A[r + c*lda];
        b = A[r + sP[c]*lda];
    }
    __syncthreads();
    if (r < m && c < n) {
        if (sI[c] != -1) {
            A[r + sI[c]*lda] = a;
        }
        A[r + c*lda] = b;
    }
}


}


/**
 * Permutes the columns of `A` so that the first `BLOCK_SIZE` columns become
 * the ones given by the list of indeces `P`.
 *
 * @tparam T  the element type of matrix `A`
 *
 * @param m  the number of rows of `A`
 * @param n  the number of columns of `A`
 * @param A  the memory location of `m`-by-`n` matrix `A`
 * @param lda  the leading dimension of `A`
 * @param P  an array of size `min(n, BLOCK_SIZE)` containing the list of
 *           indeces
 */
template<typename T>
inline void bcp(const DMat<T> &A, int *P, cudaStream_t s)
{
    const dim3 b(BLOCK_SIZE, BLOCK_SIZE);
    const int n = min(A.n_cols, BLOCK_SIZE);
    const dim3 g = ggs(b, A.n_rows, n);

    _bcp::kernel<BLOCK_SIZE><<<g, b, 0, s>>>
        (A.n_rows, n, A.data, A.ld, P);
}


}


#endif

