#ifndef GSVD_PREPROC_H_
#define GSVD_PREPROC_H_

void gsvd_preproc(int m, int n, int p, double *A, double *B,
                  int *k, int *l, double *U, double *V, double *Q, double tol);

void gsvd_preproc_sp(int m, int n, int p, float *A, float *B,
                     int *k, int *l, float *U, float *V, float *Q, float tol);

#endif

