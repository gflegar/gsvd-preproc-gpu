#ifndef TBRQ_CUH_
#define TBRQ_CUH_


#include <base_defs.h>
#include <coppy_block.cuh>
#include <givens_rot.cuh>
#include <utils.cuh>

#include <cstdio>


namespace gpg {


namespace _tbrq {


template <int bs, typename T>
__global__ void kernel(int ox, int oy, T * __restrict__ R, int ldr,
                       T * __restrict__ Q, int ldq, int ofs)
{
    const int ldl = 2*bs + 1;
    const int bx = blockIdx.x * bs;
    const int by = ((2*blockIdx.y + threadIdx.y / (bs/2) + 1) * ofs 
                    - 1 - threadIdx.y / (bs/2)) * bs;

    __shared__ T L[3*bs * ldl];

    ox -= bx;
    oy -= by;
    R += bx + by*ldr;
    Q += 2*bx + by*ldq;

    copyBlockFC<1, bs, 2*bs, 1, 1>(ox, oy, R, ldr, L, ldl);
    L[threadIdx.x + (threadIdx.y+bs)*ldl] = T(threadIdx.x == threadIdx.y);
    L[threadIdx.x + (threadIdx.y+2*bs)*ldl] =
        T(threadIdx.x == threadIdx.y + bs);

    for (int col = bs; col > 0; --col) {
        const int row = bs-1 + col - threadIdx.y;
        __syncthreads();
        if (row >= bs) {
            T c, s;
            genRot(L[row + (row-bs)*ldl], L[row-col + (row-bs)*ldl], c, s);
            applyRot<2*bs, 1, ldl>(c, s, L + row, L + row-col);
            applyRotB<2*bs, 1, ldl>(bs, c, s, L + row + 2*bs*ldl,
                                    L + row-col + 2*bs*ldl);
        }
    }

    __syncthreads();

    copyBlockFCR<0, bs, 2*bs, 1, 1>(ox, oy, L, ldl, R, ldr);
    copyBlockFCR<0, bs, 2*bs, 2, 1>(0, oy, L + bs*ldl, ldl, Q, ldq);
}


}


namespace sizes {


template <int bs>
inline dim3 tbrq_q(int m, int n)
{
    return dim3(rud(m, bs) * 2*bs, n);
}


}


/**
 * Computes the RQ factorization of triangular block pairs.
 *
 * Let `R` be an `m`-by-`n` matrix divided into `BLOCK_SIZE`-by-`BLOCK_SIZE`
 * block where first block-row or first block-column is smaller if `m` or `n`
 * is not divisible by `BLOCK_SIZE`.
 * Similarly, let `Q` be an `floor(m / (2*BLOCK_SIZE))*2*BLOCK_SIZE`-by-`n`
 * matrix divided into `2*BLOCK_SIZE`-by-`BLOCK_SIZE` blocks in the same
 * manner.
 * Let `R{i,j}` and `Q{i,j}` denote the `(i,j)`-th block of `R` and `Q`
 * respectively.
 * 
 * This function expects that `R{i,j}` is upper triangular (with the bottom
 * point of the triangle in the lower-right corner of the block in case of
 * non-square blocks). For each block-row `i` it calculates the RQ
 * factorization
 *
 *     [R{i,j-offset} R{i,j}] * [Q{i,j-offset} Q{i,j}] = [S{i,j-offset} S{i,j}]
 *
 * where `j` starts from the last block-column and moves down in steps of
 * `2*offset`. The resulting matrices satisfy the following:
 *
 * - [Q{i,j-offset} Q{i,j}] is orthonormal
 * - S{i,j-offset} is a null-matrix
 * - S{i,j} is upper-triangular
 *
 * The submatrices of `R` used in computation are overwritten with the
 * corresponding `S` blocks.
 *
 * @tparam T  a numeric type
 * 
 * @param[in] m  the number of rows of `R`
 * @param[in] n  the number of columns of `R`
 * @param[in,out] R  the memory location of the `m`-by-`n` matrix `R`
 * @param[in] ldr  the leading dimension of `R`
 * @param[out] Q  the memory location of the
 *                `(floor(m/(2*BLOCK_SIZE)*2*BLOCK_SIZE`-by-`n` matrix `Q`
 * @param[in] ldq  the leading dimension of `Q`
 * @param[in] offset  the horizontal offset of block pairs
 */
template <typename T>
inline void tbrq(const DMat<T> &R, const DMat<T> &Q, int offset,
                 cudaStream_t s)
{
    const dim3 b(2*BLOCK_SIZE, BLOCK_SIZE);
    dim3 g = ggs(dim3(BLOCK_SIZE, 2*BLOCK_SIZE), R.n_rows, R.n_cols);
    g.y = rud(g.y, offset);
    const int ox = g.x*BLOCK_SIZE - R.n_rows;
    const int oy = g.y*offset*2*BLOCK_SIZE - R.n_cols;
    _tbrq::kernel<BLOCK_SIZE><<<g, b, 0, s>>>
        (ox, oy, R.data - ox - oy*R.ld, R.ld, Q.data - oy*Q.ld, Q.ld, offset);
}


}


#endif

