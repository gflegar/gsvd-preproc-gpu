#ifndef GPG_UTILS_CUH_
#define GPG_UTILS_CUH_


#include <stdexcept>


namespace gpg {


/**
 * Divides \p x with \p y and rounds up the result.
 *
 * @tparam T  an integral type
 * @tparam S  an integral type
 *
 * @param[in] x  numerator
 * @param[in] y  denominator
 *
 * @return `ceil(x / y)`
 */
template <typename T, typename S>
inline S rud(T x, S y)
{
    return (x + y - 1) / y;
}


/**
 * Calculates the grid size from the given block size and the total thread
 * count.
 *
 * @tparam T  an integral type
 *
 * @param[in] bsize  the block size
 * @param[in] x  the number of threads in x dimension
 * @param[in] y  the number of threads in y dimension
 * @param[in] z  the number of threads in z dimension
 *
 * @return the required grid size to fit `x*y*z` threads with \p bsize blocks.
 */
template <typename T>
inline dim3 ggs(dim3 bsize, T x, T y = 1, T z = 1)
{
    return dim3(rud(x, bsize.x), rud(y, bsize.y), rud(z, bsize.z));
}


/**
 * Raises a runtime error if `error != 0`.
 *
 * @param[in] error  the error code
 *
 * @throw std::runtime_error
 */
inline void cudaAssert(cudaError_t error)
{
    if (error != 0) {
        throw std::runtime_error(cudaGetErrorString(error));
    }
}


}


#endif

