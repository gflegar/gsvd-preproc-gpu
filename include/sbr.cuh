#ifndef SBR_CUH_
#define SBR_CUH_


#include <base_defs.h>
#include <dmat.cuh>
#include <coppy_block.cuh>
#include <givens_rot.cuh>
#include <rot_order.cuh>
#include <utils.cuh>


#ifndef NOCONSTANT
__constant__ int SBR_POOL[3 * BLOCK_SIZE*DB_STEPS];
#endif


namespace gpg {


namespace _sbr {


template <int bs, typename T>
__global__ void
__launch_bounds__(512, 3)
#ifdef NOCONSTANT
kernel(int m, int n, T * __restrict__ A, int lda, T * __restrict__ R, int ldr,
       int * __restrict__ P, int * __restrict__ SBR_POOL)
#else
kernel(int m, int n, T * __restrict__ A, int lda, T * __restrict__ R, int ldr,
       int * __restrict__ P)
#endif
{
    int *CS = SBR_POOL;
    int *SS = SBR_POOL + bs * DB_STEPS;
    int *PE = SBR_POOL + 2*bs * DB_STEPS;

    const int ldl = 2*bs + 1;
    const int bx = blockIdx.x * 2*bs;
    const int by = blockIdx.y * 2*bs;

    __shared__ T L[2*bs * ldl];

    m -= bx;
    n -= by;
    A += bx;
    P += by;
    R += bx + by*ldr;

    copyBlockP<1, 2*bs, bs, 1, 2>(m, n, A, lda, L, ldl, P);

    /*
    for (int col = 0; col < 2*bs - 1; ++col) {
        for (int offset = 1; col + offset < 2*bs; offset <<= 1) {
            const int row = col + (2 * threadIdx.y + 1) * offset;
            __syncthreads();
            if (row < 2*bs) {
                T c, s;
                genRot(L[row-offset + col*ldl], L[row + col*ldl], c, s);
                applyRot<2*bs, 1, ldl>(c, s, L + row-offset, L + row);
            }
        }
    }*/

    for (int step = 0; step < DB_STEPS; ++step) {
        const int cosr = CS[step + threadIdx.y * DB_STEPS];
        const int sinr = SS[step + threadIdx.y * DB_STEPS];
        const int col = PE[step + threadIdx.y * DB_STEPS];
        __syncthreads();
        /*if (threadIdx.x == 0 && blockIdx.x + blockIdx.y == 0) {
            printf("step %d; thread %d; %d -> %d (%d)\n",
                   step, threadIdx.y, sinr, cosr, col);
        }*/
        if (col != -1) {
            T c, s;
            genRot(L[cosr + col*ldl], L[sinr + col*ldl], c, s);
            applyRot<2*bs, 1, ldl>(c, s, L + cosr, L + sinr);
        }
    }
    __syncthreads();

    copyBlock<0, 2*bs, bs, 1, 2>(m, n, L, ldl, R, ldr);
}


}


/**
 * Computes the `R` factor in QR decompositon of each block of `A * P**T`.
 *
 * For each `BLOCK_SIZE`-by-`BLOCK_SIZE` submatrix
 * `B = (A * P**T)(i*2*BLOCK_SIZE:(i+1)*2*BLOCK_SIZE,
 * j*2*BLOCK_SIZE:(j+1)*2*BLOCK_SIZE)`
 * computes a factorization `Qp * B = Rp` where `Qp` is orthonormal and `Rp`
 * upper triangular and stores `Rp` to `R(i+2*BLOCK_SIZE:(i+1)*2*BLOCK_SIZE,
 * j*2*BLOCK_SIZE:(j+1)*2*BLOCK_SIZE)`.
 *
 * @tparam T  a numeric type
 * 
 * @param[in] m  the number of rows of `A`
 * @param[in] n  the number of columns of `A`
 * @param[in] A  the memory location of `m`-by-`n` matrix `A`
 * @param[in] lda  the leading dimension of `A`
 * @param[out] R  the memory location of `m`-by-`n` matrix `R`
 * @param[in] ldr  the leading dimension of `R`
 * @param[in] P  an array containing the position of 1 in each row of `P`
 */
template <typename T>
inline void sbr(const DMat<T> &A, const DMat<T> &R, int *P, cudaStream_t s)
{
    static bool cset = false;
#ifdef NOCONSTANT
    static int *SBR_POOL;
#endif
    const dim3 b(2*BLOCK_SIZE, BLOCK_SIZE);
    const dim3 g(gpg::rud(A.n_rows, 2*BLOCK_SIZE),
                 gpg::rud(A.n_cols, 2*BLOCK_SIZE));
    if (!cset) {
        const int cmsize = BLOCK_SIZE * DB_STEPS;
        int H_POOL[3*cmsize];
        gpg::rot_order<2*BLOCK_SIZE, DB_STEPS>(H_POOL, H_POOL+cmsize,
                                               H_POOL+2*cmsize);
#ifdef NOCONSTANT
        cudaAssert(cudaMalloc(&SBR_POOL, 3*cmsize*sizeof(int)));
        cudaAssert(cudaMemcpy(SBR_POOL, H_POOL, 3*cmsize*sizeof(int),
                              cudaMemcpyHostToDevice));
#else
        cudaAssert(cudaMemcpyToSymbol(SBR_POOL, H_POOL,
                    3*cmsize * sizeof(int)));
#endif
        cset = true;
    }
#ifdef NOCONSTANT
    _sbr::kernel<BLOCK_SIZE><<<g, b, 0, s>>>
        (A.n_rows, A.n_cols, A.data, A.ld, R.data, R.ld, P, SBR_POOL);
#else
    _sbr::kernel<BLOCK_SIZE><<<g, b, 0, s>>>
        (A.n_rows, A.n_cols, A.data, A.ld, R.data, R.ld, P);
#endif
}


}


#endif

