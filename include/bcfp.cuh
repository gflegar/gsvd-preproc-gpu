#ifndef BCFP_CUH_
#define BCFP_CUH_


#include <algorithm>
#include <iostream>

#include <base_defs.h>
#include <utils.cuh>
#include <dmat.cuh>
#include <bcr.cuh>
#include <sbpc.cuh>
#include <iota.cuh>


namespace gpg {


namespace {


inline int block_count(int n)
{
    return gpg::rud(n, 2*BLOCK_SIZE);
}


inline int new_n(int n)
{
    if (n % (2*BLOCK_SIZE) == 0) {
        return n / 2;
    } else {
        const int blocks = block_count(n);
        return (blocks-1)*BLOCK_SIZE + min(n % (2*BLOCK_SIZE), BLOCK_SIZE);
    }
}


}


namespace sizes {


template <int bs>
inline dim3 bcfp_p(int m, int n)
{
    return dim3(max(2*bs+1, n));
}


template <int bs>
inline dim3 bcfp_w(int m, int n)
{
    return dim3(m, n);
}


template <int bs>
inline dim3 bcfp_iw(int m, int n)
{
    return dim3(n);
}


}


/**
 * Selects `min(BLOCK_SIZE, n)` columns from the matrix `A` with the maximal
 * posible rank.
 *
 * @tparam T  a numeric type
 *
 * @param[in] m  the number of rows of matrix `A`
 * @param[in] n  the number of columns of matrix `A`
 * @param[in] A  the memory location of an `m`-by-`n` matrix `A`
 * @param[in] lda  the leading dimension of `R`
 * @param[out] work  the memory location of an `m`-by-`n` matrix used to store
 *                  intermediate results
 * @param[in] ldw  the leading dimension of `work`
 * @param[out] P  an array of size `max(2*BLOCK_SIZE + 1, n)` used to
 *                store the indeces of resulting BLOCK_SIZE vectors (the rest
 *                is used for storing intermediate results)
 * @param[out] iwork  an array of `n` elements used to store intermediate
 *                    results
 * @param[out] rank  a memory location (on host) used to store the maximal
 *                   number of independent columns among the selected ones
 * @param[in] tol  a tolerance value used for the computation of `rank`
 */
template <typename T>
void bcfp(DMat<T> A, DMat<T> work, int *P, int *iwork,
          int &rank, T tol, cudaStream_t s)
{
    int *CP[] = {P, iwork};
    int blocks = block_count(A.n_cols);
    int p = 0;
    work.n_cols = A.n_cols;
    work.n_rows = A.n_rows;
    iota(A.n_cols, CP[p], s);

    /*int *bla = new int[A.n_cols];
    cudaMemcpy(bla, CP[p], sizeof(int)*A.n_cols,
               cudaMemcpyDeviceToHost);
    std::cout << "DEBUG (A is " << A.n_rows << " x " << A.n_cols << "):";
    for (int i = 0; i < A.n_cols; ++i) {
        std::cout << bla[i] << ' ';
    }
    std::cout << std::endl;
    delete[] bla;*/

    bcr(A, work, CP[p], s);
    while (blocks > 1) {
        /*int *bla = new int[A.n_cols];
        cudaMemcpy(bla, CP[p], sizeof(int)*A.n_cols,
                   cudaMemcpyDeviceToHost);
        std::cout << "DEBUG2 (A is " << A.n_rows << " x " << A.n_cols << "):";
        for (int i = 0; i < A.n_cols; ++i) {
            std::cout << bla[i] << ' ';
        }
        std::cout << std::endl;
        delete[] bla;*/

        sbpc(work, CP[p], CP[1-p], NULL, T(0), s);

        work.n_cols = A.n_cols = new_n(A.n_cols);
        blocks = block_count(A.n_cols);
        p = 1-p;

        /*bla = new int[A.n_cols];
        cudaMemcpy(bla, CP[p], sizeof(int)*A.n_cols,
                   cudaMemcpyDeviceToHost);
        std::cout << "DEBUG (A is " << A.n_rows << " x " << A.n_cols << "):";
        for (int i = 0; i < A.n_cols; ++i) {
            std::cout << bla[i] << ' ';
        }
        std::cout << std::endl;
        delete[] bla;*/

        bcr(A, work, CP[p], s);
    }
    /*bla = new int[A.n_cols];
    cudaMemcpy(bla, CP[p], sizeof(int)*A.n_cols,
               cudaMemcpyDeviceToHost);
    std::cout << "DEBUG2 (A is " << A.n_rows << " x " << A.n_cols << "):";
    for (int i = 0; i < A.n_cols; ++i) {
        std::cout << bla[i] << ' ';
    }
    std::cout << std::endl;
    delete[] bla;*/

    sbpc(work, CP[p], P, P+2*BLOCK_SIZE, tol, s);

    work.n_cols = A.n_cols = new_n(A.n_cols);
    /*bla = new int[A.n_cols];
    cudaMemcpy(bla, P, sizeof(int)*A.n_cols,
               cudaMemcpyDeviceToHost);
    std::cout << "DEBUG (A is " << A.n_rows << " x " << A.n_cols << "):";
    for (int i = 0; i < A.n_cols; ++i) {
        std::cout << bla[i] << ' ';
    }
    std::cout << std::endl;
    delete[] bla;*/

    cudaMemcpy(&rank, P+2*BLOCK_SIZE, sizeof(int), cudaMemcpyDeviceToHost);
    rank = min(rank, BLOCK_SIZE);
}


}


#endif

