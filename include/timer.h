#ifndef TIMER_H_
#define TIMER_H_

#include <ctime>
#include <ostream>

class Timer {
public:
    Timer(clockid_t cid = CLOCK_REALTIME): cid(cid) {}
    void start() { clock_gettime(cid, &stime); }
    void stop() { clock_gettime(cid, &etime); }
    double elapsed() const {
        return difftime(etime.tv_sec, stime.tv_sec) +
               (etime.tv_nsec - stime.tv_nsec) * 1e-9;
    }
private:
    clockid_t cid;
    struct timespec stime;
    struct timespec etime;
};

inline std::ostream& operator<<(std::ostream &os, const Timer &timer) {
    return os << timer.elapsed();
}

#endif

