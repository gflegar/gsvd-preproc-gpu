#ifndef BCQR_CUH_
#define BCQR_CUH_


#include <algorithm>
#include <vector>


#include <base_defs.h>
#include <dmat.cuh>
#include <sbqr.cuh>
#include <tbqr.cuh>
#include <bdmm.cuh>
#include <bgrmm.cuh>


namespace gpg {


namespace sizes {


template <int bs>
inline dim3 bcqr_w(int m, int n)
{
    dim3 res = sbqr_q<bs>(m, std::min(n, bs));
    for (int o = 1; o * bs < m; o <<= 1) {
        dim3 tmp = tbqr_q<bs>(m, std::min(n, bs));
        res.x = std::max(res.x, tmp.x);
        res.y += tmp.y;
    }
    return res;
}


}


/**
 * Computes the QR decomposition of tall and skiny matrix `A` and multiplies
 * each matrix in `Qs` with `Q**T`.
 *
 * Overwrites `A` with the `R` factor from the QR decomposition.
 *
 * @tparam T  a numeric type
 *
 * @param[in,out] A  described above
 * @param[in] astream  the CUDA stream used for the computation of the
 *                     QR decomposition
 * @param[in,out] Qs  described above
 * @param[in] qstreams  the CUDA stream used for the multiplication of `Qs` by
 *                      `Q**T`
 * @param[out] work  a matrix of size
 *                   `sizes::bcqr_w<BLOCK_SIZE>(A.n_rows, A.n_cols)` used to
 *                   store intermediate values
 */
template <typename T>
void bcqr(DMat<T> A, cudaStream_t astream, std::vector<DMat<T> > &Qs,
          const std::vector<cudaStream_t> &qstreams, DMat<T> work)
{
    const int sbw = sizes::sbqr_q<BLOCK_SIZE>(A.n_rows, BLOCK_SIZE).y;
    const int tbw = sizes::tbqr_q<BLOCK_SIZE>(A.n_rows, BLOCK_SIZE).y;

    A = A(0, -1, 0, std::min(A.n_cols, BLOCK_SIZE));

    cudaEvent_t stepComputed;
    cudaAssert(cudaEventCreateWithFlags(
            &stepComputed, cudaEventDisableTiming));

    sbqr(A, work, astream);
    cudaAssert(cudaEventRecord(stepComputed, astream));

    for (int i = Qs.size() - 1; i >= 0; --i) {
        cudaAssert(cudaStreamWaitEvent(qstreams[i], stepComputed, 0));
        bdmm(work, Qs[i], qstreams[i]);
    }

    work = work(0, -1, sbw, -1);

    for (int o = 1; o * BLOCK_SIZE < A.n_rows; o <<= 1) {
        tbqr(A, work, o, astream);
        cudaAssert(cudaEventRecord(stepComputed, astream));
        for (int i = Qs.size() - 1; i >= 0; --i) {
            cudaAssert(cudaStreamWaitEvent(qstreams[i], stepComputed, 0));
            bgrmm(work, Qs[i], o, qstreams[i]);
        }
        work = work(0, -1, tbw, -1);
    }

    cudaAssert(cudaEventDestroy(stepComputed));
}


}

#endif

