#ifndef BASE_DEFS_H_
#define BASE_DEFS_H_


#include <cstddef>


/**
 * Size of matrix block.
 */
const int BLOCK_SIZE = 16;

/**
 * Number of steps needed for single block QR.
 */
const int SB_STEPS = 26;

/**
 * Number of steps needed for double block QR.
 */
const int DB_STEPS = 56;


#endif

