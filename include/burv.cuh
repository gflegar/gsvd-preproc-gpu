#ifndef BURV_CUH_
#define BURV_CUH_


#include <vector>


#include <base_defs.h>
#include <dmat.cuh>
#include <bqrp.cuh>
#include <brq.cuh>


namespace gpg {


namespace sizes {


template <int bs>
inline dim3 burv_w(int m, int n)
{
    dim3 wqrp = bqrp_w<bs>(m, n);
    dim3 wrq = brq_w<bs>(m, n);
    return dim3(max(wqrp.x, wrq.x), max(wqrp.y, wrq.y));
}


template <int bs>
inline dim3 burv_iw(int m, int n)
{
    return bqrp_iw<bs>(m, n);
}


}


template <typename T>
void burv(DMat<T> A, cudaStream_t astream, std::vector<DMat<T> > Us,
          std::vector<cudaStream_t> ustreams, std::vector<DMat<T> > Vs,
          std::vector<cudaStream_t> vstreams, DMat<T> work, int *iwork,
          int &rank, T tol)
{
    bqrp(A, astream, Us, ustreams, Vs, vstreams, work, iwork, rank, tol);
    cudaEvent_t multCompleted;
    cudaAssert(cudaEventCreateWithFlags(
            &multCompleted, cudaEventDisableTiming));
    for (int i = 0; i < ustreams.size(); ++i) {
        cudaAssert(cudaEventRecord(multCompleted, ustreams[i]));
        cudaAssert(cudaStreamWaitEvent(astream, multCompleted, 0));
    }
    for (int i = 0; i < vstreams.size(); ++i) {
        cudaAssert(cudaEventRecord(multCompleted, vstreams[i]));
        cudaAssert(cudaStreamWaitEvent(astream, multCompleted, 0));
    }
    //cudaAssert(cudaDeviceSynchronize());
    brq(A(0, rank, 0, -1), astream, Vs, vstreams, work);
}


}


#endif

