#ifndef IOTA_CUH_
#define IOTA_CUH_


#include <base_defs.h>
#include <utils.cuh>


namespace gpg {


namespace _iota {


__global__ void kernel(int n, int *P);


}


/**
 * Set permutation matrix `P` to identity matrix.
 *
 * @param[in] n  the dimension of `P`
 * @param[out] P  an array containing the position of 1 in each row of `P`
 */
inline void iota(int n, int *P, cudaStream_t s)
{
    _iota::kernel<<<rud(n, BLOCK_SIZE), BLOCK_SIZE, 0, s>>>(n, P);
}


}

#endif

