#ifndef BDMM_CUH_
#define BDMM_CUH_


#include <base_defs.h>
#include <dmat.cuh>
#include <coppy_block.cuh>
#include <utils.cuh>


namespace gpg {


namespace _bdmm {


template <int bs, typename T>
__global__ void kernel(int m, int n, T * __restrict__ D, int ldd,
                       T * __restrict__ A, int lda)
{
    const int bx = blockIdx.x * bs;
    const int by = blockIdx.y * bs;

    __shared__ T LD[bs * bs];
    __shared__ T LA[bs * bs];

    m -= bx;
    n -= by;
    D += bx;
    A += bx + by*lda;

    T r = 0.0;

    copyBlock<1, bs, bs, 1, 1>(m, bs, D, ldd, LD, bs);
    copyBlock<1, bs, bs, 1, 1>(m, n, A, lda, LA, bs);

    __syncthreads();

    for (int k = 0; k < BLOCK_SIZE; ++k) {
        r += LD[threadIdx.x + k*bs] * LA[k + threadIdx.y*bs];
    }

    if (threadIdx.x < m && threadIdx.y < n) {
        A[threadIdx.x + threadIdx.y * lda] = r;
    }
}


}


/**
 * Multiplies the matrix `A` with the Q factor obtained by the call to
 * `sbqr()`.
 *
 * @tparam T  a numeric type
 *
 * @param[in] Q  the `Q` factor from `sbqr()`.
 * @param[in,out] A  the `m`-by-`n` matrix `A`
 * @param[in] s  the CUDA stream used for computation
 */
template <typename T>
inline void bdmm(const DMat<T> &Q, const DMat<T> &A, cudaStream_t s)
{
    dim3 b(BLOCK_SIZE, BLOCK_SIZE);
    dim3 g = ggs(b, A.n_rows, A.n_cols);
    _bdmm::kernel<BLOCK_SIZE><<<g, b, 0, s>>>
        (A.n_rows, A.n_cols, Q.data, Q.ld, A.data, A.ld);
}


}

#endif

