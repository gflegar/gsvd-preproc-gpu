#ifndef BRRQ_CUH_
#define BRRQ_CUH_



#include <algorithm>
#include <vector>


#include <base_defs.h>
#include <dmat.cuh>
#include <sbrq.cuh>
#include <tbrq.cuh>
#include <bdmmr.cuh>
#include <bgrmmr.cuh>


namespace gpg {


namespace sizes {


template <int bs>
inline dim3 brrq_w(int m, int n)
{
    dim3 res = sbrq_q<bs>(std::min(m, bs), n);
    for (int o = 1; o * bs < n; o <<= 1) {
        dim3 tmp = tbrq_q<bs>(std::min(m, bs), n);
        res.x += tmp.x;
        res.y = std::max(res.y, tmp.y);
    }
    return res;
}


}


/**
 * Computes the RQ decomposition of tall and skiny matrix `A` and multiplies
 * each matrix in `Qs` with `Q**T`.
 *
 * Overwrites `A` with the `R` factor from the QR decomposition.
 *
 * @tparam T  a numeric type
 *
 * @param[in,out] A  described above
 * @param[in] astream  the CUDA stream used for the computation of the
 *                     QR decomposition
 * @param[in,out] Qs  described above
 * @param[in] qstreams  the CUDA stream used for the multiplication of `Qs` by
 *                      `Q**T`
 * @param[out] work  a matrix of size
 *                   `sizes::bcqr_w<BLOCK_SIZE>(A.n_rows, A.n_cols)` used to
 *                   store intermediate values
 */
template <typename T>
void brrq(DMat<T> A, cudaStream_t astream, std::vector<DMat<T> > &Qs,
          const std::vector<cudaStream_t> &qstreams, DMat<T> work)
{
    const int sbw = sizes::sbrq_q<BLOCK_SIZE>(BLOCK_SIZE, A.n_cols).x;
    const int tbw = sizes::tbrq_q<BLOCK_SIZE>(BLOCK_SIZE, A.n_cols).x;

    A = A(0, std::min(A.n_rows, BLOCK_SIZE), 0, -1);

    cudaEvent_t stepComputed;
    cudaAssert(cudaEventCreateWithFlags(
            &stepComputed, cudaEventDisableTiming));

    sbrq(A, work, astream);
    cudaAssert(cudaEventRecord(stepComputed, astream));

    for (int i = 0; i < Qs.size(); ++i) {
        cudaAssert(cudaStreamWaitEvent(qstreams[i], stepComputed, 0));
        bdmmr(work, Qs[i], qstreams[i]);
    }

    work = work(sbw, -1, 0, -1);

    for (int o = 1; o * BLOCK_SIZE < A.n_cols; o <<= 1) {
        tbrq(A, work, o, astream);
        cudaAssert(cudaEventRecord(stepComputed, astream));
        for (int i = 0; i < Qs.size(); ++i) {
            cudaAssert(cudaStreamWaitEvent(qstreams[i], stepComputed, 0));
            bgrmmr(work, Qs[i], o, qstreams[i]);
        }
        work = work(tbw, -1, 0, -1);
    }

    cudaAssert(cudaEventDestroy(stepComputed));
}


}

#endif

