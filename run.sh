TD=$1
TC=$2
ALG=$3

if [ -z $4 ]; then
    VD=0
else
    VD=$4
fi

for i in $(seq 1 $TC); do
    CUDA_VISIBLE_DEVICES=$VD\
        ./build/gsvd-test-$ALG $(cat test-data/t$TD/call.txt.$i) || exit 1
done
