function [] = createCallData(dirname, md)
    p = ['test-data/' dirname '/'];
    e = '.out.%d'; 
    pia = [p 'A.in.%d'];
    pib = [p 'B.in.%d'];
    pa = [' -a ' p 'A' e];
    pb = [' -b ' p 'B' e];
    pu = [' -u ' p 'U' e];
    pv = [' -v ' p 'V' e];
    pq = [' -q ' p 'Q' e];
    
    fstring = ['-m %d -n %d -p %d' pa pb pu pv pq ' ' pia ' ' pib char(10)];
    for i = 2:size(md,1)
        j = i-1;
        fid = fopen(sprintf('%s/call.txt.%d', dirname, j), 'w');
        fprintf(fid, fstring, md(i,1), md(i,2), md(i,1), j, j, j, j, j, j, j);
        fclose(fid);
    end
end
