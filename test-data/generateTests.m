function [] = generateTests(dirname, rsizes, csizes, rratio)
    more off;
    if ~exist('rratio')
        rratio = [0.3, 0.5];
    end
    %[~, err, ~] = stat(dirname);
    if ~exist(dirname, 'dir')
        mkdir(dirname);
        fprintf('Creating metadata\n');
        md = genTestMetadata(rsizes, csizes, rratio);
        csvwrite([dirname '/meta.txt'], md);
    else
        fprintf('Loading metadata from file\n');
        md = csvread([dirname '/meta.txt']);
    end
    createCallData(dirname, md);
    for i = 2:size(md,1)
        fprintf('Generating test %d...   ', i-1);
        tic();
        [A, B] = genTestData(md(i,:));
        t = toc();
        fprintf('DONE (%.2es)\n', t);
        fprintf('Saving test %d...       ', i-1);
        fid = fopen(sprintf('%s/A.in.%d', dirname, i-1), 'w');
        fwrite(fid, A, 'double');
        fclose(fid);
        fid = fopen(sprintf('%s/B.in.%d', dirname, i-1), 'w');
        fwrite(fid, B, 'double');
        fclose(fid);
        fprintf('DONE\n');
    end
end
