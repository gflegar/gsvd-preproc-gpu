function [A, B] = genTestData(metadata)
    mcell = num2cell(metadata);
    [m, n, ra, rb, seed] = mcell{:};
    randn('state', seed);
    [Q1, R1] = qr(randn(m, ra+rb), 0);
    [Q2, R2] = qr(randn(m, rb), 0);
    [Q3, ~] = qr(randn(n,n),0);
    A = Q1 * [zeros(ra+rb, n-ra-rb), R1] * Q3;
    B = Q2 * [zeros(rb, n-rb) R2] * Q3;
end
