function [metadata] = genTestMetadata(rsizes, csizes, rratio, seed)
    if ~exist('seed')
        seed = rand('state');
        seed = seed(1);
    end
    rand('state', seed);
    
    nr = length(rsizes);
    nc = length(csizes);
    
    metadata = uint32(zeros(nr*nc+1, 5));
    metadata(1,5) = seed;
    for j = 1:nr
        for i = 1:nc
            metadata((i-1)*nr + j+1, :) = ...
                [rsizes(j)*csizes(i) csizes(i) ceil(csizes(i)*rratio) randi(4e9, 1, 1)]; 
        end
    end
    %sizes = uint32([ceil(sizes(:)) ceil(sizes(:)*ratios(:)')]);
    %ranks = zeros(n, 1);
    %for i = 1:n
    %    ranks(i,1) = randi(ceil(min(sizes(i,1:2))*rratios));
    %    ranks(i,2) = randi(ceil(min(sizes(i,2:3))*rratios));
    %end
    
    %sizes = [sizes ranks];
    
    %[r, c] = size(sizes);
    %seeds = randi(4e9, r, 1);
    %metadata = [zeros(1, c) seed; sizes seeds];
end
